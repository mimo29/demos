﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Uniconta.API.System;
using Uniconta.ClientTools;
using Uniconta.Common;
using Uniconta.DataModel;

namespace ImportingTool.Model
{
    public partial class importLog : INotifyPropertyChanged
    {
        public TextBox target;
        public string LogMsg { get { return LogMessage.ToString(); } set { NotifyPropertyChanged("LogMsg"); } }
        StringBuilder LogMessage = new StringBuilder();
        int errcnt;
        public void AppendLog(string msg)
        {
            LogMessage.Append(msg);
            LogMsg = LogMessage.ToString();
        }
        public void AppendErrorLogLine(string msg)
        {
            errcnt++;
            if (errcnt < 100)
                AppendLogLine(msg);
        }
        public void AppendLogLine(string msg)
        {
            LogMessage.AppendLine(msg);
            LogMsg = LogMessage.ToString();
            if (target != null)
                target.ScrollToEnd();
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public int GetPos(string colName)
        {
            var pos = FirstLine.IndexOf(colName);
            return pos;
        }
        public List<string> GetFirstLine()
        {
            return FirstLine;
        }

        public bool Terminate;
        public ProgressBar progressBar;
        public string extension;
        public string path;
        public StringSplit sp;
        public string CurFile;
        public CrudAPI api;
        public PaymentTerm[] pTerms;
        TextReader r;
        public List<string> FirstLine;
        public bool FirstLineIsEmpty, HasDebtorSummeryAccount, HasCreditorSummeryAccount;
        public bool HasDepartment, HasPorpose, HasCentre, HasBOM, HasItem, HasPriceList, HasDebitor, HasCreditor, HasContact, HasDelivery, HasDim1, HasDim2, HasDim3, HasDim4, HasDim5;
        public int NumberOfDimensions;
        public SQLCache LedgerAccounts, vats, Debtors, Creditors, PriceLists, Items, ItemGroups, Employees, Payments, DebGroups, CreGroups, Shipments;
        public SQLCache dim1, dim2, dim3, dim4, dim5;
        public string errorAccount, valutaDif;
        public Encoding C5Encoding;
        public byte[] CharBuf;
        StringBuilder readSb;

        public bool VatIsUpdated;
        public Currencies CompCurCode;
        public string CompCur;
        public CountryCode CompCountryCode;
        public string CompCountry;

        public CompanyFinanceYear[] years;
        public int Accountlen;
        public bool Set0InAccount;

        public void closeFile()
        {
            if (r != null)
            {
                var _r = r;
                r = null;
                _r.Dispose();
            }
        }

        public importLog(string path)
        {
            this.path = path;
            readSb = new StringBuilder(1000);
        }

        public void Ex(Exception ex)
        {
            api.ReportException(ex, string.Format("File={0}", CurFile));
            AppendLogLine(ex.Message);
        }

        public bool OpenFile(string file, string comment = null)
        {
            closeFile();

            if (comment != null)
                comment = string.Format("{0} - {1}", file, comment);
            else
                comment = file;

            CurFile = this.path + file + extension;
            if (!File.Exists(CurFile))
            {
                AppendLogLine(String.Format(Uniconta.ClientTools.Localization.lookup("FileNotFound"), comment));
                return false;
            }
            AppendLogLine(String.Format(Uniconta.ClientTools.Localization.lookup("ImportingFile"), comment));
            r = new StreamReader(CurFile, this.C5Encoding ?? Encoding.Default, true);
            if (r != null)
            {
                if (FirstLineIsEmpty)
                {
                    if (sp != null)
                        FirstLine = GetLine(1);  // first line is empty
                    else
                        ReadLine();
                }           
                return true;
            }
            AppendLogLine(string.Format("{0} : ", Uniconta.ClientTools.Localization.lookup("FileAccess"), CurFile));
            return false;
        }
        public void Log(string s)
        {
            AppendLogLine(s);
        }
        public async Task Error(ErrorCodes Err)
        {
            var errors = await api.session.GetErrors();
            var s = Err.ToString();
            api.ReportException(null, string.Format("Error in conversion = {0}, file={1}", s, CurFile));
            AppendLogLine(s);
            if (errors != null)
            {
                foreach(var er in errors)
                {
                    if (er.inProperty != null)
                        AppendLogLine(string.Format("In property = {0}", er.inProperty));
                    if (er.message != null)
                        AppendLogLine(string.Format("Message = {0}", er.message));
                }
            }
        }

        public StringBuilder ReadLine()
        {
            var C5Encoding = this.C5Encoding;
            var readSb = this.readSb;
            readSb.Clear();
            int ch;
            var r = this.r;
            while ((ch = r.Read()) >= 0)
            {
                if (ch == '\r' || ch == '\n')
                {
                    var ch2 = r.Read();
                    if (ch == '\n' && ch2 != '\r')
                    {
                        readSb.Append('\n'); // it is just a new line inside an address field. insert it.
                        ch = ch2;
                    }
                    else
                        break;
                }
                if (ch == '\\' && C5Encoding != null)
                {
                    /*
                     //******************************************************
                     //C5 data comes with special characters we must replace.
                     lin = lin.Replace("\\221", "æ");
                     lin = lin.Replace("\\233", "ø");
                     lin = lin.Replace("\\206", "å");
                     lin = lin.Replace("\\222", "Æ");
                     lin = lin.Replace("\\235", "Ø");
                     lin = lin.Replace("\\217", "Å");
                     lin = lin.Replace("\\224", "ö");
                     lin = lin.Replace("\\201", "ü");
                     lin = lin.Replace("\\204", "ä");
                     lin = lin.Replace("\\341", "ß");
                     //******************************************************
                     */
                    int val = 0;
                    for (int i = 1; i <= 3; i++)
                    {
                        ch = r.Read();
                        if (ch >= '0' && ch <= '7')
                        {
                            val = val * 8 + (ch - '0');
                            ch = 0;
                        }
                        else
                        {
                            if (i <= 2)
                            {
                                readSb.Append('\\');
                                if (i == 2)
                                    readSb.Append(val);
                            }
                            break;
                        }
                    }
                    if (val >= 128 && val <= 255)
                    {
                        this.CharBuf[0] = (byte)val;
                        var s = C5Encoding.GetString(this.CharBuf);
                        readSb.Append(s);
                    }
                }
                if (ch != 0)
                    readSb.Append((char)ch);
            }
            if (ch == -1 && readSb.Length == 0)
                return null;
            return readSb;
        }


        public List<string> GetLine(int minCols)
        {
            List<string> oldLin = null;
            for (;;)
            {
                var lin = this.ReadLine();
                if (lin == null)
                    return null;
                var lines = sp.Split(lin);
                if (oldLin != null)
                {
                    oldLin.AddRange(lines);
                    lines = oldLin;
                }
                if (lines.Count >= minCols)
                    return lines;
                oldLin = lines;
            }
        }

        static public string GetNotEmpty(string s) { return string.IsNullOrWhiteSpace(s) ? null : s; }

        static public double ToDouble(string str)
        {
            int decimals;
            var val = Uniconta.Common.Utility.NumberConvert.ToDouble(str, (char)0, out decimals);
            if (double.IsNaN(val) || val > 9999999999999.99d || val < -9999999999999.99d) // 9.999.999.999.999,99d
                return 0d;
            if (decimals <= 2)
                return val;
            else
                return Math.Round(val, 2);
        }

        //Leading zero's on GLAccount. 
        public string GLAccountFromC5(string account)
        {
            if (account != string.Empty && Accountlen != 0)
                return account.PadLeft(Accountlen, '0');
            else
                return account;
        }

        public string GLAccountFromC5_Validate(string account)
        {
            if (account == string.Empty)
                return string.Empty;

            return this.LedgerAccounts.Get(this.GLAccountFromC5(account))?.KeyStr ?? string.Empty;
        }

        public Currencies ConvertCur(string str)
        {
            if (str != string.Empty && this.CompCur != str)
            {
                Currencies cur;
                if (Enum.TryParse(str, true, out cur))
                {
                    if (cur != this.CompCurCode)
                        return cur;
                }
            }
            return 0;
        }

        public string GetVat_Validate(string vat)
        {
            if (vat != string.Empty)
            {
                var rec = (GLVat)this.vats?.Get(vat);
                if (rec != null)
                    return rec._Vat;
            }
            return string.Empty;
        }

        static public ItemUnit ConvertUnit(string str)
        {
            if (AppEnums.ItemUnit != null)
            {
                var unit = str.Replace(".", string.Empty);
                var unitId = AppEnums.ItemUnit.IndexOf(unit);
                if (unitId > 0)
                    return (ItemUnit)unitId;
            }
            return 0;
        }

        public async Task<ErrorCodes> Insert(IEnumerable<UnicontaBaseEntity> lst, bool InsertInBackground = true)
        {
            closeFile();

            if (Terminate)
                return ErrorCodes.NoSucces;

            var count = lst.Count();
            AppendLogLine(string.Format("{0} {1}. {2} = {3} ...", Uniconta.ClientTools.Localization.lookup("Saving"), CurFile, Uniconta.ClientTools.Localization.lookup("NumberOfRecords"), count));

            var lst2 = new List<UnicontaBaseEntity>();
            int n = 0;
            int nError = 0;
            ErrorCodes ret = 0;

            api.AllowBackgroundCrud = InsertInBackground;
            progressBar.Maximum = lst.Count();
            foreach (var rec in lst)
            {
                lst2.Add(rec);

                n++;
                if ((n % 100) == 0 || n == count)
                {
                    if (n == count)
                        StartImport.UpdateProgressbar(count, null, progressBar);
                    else
                        StartImport.UpdateProgressbar(n, null, progressBar);

                    var ret2 = await api.Insert(lst2);
                    if (ret2 != Uniconta.Common.ErrorCodes.Succes)
                    {
                        foreach (var oneRec in lst2)
                        {
                            ret2 = await api.Insert(oneRec);
                            if (ret2 != 0)
                            {
                                ret = ret2;
                                nError++;
                            }
                        }
                        if (ret != 0)
                            await this.Error(ret);
                    }
                    lst2.Clear();
                }
            }
            api.AllowBackgroundCrud = false;

            if (nError != 0)
                AppendLogLine(string.Format("Saved {0}. Number of records with error = {1}", CurFile, nError));
            return ret;
        }
        public void Update(IEnumerable<UnicontaBaseEntity> lst)
        {
            closeFile();
            var count = lst.Count();
            AppendLogLine(string.Format("Updating. Number of records = {0} ...", count));
            api.UpdateNoResponse(lst);

            /*
            var lst2 = new List<UnicontaBaseEntity>();
            int n = 0;
            ErrorCodes ret = 0;
            foreach (var rec in lst)
            {
                lst2.Add(rec);

                n++;
                if ((n % 100) == 0 || n == count)
                {
                    ret = await api.Update(lst2);
                    if (ret != Uniconta.Common.ErrorCodes.Succes)
                        await this.Error(ret);

                    lst2.Clear();
                }
            }
            */
        }
    }

}
