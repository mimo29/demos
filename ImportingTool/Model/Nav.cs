﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.Common;
using Uniconta.DataModel;
using System.IO;
using Uniconta.Common.Utility;
using Uniconta.API.GeneralLedger;
using System.Threading;
using Uniconta.ClientTools;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Threading;
using Uniconta.ClientTools.DataModel;
using static ImportingTool.Model.c5;
#if WPF
using Uniconta.ClientTools.Page;
#endif

namespace ImportingTool.Model
{
    static public class Nav
    {
        static public async Task<ErrorCodes> importAll(importLog log, CountryCode Country, int DimensionImport,
            string dim1, string dim2, string dim3, string dim4, string dim5, string errorAccount)
        {
            if (log.Set0InAccount)
                Nav.getGLAccountLen(log);

            if (!string.IsNullOrWhiteSpace(dim1) && !dim1.Contains(Localization.lookup("Optional")))
                await Nav.importDim1(log, dim1);
            if (!string.IsNullOrWhiteSpace(dim2) && !dim2.Contains(Localization.lookup("Optional")))
                await Nav.importDim2(log, dim2);
            if (!string.IsNullOrWhiteSpace(dim3) && !dim3.Contains(Localization.lookup("Optional")))
                await Nav.importDim3(log, dim3);
            if (!string.IsNullOrWhiteSpace(dim4) && !dim4.Contains(Localization.lookup("Optional")))
                await Nav.importDim4(log, dim4);
            if (!string.IsNullOrWhiteSpace(dim5) && !dim5.Contains(Localization.lookup("Optional")))
                await Nav.importDim5(log, dim5);

            Nav.importCompany(log);
            await Nav.importYear(log);

            var err = await Nav.importGLAccount(log, errorAccount);
            if (err != 0)
                return err;


            err = await importVAT(log);
            if (err != 0)
                return err;

            await Nav.importDebGroup(log); //TODO: Spørg far om  var Acc = (MyGLAccount)LedgerAccounts.Get(log.GLAccountFromC5(Nav.GetField(lines, 2)));
            await Nav.importCredGroup(log); //TODO: Spørg far om  var Acc = (MyGLAccount)LedgerAccounts.Get(log.GLAccountFromC5(Nav.GetField(lines, 2)));
            await Nav.importEmployee(log);
            await Nav.importShipment(log);
            await Nav.importPaymentGroup(log);

            err = await Nav.importDebitor(log);
            if (err != 0)
                return err;

            err = await Nav.importVendor(log);
            if (err != 0)
                return err;

            await Nav.importContact(log);

            await Nav.importInvGroup(log); //TODO: Ikke færdig. Kig på C5 import for at se mangler
            await Nav.importCredPriceGroup(log);
            await Nav.importDebPriceGroup(log);

            var items = await Nav.importInv(log);
            if (items == ErrorCodes.Succes)
            {
                //TODO: importInvPrices fra C5 import, kunne jeg ikke løse i forhold til NAV.
                await Nav.importInvBOM(log);
            }

            //TODO : Kunne ikke finde importNotes i NAV.

            var orders = new Dictionary<long, DebtorOrder>();
            await Nav.importOrder(log, orders);
            if (orders.Count > 0)
                await Nav.importOrderLinje(log, orders);
            orders = null;

            var purchages = new Dictionary<long, CreditorOrder>();
            await Nav.importPurchage(log, purchages);
            if (purchages.Count > 0)
                await Nav.importPurchageLinje(log, purchages);
            purchages = null;

            // clear memory 
            log.Items = null;
            log.ItemGroups = null;
            log.PriceLists = null;
            log.Payments = null;
            log.Employees = null;

            if (log.Terminate)
                return ErrorCodes.NoSucces;

            if (log.years != null && log.years.Length > 0)
            {
                DCImportTrans[] debpost = null;
                if (log.Debtors != null)
                {
                    debpost = Nav.importDCTrans(log, true, log.Debtors);
                    if (debpost != null && !log.HasDebtorSummeryAccount)
                    {
                        log.AppendLogLine("FEJL..... Du har ikke nogen debitor-samlekonto sat op på debitorgrupperne");
                        return ErrorCodes.NoSucces;
                    }
                }

                DCImportTrans[] krepost = null;
                if (log.Creditors != null)
                {
                    krepost = Nav.importDCTrans(log, false, log.Creditors);
                    if (krepost != null && !log.HasCreditorSummeryAccount)
                    {
                        log.AppendLogLine(
                            "FEJL..... Du har ikke nogen kreditor-samlekonto sat op på kreditorgrupperne");
                        return ErrorCodes.NoSucces;
                    }
                }
                err = await Nav.importGLTrans(log, debpost, krepost);
                if (err != 0)
                    return err;
            }

            Nav.UpdateVATonAccount(log);

            return ErrorCodes.Succes;
        }

        public static void getGLAccountLen(importLog log)
        {
            if (!log.OpenFile("15 - G_L Account", "Finanskonti"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int AccountPrev = 0;
                int Accountlen = 0, nMax = 0;
                int accountPos = log.GetPos("No.");

                while ((lines = log.GetLine(2)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3")
                    {
                        log.C5Encoding = null;
                        continue;
                    }

                    var _account = log.GLAccountFromC5(Nav.GetField(lines, accountPos));
                    if (_account.Length >= Accountlen)
                    {
                        if (_account.Length == Accountlen)
                            nMax++;
                        else
                        {
                            int i = _account.Length;
                            while (--i >= 0)
                            {
                                var ch = _account[0];
                                if (ch < '0' || ch > '9')
                                    break;
                            }
                            if (i < 0)
                            {
                                AccountPrev = Accountlen;
                                Accountlen = _account.Length;
                                nMax = 0;
                            }
                        }
                    }
                }
                if (nMax > 2)
                    log.Accountlen = Accountlen;
                else
                    log.Accountlen = AccountPrev;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static async Task importYear(importLog log)
        {
            if (!log.OpenFile("50 - Accounting Period", "Finansår"))
            {
                log.AppendLogLine("Financial years not imported. No transactions will be imported");
                return;
            }
            try
            {
                List<string> lines;

                var lst = new List<CompanyFinanceYear>();
#if WPF
                var now = BasePage.GetSystemDefaultDate();
#else
                var now = DateTime.Now;
#endif
                CompanyFinanceYear rec = null;
                DateTime dt = DateTime.MinValue;

                int newFinansYearPos = log.GetPos("New Fiscal Year");
                int startYearPos = log.GetPos("Starting Date");

                while ((lines = log.GetLine(1)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4")
                    {
                        continue;
                    }

                    dt = eco.ParseDT(Nav.GetField(lines, startYearPos));

                    var year = Nav.GetField(lines, newFinansYearPos).ToLower();
                    if (year == "no" || year == "nej" || year == Uniconta.ClientTools.Localization.lookup("No").ToLower())
                        continue;

                    if (rec != null)
                    {
                        rec._ToDate = dt.AddDays(-1);
                        if (rec._FromDate <= now && rec._ToDate >= now)
                            rec._Current = true;
                    }
                    rec = new CompanyFinanceYear();
                    rec._FromDate = dt;
                    rec._State = FinancePeriodeState.Open; // set to open, otherwise we can't import transactions
                    rec.OpenAll();
                    lst.Add(rec);

                    log.AppendLogLine(dt.ToShortDateString());
                }
                if (rec != null)
                {
                    rec._ToDate = dt.AddDays(DateTime.DaysInMonth(dt.Year, dt.Month) - dt.Day);
                    if (rec._FromDate <= now && rec._ToDate >= now)
                        rec._Current = true;
                }

                var err = await log.Insert(lst);
                if (err != 0)
                    log.AppendLogLine("Financial years not imported. No transactions will be imported");
                else
                    log.years = lst.ToArray();
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static string GetField(List<string> lines, int pos)
        {
            if (lines.Count < pos || pos < 0)
            {
                return string.Empty;
            }
            else
            {
                var returnvalue = string.IsNullOrWhiteSpace(lines[pos]) ? string.Empty : lines[pos];
                return returnvalue;
            }
        }

        public static async Task<ErrorCodes> importDebGroup(importLog log)
        {
            if (!log.OpenFile("92 - Customer Posting Group", "Debitor gruppe"))
            {
                log.api.CompanyEntity.Inventory = false;
                return ErrorCodes.Succes;
            }

            try
            {
                int codePos = log.GetPos("Code");
                int receivableAccPos = log.GetPos("Receivables Account");
                int payDiscDebAccPos = log.GetPos("Payment Disc. Debit Acc.");
                int invoiceRoundingAccountPos = log.GetPos("Invoice Rounding Account");
                int additionalFeeAccountpos = log.GetPos("Additional Fee Account");
                int interestAccounttPos = log.GetPos("Interest Account");
                int salesCreditMemoAccountPos = log.GetPos("Sales Credit Memo Account");
                int purchCreditMemoAccountPos = log.GetPos("Purch. Credit Memo Account");

                var LedgerAccounts = log.LedgerAccounts;

                string lastAcc = null;
                List<string> lines;
                var lst = new Dictionary<string, DebtorGroup>(StringNoCaseCompare.GetCompare());
                var InvoiceAccs = new List<c5.InvoiceAccounts>();

                while ((lines = log.GetLine(8)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2")
                        continue;

                    var rec = new DebtorGroup();
                    rec._Group = Nav.GetField(lines, codePos);
                    if (string.IsNullOrWhiteSpace(rec._Group))
                        continue;

                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, codePos));
                    rec._SummeryAccount = log.GLAccountFromC5_Validate(Nav.GetField(lines, receivableAccPos));
                    rec._SettlementDiscountAccount =
                        log.GLAccountFromC5_Validate(Nav.GetField(lines, payDiscDebAccPos));

                    //var Acc = (MyGLAccount)LedgerAccounts.Get(log.GLAccountFromC5(Nav.GetField(lines, receivableAccPos)));
                    //if (Acc != null)
                    //{
                    //    rec._RevenueAccount = Acc._Account;
                    //    rec._Vat = log.GetVat_Validate(Acc.VatKode);
                    //}

                    rec._UseFirstIfBlank = true;
                    if (!lst.Any())
                        rec._Default = true;

                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);

                    if (rec._SummeryAccount != string.Empty && rec._SummeryAccount != lastAcc)
                    {
                        lastAcc = rec._SummeryAccount;
                        var acc = (MyGLAccount) LedgerAccounts?.Get(lastAcc);
                        if (acc != null)
                        {
                            acc._AccountType = (byte) GLAccountTypes.Debtor;
                            acc.HasChanges = true;
                        }
                    }
                }

                if (lastAcc != null)
                {
                    log.HasDebtorSummeryAccount = true;
                    foreach (var gr in lst.Values)
                        if (gr._SummeryAccount == string.Empty)
                            gr._SummeryAccount = lastAcc;
                }

                await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.DebGroups = new SQLCache(accs, true);

                return ErrorCodes.Succes;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> importCredGroup(importLog log)
        {
            if (!log.OpenFile("93 - Vendor Posting Group", "Kreditor gruppe"))
            {
                log.api.CompanyEntity.Inventory = false;
                return ErrorCodes.Succes;
            }

            try
            {
                int codePos = log.GetPos("Code");
                int payableAccPos = log.GetPos("Payables Account");
                int payDiscDebAccPos = log.GetPos("Payment Disc. Debit Acc.");
                int invoiceRoundingAccountPos = log.GetPos("Invoice Rounding Account");
                int additionalFeeAccountpos = log.GetPos("Additional Fee Account");
                int interestAccounttPos = log.GetPos("Interest Account");
                int salesCreditMemoAccountPos = log.GetPos("Sales Credit Memo Account");
                int purchCreditMemoAccountPos = log.GetPos("Purch. Credit Memo Account");

                List<string> lines;
                string lastAcc = null;
                var LedgerAccounts = log.LedgerAccounts;

                var lst = new Dictionary<string, CreditorGroup>(StringNoCaseCompare.GetCompare());
                var InvoiceAccs = new List<c5.InvoiceAccounts>();

                while ((lines = log.GetLine(8)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2")
                        continue;

                    var rec = new CreditorGroup();

                    rec._Group = Nav.GetField(lines, codePos);
                    if (string.IsNullOrWhiteSpace(rec._Group))
                        continue;

                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, codePos));
                    rec._SummeryAccount = log.GLAccountFromC5_Validate(Nav.GetField(lines, payableAccPos));
                    rec._SettlementDiscountAccount =
                        log.GLAccountFromC5_Validate(Nav.GetField(lines, payDiscDebAccPos));

                    rec._UseFirstIfBlank = true;
                    if (!lst.Any())
                        rec._Default = true;
                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);

                    if (rec._SummeryAccount != string.Empty && rec._SummeryAccount != lastAcc)
                    {
                        lastAcc = rec._SummeryAccount;
                        var acc = (MyGLAccount) LedgerAccounts.Get(lastAcc);
                        if (acc != null)
                        {
                            acc._AccountType = (byte) GLAccountTypes.Creditor;
                            acc.HasChanges = true;
                        }
                    }
                }
                if (lastAcc != null)
                {
                    log.HasCreditorSummeryAccount = true;
                    foreach (var gr in lst.Values)
                        if (gr._SummeryAccount == string.Empty)
                            gr._SummeryAccount = lastAcc;
                }
                await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.CreGroups = new SQLCache(accs, true);

                return ErrorCodes.Succes;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> importDebitor(importLog log)
        {
            if (!log.OpenFile("18 - Customer", "Debitorer"))
            {
                return ErrorCodes.Succes;
            }

            try
            {
                List<string> lines;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;

                var PriceLists = log.PriceLists;
                var Employees = log.Employees;
                var Payments = log.Payments;
                var grpCache = log.DebGroups;
                var Shipments = log.Shipments;

                int groupPos = log.GetPos("Customer Posting Group");
                int accountPos = log.GetPos("No.");
                int namePos = log.GetPos("Name");
                int addressPos = log.GetPos("Address");
                int address2Pos = log.GetPos("Address 2");
                int cityPos = log.GetPos("City");
                int zipPos = log.GetPos("Post Code");
                int ourAccountNOPos = log.GetPos("Our Account No.");
                int emailPos = log.GetPos("E-Mail");
                int contactPersPos = log.GetPos("Contact");
                int phoneNoPos = log.GetPos("Phone No.");
                int EANNoPos = log.GetPos("EAN No.");
                int VATNoPos = log.GetPos("VAT Registration No.");
                int dim1Pos = log.GetPos("Global Dimension 1 Code");
                int dim2Pos = log.GetPos("Global Dimension 2 Code");
                int maxCredPos = log.GetPos("Credit Limit (LCY)");
                int cosPriceGrpPos = log.GetPos("Customer Price Group");
                int countryCodePos = log.GetPos("Country/Region Code");
                int curCodePos = log.GetPos("Currency Code");
                int paymentMethPos = log.GetPos("Payment Method Code");
                int paymentTermPos = log.GetPos("Payment Terms Code");
                int salesPersCodePos = log.GetPos("Salesperson Code");
                int shipmnentPos = log.GetPos("Shipment Method Code");
                int priceVatPos = log.GetPos("Prices Including VAT");
                int invoiceAccPos = log.GetPos("Bill-to Customer No.");
                int vatGroupPos = log.GetPos("VAT Bus. Posting Group");

                var InvoiceAccs = new List<c5.InvoiceAccounts>();

                var lst = new Dictionary<string, Debtor>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(25)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new Debtor();
                    rec._Account = Nav.GetField(lines, accountPos);
                    if (string.IsNullOrWhiteSpace(rec._Account))
                        continue;

                    rec._Group = grpCache.Get(Nav.GetField(lines, groupPos))?.KeyStr;
                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, namePos));
                    rec._Address1 = importLog.GetNotEmpty(Nav.GetField(lines, addressPos));
                    rec._Address2 = importLog.GetNotEmpty(Nav.GetField(lines, address2Pos));
                    rec._ZipCode = importLog.GetNotEmpty(Nav.GetField(lines, zipPos));
                    rec._City = importLog.GetNotEmpty(Nav.GetField(lines, cityPos));
                    rec._LegalIdent = importLog.GetNotEmpty(Nav.GetField(lines, VATNoPos));
                    rec._ContactEmail = importLog.GetNotEmpty(Nav.GetField(lines, emailPos));
                    rec._ContactPerson = importLog.GetNotEmpty(Nav.GetField(lines, contactPersPos));
                    rec._Phone = importLog.GetNotEmpty(Nav.GetField(lines, phoneNoPos));
                    rec._EAN = importLog.GetNotEmpty(Nav.GetField(lines, EANNoPos));
                   // rec._Vat = log.GetVat_Validate(Nav.GetField(lines, vatGroupPos));
                    rec._Dim1 = dim1?.Get(Nav.GetField(lines, dim1Pos))?.KeyStr;
                    rec._Dim2 = dim2?.Get(Nav.GetField(lines, dim2Pos))?.KeyStr;
                    rec._Employee = Employees?.Get(Nav.GetField(lines, salesPersCodePos))?.KeyStr;
                    rec._Payment = Payments?.Get(Nav.GetField(lines, paymentTermPos))?.KeyStr;
                    rec._Shipment = Shipments?.Get(Nav.GetField(lines, shipmnentPos))?.KeyStr;
                    rec._CreditMax = importLog.ToDouble(Nav.GetField(lines, maxCredPos));

                    var priceVat = Nav.GetField(lines, priceVatPos).ToLower();
                    if (priceVat == "yes" || priceVat == "ja" || priceVat == Uniconta.ClientTools.Localization.lookup("Yes").ToLower())
                        rec._PricesInclVat = true;

                    if (PriceLists != null)
                    {
                        var priceRec = (InvPriceList) PriceLists.Get(Nav.GetField(lines, cosPriceGrpPos));
                        if (priceRec != null)
                        {
                            rec._PriceList = priceRec.KeyStr;
                            rec._PricesInclVat = priceRec._InclVat;
                        }
                    }

                    //var t = (int)NumberConvert.ToInt(Nav.GetField(lines, 11));
                    //if (t > 0)
                    //    rec._VatZone = (VatZones)(t - 1);

                    rec._Country = c5.convertCountry(Nav.GetField(lines, countryCodePos), log.CompCountryCode,
                        out rec._VatZone);
                    rec._Currency = log.ConvertCur(Nav.GetField(lines, curCodePos));

                    if (!lst.ContainsKey(rec.KeyStr))
                    {
                        lst.Add(rec.KeyStr, rec);
                        if (Nav.GetField(lines, invoiceAccPos) != string.Empty)
                            InvoiceAccs.Add(new c5.InvoiceAccounts
                            {
                                Acc = rec._Account,
                                InvAcc = Nav.GetField(lines, invoiceAccPos)
                            });
                    }
                }
                await log.Insert(lst.Values);
                log.HasDebitor = true;
                var accs = lst.Values.ToArray();
                log.Debtors = new SQLCache(accs, true);

                c5.UpdateInvoiceAccount(log, InvoiceAccs, true);

                return ErrorCodes.Succes;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }
        }

        public static async Task<ErrorCodes> importVendor(importLog log)
        {
            if (!log.OpenFile("23 - Vendor", "Kreditorer"))
            {
                return ErrorCodes.Succes;
            }

            try
            {
                List<string> lines;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;

                var PriceLists = log.PriceLists;
                var Employees = log.Employees;
                var Payments = log.Payments;
                var grpCache = log.CreGroups;
                var Shipment = log.Shipments;

                int groupPos = log.GetPos("Vendor Posting Group");
                int accountPos = log.GetPos("No.");
                int namePos = log.GetPos("Name");
                int addressPos = log.GetPos("Address");
                int address2Pos = log.GetPos("Address 2");
                int cityPos = log.GetPos("City");
                int zipPos = log.GetPos("Post Code");
                int ourAccPos = log.GetPos("Pay-to Vendor No.");
                int emailPos = log.GetPos("E-Mail");
                int contactPersPos = log.GetPos("Contact");
                int phoneNoPos = log.GetPos("Phone No.");
                //int EANNoPos = log.GetPos("EAN No.");
                int VATNoPos = log.GetPos("VAT Registration No.");
                int dim1Pos = log.GetPos("Global Dimension 1 Code");
                int dim2Pos = log.GetPos("Global Dimension 2 Code");
                //int maxCredPos = log.GetPos("Credit Limit (LCY)");
                //int cosPriceGrpPos = log.GetPos("Customer Price Group");
                int countryCodePos = log.GetPos("Country/Region Code");
                int curCodePos = log.GetPos("Currency Code");
                int paymentMethPos = log.GetPos("Payment Terms Code");
                int purchaserCodePos = log.GetPos("Purchaser Code");
                int shipCodePos = log.GetPos("Shipment Method Code");
                int vatPos = log.GetPos("VAT Bus. Posting Group");

                var InvoiceAccs = new List<c5.InvoiceAccounts>();

                var lst = new Dictionary<string, Creditor>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(19)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new Creditor();
                    rec._Account = Nav.GetField(lines, accountPos);
                    if (string.IsNullOrWhiteSpace(rec._Account))
                        continue;

                    rec._Group = grpCache.Get(Nav.GetField(lines, groupPos))?.KeyStr;
                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, namePos));
                    rec._Address1 = importLog.GetNotEmpty(Nav.GetField(lines, addressPos));
                    rec._Address2 = importLog.GetNotEmpty(Nav.GetField(lines, address2Pos));
                    rec._ZipCode = importLog.GetNotEmpty(Nav.GetField(lines, zipPos));
                    rec._City = importLog.GetNotEmpty(Nav.GetField(lines, cityPos));
                    rec._LegalIdent = importLog.GetNotEmpty(Nav.GetField(lines, VATNoPos));
                    rec._ContactEmail = importLog.GetNotEmpty(Nav.GetField(lines, emailPos));
                    rec._ContactPerson = importLog.GetNotEmpty(Nav.GetField(lines, contactPersPos));
                    rec._Phone = importLog.GetNotEmpty(Nav.GetField(lines, phoneNoPos));
                    rec._Payment = Payments?.Get(Nav.GetField(lines, paymentMethPos))?.KeyStr;
                    rec._Employee = Employees?.Get(Nav.GetField(lines, purchaserCodePos))?.KeyStr;
                    rec._Shipment = Shipment?.Get(Nav.GetField(lines, shipCodePos))?.KeyStr;
                    // rec._Vat = log.GetVat_Validate(lines[24]);

                    //if (lines.Count > (58 + 2))
                    //    rec._EAN = importLog.GetNotEmpty(Nav.GetField(lines, EANNoPos));

                    //rec._Vat = log.GetVat_Validate(Nav.GetField(lines, VATNoPos));
                    rec._Dim1 = dim1?.Get(Nav.GetField(lines, dim1Pos))?.KeyStr;
                    rec._Dim2 = dim2?.Get(Nav.GetField(lines, dim2Pos))?.KeyStr;

                    //if (Nav.GetField(lines, 42) != string.Empty)
                    //    rec._CreditMax = importLog.ToDouble(Nav.GetField(lines, maxCredPos));

                    //if (PriceLists != null)
                    //{
                    //    var priceRec = (InvPriceList)PriceLists.Get(Nav.GetField(lines, cosPriceGrpPos));
                    //    if (priceRec != null)
                    //    {
                    //        rec._PriceList = priceRec.KeyStr;
                    //        rec._PricesInclVat = priceRec._InclVat;
                    //    }
                    //}

                    rec._Country = c5.convertCountry(Nav.GetField(lines, countryCodePos), log.CompCountryCode,
                        out rec._VatZone);
                    rec._Currency = log.ConvertCur(Nav.GetField(lines, curCodePos));

                    if (!lst.ContainsKey(rec.KeyStr))
                    {
                        lst.Add(rec.KeyStr, rec);
                        if (Nav.GetField(lines, ourAccPos) != string.Empty)
                            InvoiceAccs.Add(new c5.InvoiceAccounts
                            {
                                Acc = rec._Account,
                                InvAcc = Nav.GetField(lines, ourAccPos)
                            });
                    }
                }

                await log.Insert(lst.Values);
                log.HasCreditor = true;
                var accs = lst.Values.ToArray();
                log.Creditors = new SQLCache(accs, true);

                c5.UpdateInvoiceAccount(log, InvoiceAccs, true);

                return ErrorCodes.Succes;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }
        }

        public static async Task importContact(importLog log)
        {

            if (!log.OpenFile("5050 - Contact", "kontakter"))
            {
                log.AppendLogLine(String.Format(Localization.lookup("FileNotFound"),
                    "5050 - Contact" + " : kontakter"));
                return;
            }

            try
            {

                int noPos = log.GetPos("No.");
                int namePos = log.GetPos("Name");
                int addressPos = log.GetPos("Address");
                int address2Pos = log.GetPos("Address 2");
                int cityPos = log.GetPos("City");
                int zipPos = log.GetPos("Post Code");
                int countryRegPos = log.GetPos("Country / Region Code");
                int phoneNoPos = log.GetPos("Phone No.");
                int currPos = log.GetPos("Currency Code");
                int salespersonPos = log.GetPos("Salesperson Code");
                int vatRegistrationPos = log.GetPos("VAT Registration No.");
                int emailPos = log.GetPos("E-Mail");
                int comNoPos = log.GetPos("Company No.");
                int companyNamePos = log.GetPos("Company Name");
                int firstNamePos = log.GetPos("First Name");
                int middleNamePos = log.GetPos("Middle Name");
                int surNamePos = log.GetPos("Surname");

                List<string> lines;
                var lst = new List<Contact>();
                while ((lines = log.GetLine(10)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new Contact();

                    rec._Name = Nav.GetField(lines, namePos);
                    if (string.IsNullOrWhiteSpace(rec._Name))
                    {
                        rec._Name = Nav.GetField(lines, comNoPos);
                        if (string.IsNullOrWhiteSpace(rec._Name))
                        {
                            rec._Name = Nav.GetField(lines, noPos);

                            if (string.IsNullOrWhiteSpace(rec._Name))
                                continue;
                        }
                    }
                    rec._Mobil = Nav.GetField(lines, phoneNoPos);
                    rec._Email = Nav.GetField(lines, emailPos);

                    //if (Nav.GetField(lines, 1) != "0")
                    //    rec._AccountStatement = rec._InterestNote = rec._CollectionLetter = rec._Invoice = true;
                    lst.Add(rec);
                }
                await log.Insert(lst, true);
                if (lst.Count > 0)
                    log.HasContact = true;

            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importShipment(importLog log)
        {
            if (!log.OpenFile("10 - Shipment Method", "Forsendelser"))
            {
                return;
            }

            try
            {
                List<string> lines;

                int codePos = log.GetPos("Code");
                int descriptionPos = log.GetPos("Description");

                var lst = new Dictionary<string, ShipmentType>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(2)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2")
                        continue;

                    var rec = new ShipmentType();
                    rec._Number = Nav.GetField(lines, codePos);
                    if (rec._Number == string.Empty)
                        continue;
                    rec._Name = Nav.GetField(lines, descriptionPos);

                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }
                await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.Shipments = new SQLCache(accs, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importEmployee(importLog log)
        {
            if (!log.OpenFile("5200 - Employee", "Medarbejder"))
            {
                return;
            }

            try
            {
                List<string> lines;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;

                int codePos = log.GetPos("No.");
                int fnamePos = log.GetPos("First Name");
                int mnamePos = log.GetPos("Middle Name");
                int lnamePos = log.GetPos("Last Name");
                int addePos = log.GetPos("Address");
                int add2Pos = log.GetPos("Address 2");
                int cityPos = log.GetPos("City");
                int postPos = log.GetPos("Post Code");
                int countryPos = log.GetPos("Country/Region Code");
                int empDatePos = log.GetPos("Employment Date");
                int jobTitlePos = log.GetPos("Job Title");
                int dim1Pos = log.GetPos("Global Dimension 1 Code");
                int dim2Pos = log.GetPos("Global Dimension 2 Code");
                int phoneNoPos = log.GetPos("Phone No."); 
                int mobphoneNoPos = log.GetPos("Mobile Phone No.");
                int emailPos = log.GetPos("E-Mail");
                int email2Pos = log.GetPos("E-Mail 2");

                var lst = new Dictionary<string, Employee>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(7)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new Employee();
                    rec._Number = importLog.GetNotEmpty(Nav.GetField(lines, codePos));
                    if (rec._Number == null)
                        continue;

                    rec._Name = Nav.GetField(lines, fnamePos) + " " + Nav.GetField(lines, mnamePos) + " " + Nav.GetField(lines, lnamePos);
                    rec._Address1 = Nav.GetField(lines, addePos);
                    rec._Address2 = Nav.GetField(lines, add2Pos);
                    rec._City = Nav.GetField(lines, cityPos);
                    rec._ZipCode = Nav.GetField(lines, postPos);
                    
                    rec._Mobil = importLog.GetNotEmpty(Nav.GetField(lines, mobphoneNoPos));

                    if (!string.IsNullOrWhiteSpace(Nav.GetField(lines, emailPos)))
                        rec._Email = importLog.GetNotEmpty(Nav.GetField(lines, emailPos));
             
                    rec._Title = ContactTitle.Employee;

                    rec._Dim1 = dim1?.Get(Nav.GetField(lines, dim1Pos))?.KeyStr;
                    rec._Dim2 = dim2?.Get(Nav.GetField(lines, dim2Pos))?.KeyStr;


                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }
                var err = await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.Employees = new SQLCache(accs, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static void importCompany(importLog log)
        {
            if (!log.OpenFile("79 - Company Information", "Firmaoplysninger"))
            {
                return;
            }

            try
            {
                List<string> lines;

                int namePos = log.GetPos("Name");
                int addressPos = log.GetPos("Address");
                int address2Pos = log.GetPos("Address 2");
                int cityPos = log.GetPos("City");
                int zipPos = log.GetPos("Post Code");
                int countryRegPos = log.GetPos("Country / Region Code");
                int phoneNoPos = log.GetPos("Phone No.");
                int vatRegistrationPos = log.GetPos("VAT Registration No.");
                int emailPos = log.GetPos("E-Mail");
                int bankNamePos = log.GetPos("Bank Name");
                int giroPos = log.GetPos("Giro No.");
                int bankAccountPos = log.GetPos("Bank Account No.");
                int IBANPos = log.GetPos("IBAN");
                int SWIFTPos = log.GetPos("SWIFT Code");

                while ((lines = log.GetLine(14)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = log.api.CompanyEntity;
                    rec._Address1 = Nav.GetField(lines, addressPos);
                    rec._Address2 = Nav.GetField(lines, address2Pos);
                    rec._Address3 = Nav.GetField(lines, cityPos) + " " + Nav.GetField(lines, countryRegPos);
                    rec._Phone = Nav.GetField(lines, phoneNoPos);
                    rec._Id = Nav.GetField(lines, vatRegistrationPos);
                    rec._NationalBank = Nav.GetField(lines, bankAccountPos);
                    if (string.IsNullOrWhiteSpace(rec._FIK))
                        rec._FIK = Nav.GetField(lines, giroPos);
                    rec._IBAN = Nav.GetField(lines, IBANPos);
                    rec._SWIFT = Nav.GetField(lines, SWIFTPos);
                    rec._FIKDebtorIdPart = 7;

                    //if (log.HasDepartment)
                    //{
                    //    rec.NumberOfDimensions = 1;
                    //    rec._Dim1 = "Afdeling";
                    //}
                    //if (log.HasCentre)
                    //{
                    //    rec.NumberOfDimensions = 2;
                    //    rec._Dim2 = "Bærer";
                    //}
                    //if (log.HasPorpose)
                    //{
                    //    rec.NumberOfDimensions = 3;
                    //    rec._Dim3 = "Formål";
                    //}

                    // log.NumberOfDimensions = rec.NumberOfDimensions;
                    log.api.UpdateNoResponse(rec);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static async Task importOrder(importLog log, Dictionary<long, DebtorOrder> orders)
        {
            if (!log.OpenFile("36 - Sales Header", "Salgsordre"))
            {
                return;
            }

            try
            {
                var Debtors = log.Debtors;
                var Employees = log.Employees;

                int billToCustomerPos = log.GetPos("Bill-to Customer No.");
                int codePos = log.GetPos("No.");
                int shipNamePos = log.GetPos("Ship-to Name");
                int shipAddPos = log.GetPos("Ship-to Address");
                int shipAdd2Pos = log.GetPos("Ship-to Address 2");
                int shipCityPos = log.GetPos("Ship-to City");
                int shipContactPos = log.GetPos("Ship-to Contact");
                int orderDatePos = log.GetPos("Order Date");
                int postDatePos = log.GetPos("Posting Date");
                int shipDatePos = log.GetPos("Shipment Date");
                int paymentTermsPos = log.GetPos("Payment Terms Code");
                int dueDatePos = log.GetPos("Due Date");
                int payDiscPos = log.GetPos("Payment Discount %");
                int shipMethPos = log.GetPos("Shipment Method Code");
                int dim1Pos = log.GetPos("Shortcut Dimension 1 Code");
                int dim2Pos = log.GetPos("Shortcut Dimension 2 Code");
                int cusPostPos = log.GetPos("Customer Posting Group");
                int currPos = log.GetPos("Currency Code");
                int currFacPos = log.GetPos("Currency Factor");
                int cusPricePos = log.GetPos("Customer Price Group");
                int languagePos = log.GetPos("Language Code");
                int empCodePos = log.GetPos("Salesperson Code");
                int incVatPos = log.GetPos("Prices Including VAT");
                int shipNoPos = log.GetPos("Shipping No.");
                int postingNoPos = log.GetPos("Posting No.");
                int lastShipNoPos = log.GetPos("Last Shipping No.");
                int lastPostingNoPos = log.GetPos("Last Posting No.");
                int vatRegNoPos = log.GetPos("VAT Registration No.");
                int busPostGroupPos = log.GetPos("Gen. Bus. Posting Group");
                int euThirdPartyPos = log.GetPos("EU 3-Party Trade.");
                int transTypePos = log.GetPos("Transaction Type");
                int transMethPos = log.GetPos("Transaction Method");
                int vatCountryPos = log.GetPos("VAT Country/Region Code");
                int refPos = log.GetPos("Your Reference");

                List<string> lines;
                int MaxOrderNumber = 0;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;
                var Payments = log.Payments;
                var PriceLists = log.PriceLists;
                var Shipment = log.Shipments;

                while ((lines = log.GetLine(30)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new DebtorOrder();
                    rec._OrderNumber = (int) NumberConvert.ToInt(Nav.GetField(lines, codePos).Replace("-", ""));
                    if (rec._OrderNumber == 0)
                        continue;
                    if (rec._OrderNumber > MaxOrderNumber)
                        MaxOrderNumber = rec._OrderNumber;

                    var deb = (Debtor) Debtors.Get(Nav.GetField(lines, billToCustomerPos));
                    if (deb == null)
                        continue;
                    rec.SetMaster(deb);

                    var inclVat = Nav.GetField(lines, incVatPos).ToLower();
                    if (inclVat == "yes" || inclVat == "ja" || inclVat == Uniconta.ClientTools.Localization.lookup("Yes").ToLower())
                        rec._PricesInclVat = true;
                    else
                        rec._PricesInclVat = false;

                    rec._DeliveryAddress1 = importLog.GetNotEmpty(Nav.GetField(lines, shipAddPos));
                    if (rec._DeliveryAddress1 != null)
                        log.HasDelivery = true;
                    rec._DeliveryAddress2 = importLog.GetNotEmpty(Nav.GetField(lines, shipAdd2Pos));
                    rec._DeliveryAddress3 = importLog.GetNotEmpty(Nav.GetField(lines, shipCityPos));

                    if (Nav.GetField(lines, vatCountryPos) != string.Empty)
                    {
                        VatZones vatz;
                        rec._DeliveryCountry = c5.convertCountry(Nav.GetField(lines, vatCountryPos),
                            log.CompCountryCode, out vatz);
                    }

                    if (Nav.GetField(lines, postDatePos) != string.Empty)
                        rec._DeliveryDate = eco.ParseDT(Nav.GetField(lines, postDatePos));
                    if (Nav.GetField(lines, orderDatePos) != string.Empty)
                        rec._Created = eco.ParseDT(Nav.GetField(lines, orderDatePos));

                    rec._Currency = log.ConvertCur(Nav.GetField(lines, currPos));

                    rec._EndDiscountPct = importLog.ToDouble(Nav.GetField(lines, payDiscPos));

                    //TODO: HVad betyder det nedenfor? (far)
                    //var val = importLog.ToDouble(Nav.GetField(lines, 27]);
                    //rec._DeleteLines =
                    //rec._DeleteOrder = (val <= 2);

                    //rec._Remark = importLog.GetNotEmpty(Nav.GetField(lines, 23]);
                    rec._YourRef = importLog.GetNotEmpty(Nav.GetField(lines, refPos));
                    //rec._OurRef = importLog.GetNotEmpty(Nav.GetField(lines, 38]);
                    //rec._Requisition = importLog.GetNotEmpty(Nav.GetField(lines, 39]);

                    rec._Payment = Payments?.Get(Nav.GetField(lines, paymentTermsPos))?.KeyStr;
                    rec._Employee = Employees?.Get(Nav.GetField(lines, empCodePos))?.KeyStr;
                    rec._Shipment = Shipment?.Get(Nav.GetField(lines, shipMethPos))?.KeyStr;

                    rec._Dim1 = dim1?.Get(Nav.GetField(lines, dim1Pos))?.KeyStr;
                    rec._Dim2 = dim2?.Get(Nav.GetField(lines, dim2Pos))?.KeyStr;

                    orders.Add(rec._OrderNumber, rec);
                }

                await log.Insert(orders.Values);

                var arr = await log.api.Query<CompanySettings>();
                if (arr != null && arr.Length > 0)
                {
                    arr[0]._SalesOrder = MaxOrderNumber;
                    log.api.UpdateNoResponse(arr[0]);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static async Task importOrderLinje(importLog log, Dictionary<long, DebtorOrder> orders)
        {
            if (!log.OpenFile("37 - Sales Line", "Ordrelinjer"))
            {
                return;
            }

            try
            {
                var Items = log.Items;
                if (Items == null)
                    return;

                int orderNoPos = log.GetPos("Document No.");
                int lineNoPos = log.GetPos("Line No.");
                int typePos = log.GetPos("Type");
                int itemNoPos = log.GetPos("No.");
                int postGroupPos = log.GetPos("Posting Group");
                int shipDatePos = log.GetPos("Shipment Date");
                int descriptionPos = log.GetPos("Description");
                int unitMeasurePos = log.GetPos("Unit of Measure");
                int qtyPos = log.GetPos("Quantity");
                int unitPricePos = log.GetPos("Unit Price");
                int unitCostPos = log.GetPos("Unit Cost (LCY)");
                int vatprocPos = log.GetPos("VAT %");
                int lineDiscProcPos = log.GetPos("Line Discount %");
                int lineDiscAmountPos = log.GetPos("Line Discount Amount");
                int amountPos = log.GetPos("Amount");
                int amountInclVATPos = log.GetPos("Amount Including VAT");
                int grossWeightPos = log.GetPos("Gross Weight");
                int netWeightPos = log.GetPos("Net Weight");
                int unitVolumePos = log.GetPos("Unit Volume");
                int dim1Pos = log.GetPos("Shortcut Dimension 1 Code");
                int dim2Pos = log.GetPos("Shortcut Dimension 1 Code");
                int cusPriceGroupPos = log.GetPos("Customer Price Group");
                int billCusNoPos = log.GetPos("Bill-to Customer No.");
                int purchOrderNoPos = log.GetPos("Purchase Order No.");
                int busPostGroupPos = log.GetPos("Gen. Bus. Posting Group");
                int prodPostGroupPos = log.GetPos("Gen. Prod. Posting Group");
                int vatProdPostGroupPos = log.GetPos("VAT. Prod. Posting Group");
                int vatBusPostGroupPos = log.GetPos("VAT Bus. Posting Group");
                int currCodePos = log.GetPos("Currency Code");
                int lineAmountPos = log.GetPos("Line Amount");

                List<string> lines;

                var lst = new List<DebtorOrderLine>();

                while ((lines = log.GetLine(22)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var OrderNumber = NumberConvert.ToInt(Nav.GetField(lines, orderNoPos).Replace("-", ""));
                    if (OrderNumber == 0)
                        continue;

                    DebtorOrder order;
                    if (!orders.TryGetValue(OrderNumber, out order))
                        continue;

                    var rec = new DebtorOrderLine();
                    rec._LineNumber = NumberConvert.ToInt(Nav.GetField(lines, lineNoPos));

                    InvItem item;
                    if (!string.IsNullOrEmpty(Nav.GetField(lines, itemNoPos)))
                    {
                        item = (InvItem) Items.Get(Nav.GetField(lines, itemNoPos));
                        if (item == null)
                            continue;
                        rec._Item = item._Item;
                        rec._CostPrice = item._CostPrice;
                    }
                    else
                        item = null;
                    rec._Text = importLog.GetNotEmpty(Nav.GetField(lines, descriptionPos));
                    rec._Qty = importLog.ToDouble(Nav.GetField(lines, qtyPos));
                    rec._Price = importLog.ToDouble(Nav.GetField(lines, unitPricePos));
                    rec._DiscountPct = importLog.ToDouble(Nav.GetField(lines, lineDiscProcPos));

                    var amount = importLog.ToDouble(Nav.GetField(lines, amountPos));
                    if (rec._Price * rec._Qty == 0d && amount != 0)
                        rec._AmountEntered = amount;

                    //TODO: Forstår ikke QtyNow? (far)
                    //rec._QtyNow = importLog.ToDouble(Nav.GetField(lines, 11]);
                    //if (rec._QtyNow == rec._Qty)
                    //    rec._QtyNow = 0d;

                    rec._Date = eco.ParseDT(Nav.GetField(lines, shipDatePos));

                    rec._Unit = importLog.ConvertUnit(Nav.GetField(lines, unitMeasurePos));
                    if (item != null && item._Unit == rec._Unit)
                        rec._Unit = 0;

                    rec._Currency = string.IsNullOrWhiteSpace(Nav.GetField(lines, currCodePos))
                        ? order._Currency
                        : log.ConvertCur(Nav.GetField(lines, currCodePos));

                    if (rec._CostPrice == 0d)
                        rec._CostPrice = importLog.ToDouble(Nav.GetField(lines, unitCostPos));

                    rec.SetMaster(order);
                    lst.Add(rec);
                }

                await log.Insert(lst, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static async Task importPurchage(importLog log, Dictionary<long, CreditorOrder> purchages)
        {
            if (!log.OpenFile("38 - Purchase Header", "Indkøbssordre"))
            {
                return;
            }

            try
            {
                var Creditors = log.Creditors;
                var Employees = log.Employees;
                var Shipment = log.Shipments;

                int payToVendorPos = log.GetPos("Pay-to Vendor No.");
                int codePos = log.GetPos("No.");
                int shipNamePos = log.GetPos("Ship-to Name");
                int shipAddPos = log.GetPos("Ship-to Address");
                int shipAdd2Pos = log.GetPos("Ship-to Address 2");
                int shipCityPos = log.GetPos("Ship-to City");
                int shipContactPos = log.GetPos("Pay-to Contact");
                int orderDatePos = log.GetPos("Order Date");
                int postDatePos = log.GetPos("Posting Date");
                int receiptDatePos = log.GetPos("Expected Receipt Date");
                int paymentTermsPos = log.GetPos("Payment Terms Code");
                int dueDatePos = log.GetPos("Due Date");
                int payDiscPos = log.GetPos("Payment Discount %");
                int shipMethPos = log.GetPos("Shipment Method Code");
                int dim1Pos = log.GetPos("Shortcut Dimension 1 Code");
                int dim2Pos = log.GetPos("Shortcut Dimension 2 Code");
                int cusPostPos = log.GetPos("Vendor Posting Group");
                int currPos = log.GetPos("Currency Code");
                int currFacPos = log.GetPos("Currency Factor");
                int cusPricePos = log.GetPos("Customer Price Group");
                int languagePos = log.GetPos("Language Code");
                int empCodePos = log.GetPos("Purchaser Code");
                int incVatPos = log.GetPos("Prices Including VAT");
                int shipNoPos = log.GetPos("Shipping No.");
                int postingNoPos = log.GetPos("Posting No.");
                int lastShipNoPos = log.GetPos("Last Shipping No.");
                int lastPostingNoPos = log.GetPos("Last Posting No.");
                int vatRegNoPos = log.GetPos("VAT Registration No.");
                int busPostGroupPos = log.GetPos("Gen. Bus. Posting Group");
                int euThirdPartyPos = log.GetPos("EU 3-Party Trade.");
                int transTypePos = log.GetPos("Transaction Type");
                int transMethPos = log.GetPos("Transaction Method");
                int vatCountryPos = log.GetPos("VAT Country/Region Code");
                int refPos = log.GetPos("Your Reference");

                List<string> lines;
                int MaxOrderNumber = 0;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;
                var Payments = log.Payments;
                var PriceLists = log.PriceLists;

                while ((lines = log.GetLine(30)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new CreditorOrder();
                    rec._OrderNumber = (int) NumberConvert.ToInt(Nav.GetField(lines, codePos).Replace("-", ""));
                    if (rec._OrderNumber == 0)
                        continue;

                    if (rec._OrderNumber > MaxOrderNumber)
                        MaxOrderNumber = rec._OrderNumber;

                    var cre = (Creditor) Creditors.Get(Nav.GetField(lines, payToVendorPos));
                    if (cre == null)
                        continue;

                    rec.SetMaster(cre);

                    var inclVat = Nav.GetField(lines, incVatPos).ToLower();
                    if (inclVat == "yes" || inclVat == "ja" || inclVat == Uniconta.ClientTools.Localization.lookup("Yes").ToLower())
                        rec._PricesInclVat = true;
                    else
                        rec._PricesInclVat = false;

                    rec._DeliveryAddress1 = importLog.GetNotEmpty(Nav.GetField(lines, shipAddPos));
                    if (rec._DeliveryAddress1 != null)
                        log.HasDelivery = true;
                    rec._DeliveryAddress2 = importLog.GetNotEmpty(Nav.GetField(lines, shipAdd2Pos));
                    rec._DeliveryAddress3 = importLog.GetNotEmpty(Nav.GetField(lines, shipCityPos));
                    if (Nav.GetField(lines, vatCountryPos) != string.Empty)
                    {
                        VatZones vatz;
                        rec._DeliveryCountry = c5.convertCountry(Nav.GetField(lines, vatCountryPos),
                            log.CompCountryCode, out vatz);
                    }

                    if (Nav.GetField(lines, postDatePos) != string.Empty)
                        rec._DeliveryDate = eco.ParseDT(Nav.GetField(lines, postDatePos));
                    if (Nav.GetField(lines, orderDatePos) != string.Empty)
                        rec._Created = eco.ParseDT(Nav.GetField(lines, orderDatePos));

                    rec._Currency = log.ConvertCur(Nav.GetField(lines, currPos));

                    rec._EndDiscountPct = importLog.ToDouble(Nav.GetField(lines, payDiscPos));

                    //TODO: HVad betyder det nedenfor? (far)
                    //var val = importLog.ToDouble(Nav.GetField(lines, 27]);
                    //rec._DeleteLines =
                    //rec._DeleteOrder = (val <= 2);

                    //rec._Remark = importLog.GetNotEmpty(Nav.GetField(lines, 23]);
                    rec._YourRef = importLog.GetNotEmpty(Nav.GetField(lines, refPos));
                    //rec._OurRef = importLog.GetNotEmpty(Nav.GetField(lines, 38]);
                    //rec._Requisition = importLog.GetNotEmpty(Nav.GetField(lines, 39]);

                    rec._Payment = Payments?.Get(Nav.GetField(lines, paymentTermsPos))?.KeyStr;
                    rec._Employee = Employees?.Get(Nav.GetField(lines, empCodePos))?.KeyStr;
                    rec._Shipment = Shipment?.Get(Nav.GetField(lines, shipMethPos))?.KeyStr;

                    rec._Dim1 = dim1?.Get(Nav.GetField(lines, dim1Pos))?.KeyStr;
                    rec._Dim2 = dim2?.Get(Nav.GetField(lines, dim2Pos))?.KeyStr;

                    purchages.Add(rec._OrderNumber, rec);
                }

                await log.Insert(purchages.Values);

                var arr = await log.api.Query<CompanySettings>();
                if (arr != null && arr.Length > 0)
                {
                    arr[0]._PurchaceOrder = MaxOrderNumber;
                    log.api.UpdateNoResponse(arr[0]);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static async Task importPurchageLinje(importLog log, Dictionary<long, CreditorOrder> purchages)
        {
            if (!log.OpenFile("39 - Purchase Line", "Indkøbslinjer"))
            {
                return;
            }

            try
            {
                var Items = log.Items;
                if (Items == null)
                    return;

                int orderNoPos = log.GetPos("Document No.");
                int lineNoPos = log.GetPos("Line No.");
                int typePos = log.GetPos("Type");
                int itemNoPos = log.GetPos("No.");
                int postGroupPos = log.GetPos("Posting Group");
                int receiptDatePos = log.GetPos("Expected Receipt Date");
                int descriptionPos = log.GetPos("Description");
                int unitMeasurePos = log.GetPos("Unit of Measure");
                int qtyPos = log.GetPos("Quantity");
                int unitPricePos = log.GetPos("Unit Price (LCY)");
                int directUnitCostPos = log.GetPos("Direct Unit Cost");
                int unitCostPos = log.GetPos("Unit Cost (LCY)");
                int vatprocPos = log.GetPos("VAT %");
                int lineDiscProcPos = log.GetPos("Line Discount %");
                int lineDiscAmountPos = log.GetPos("Line Discount Amount");
                int amountPos = log.GetPos("Amount");
                int amountInclVATPos = log.GetPos("Amount Including VAT");
                int grossWeightPos = log.GetPos("Gross Weight");
                int netWeightPos = log.GetPos("Net Weight");
                int unitVolumePos = log.GetPos("Unit Volume");
                int dim1Pos = log.GetPos("Shortcut Dimension 1 Code");
                int dim2Pos = log.GetPos("Shortcut Dimension 1 Code");
                int vendorItemNoPos = log.GetPos("Vendor Item No.");
                int payVendorNoPos = log.GetPos("Pay-to Vendor No.");
                int salesOrderNoPos = log.GetPos("Sales Order No.");
                int salesOrderLineNoPos = log.GetPos("Sales Order Line No.");
                int busPostGroupPos = log.GetPos("Gen. Bus. Posting Group");
                int prodPostGroupPos = log.GetPos("Gen. Prod. Posting Group");
                int vatProdPostGroupPos = log.GetPos("VAT. Prod. Posting Group");
                int vatBusPostGroupPos = log.GetPos("VAT Bus. Posting Group");
                int currCodePos = log.GetPos("Currency Code");
                int lineAmountPos = log.GetPos("Line Amount");

                List<string> lines;

                var lst = new List<CreditorOrderLine>();

                while ((lines = log.GetLine(22)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var OrderNumber = NumberConvert.ToInt(Nav.GetField(lines, orderNoPos).Replace("-", ""));
                    if (OrderNumber == 0)
                        continue;

                    CreditorOrder order;
                    if (!purchages.TryGetValue(OrderNumber, out order))
                        continue;

                    var rec = new CreditorOrderLine();
                    rec._LineNumber = NumberConvert.ToInt(Nav.GetField(lines, lineNoPos));

                    InvItem item;
                    if (!string.IsNullOrEmpty(Nav.GetField(lines, itemNoPos)))
                    {
                        item = (InvItem) Items.Get(Nav.GetField(lines, itemNoPos));
                        if (item == null)
                            continue;
                        rec._Item = item._Item;
                        //rec._CostPrice = item._CostPrice;
                    }
                    else
                        item = null;

                    rec._Text = importLog.GetNotEmpty(Nav.GetField(lines, descriptionPos));
                    rec._Qty = importLog.ToDouble(Nav.GetField(lines, qtyPos));
                    rec._Price = importLog.ToDouble(Nav.GetField(lines, unitPricePos));
                    rec._DiscountPct = importLog.ToDouble(Nav.GetField(lines, lineDiscProcPos));

                    var amount = importLog.ToDouble(Nav.GetField(lines, amountPos));
                    if (rec._Price * rec._Qty == 0d && amount != 0)
                        rec._AmountEntered = amount;

                    //TODO: Forstår ikke QtyNow? (far)
                    //rec._QtyNow = importLog.ToDouble(Nav.GetField(lines, 11]);
                    //if (rec._QtyNow == rec._Qty)
                    //    rec._QtyNow = 0d;

                    rec._Date = eco.ParseDT(Nav.GetField(lines, receiptDatePos));

                    rec._Unit = importLog.ConvertUnit(Nav.GetField(lines, unitMeasurePos));
                    if (item != null && item._Unit == rec._Unit)
                        rec._Unit = 0;

                    rec._Currency = string.IsNullOrWhiteSpace(Nav.GetField(lines, currCodePos))
                        ? order._Currency
                        : log.ConvertCur(Nav.GetField(lines, currCodePos));

                    //if (rec._CostPrice == 0d)
                    //    rec._CostPrice = importLog.ToDouble(Nav.GetField(lines, unitCostPos));

                    rec.SetMaster(order);
                    lst.Add(rec);
                }

                await log.Insert(lst, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task<ErrorCodes> importGLAccount(importLog log, string errorAccount)
        {
            if (!log.OpenFile("15 - G_L Account", "Finanskonti"))
            {
                return ErrorCodes.Succes;
            }

            try
            {
                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;

                var PriceLists = log.PriceLists;
                var Employees = log.Employees;
                var Payments = log.Payments;
                var grpCache = log.DebGroups;

                int accountPos = log.GetPos("No.");
                int namePos = log.GetPos("Name");
                int searchNamePos = log.GetPos("Search Name");
                int accountTypePos = log.GetPos("Account Type");
                int dim1Pos = log.GetPos("Global Dimension 1 Code");
                int dim2Pos = log.GetPos("Global Dimension 2 Code");
                int incomeBalancePos = log.GetPos("Income/Balance");
                int debCredPos = log.GetPos("Debit/Credit");
                int account2Pos = log.GetPos("No. 2");
                int blockPos = log.GetPos("Blocked");
                int dirPostPos = log.GetPos("Direct Posting");
                int recAccPos = log.GetPos("Reconciliation Account");
                int newPagePos = log.GetPos("New Page");
                int genPostTypePos = log.GetPos("Gen. Posting Type");
                int noBlankLinesPos = log.GetPos("No. of Blank Lines");
                int indentationPos = log.GetPos("Indentation");
                int lastDateModPos = log.GetPos("Last Date Modified");
                int totalingPos = log.GetPos("Totaling");
                int conTransMethPos = log.GetPos("Consol. Translation Method");
                int legealIdentPos = log.GetPos("Our Account No.");
                int VATProdNoPos = log.GetPos("VAT Prod. Posting Group");
                int VATBusNoPos = log.GetPos("VAT Bus.. Posting Group");


                var lst = new Dictionary<string, MyGLAccount>(StringNoCaseCompare.GetCompare());
                var InvoiceAccs = new List<c5.InvoiceAccounts>();
                
                List<string> lines;
                while ((lines = log.GetLine(20)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4")
                        continue;

                    var rec = new MyGLAccount();
                    rec._Account = Nav.GetField(lines, accountPos);
                    if (string.IsNullOrWhiteSpace(rec._Account))
                        continue;

                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, namePos));

                    //var vat = Nav.GetField(lines, VATBusNoPos) + Nav.GetField(lines, VATProdNoPos) + "i";
                    //rec.VatKode = log.GetVat_Validate(vat);
                    //if (rec.v)
                    //{

                    //}

                    var accountType = Nav.GetField(lines, accountTypePos);
                    string postType = Nav.GetField(lines, genPostTypePos);

                    switch (postType.ToLower())
                    {
                        case "1":
                        case "salg":
                        case "sale":
                        case "sales":
                            rec._AccountType = (byte) GLAccountTypes.Revenue;
                            break;
                        case "2":
                        case "buy":
                        case "purchase":
                        case "køb":
                            rec._AccountType = (byte) GLAccountTypes.CostOfGoodSold;
                            break;
                        default:
                            switch (accountType.ToLower())
                            {
                                case "konto":
                                case "posting":
                                case "0":
                                    var incomeBalance = Nav.GetField(lines, incomeBalancePos).ToLower();

                                    if (incomeBalance == "balance" || incomeBalance == "balance sheet" || incomeBalance == "1")
                                        rec._AccountType = (byte) GLAccountTypes.BalanceSheet;
                                    else if (incomeBalance == "resultatopgørelse" || incomeBalance == "income Statement" || incomeBalance == "0")
                                        rec._AccountType = (byte) GLAccountTypes.PL;

                                    var recAccount = Nav.GetField(lines, recAccPos).ToLower();
                                    if (recAccount == "yes" || recAccount == "ja" || recAccount == Uniconta.ClientTools.Localization.lookup("Yes").ToLower())
                                        rec._AccountType = (byte) GLAccountTypes.Bank;

                                    var debCred = Nav.GetField(lines, debCredPos).ToLower();
                                    if (debCred == Uniconta.ClientTools.Localization.lookup("Credit").ToLower() || debCred == "kredit" || debCred == "credit" || debCred == "2")
                                        rec._DebetCredit = DebitCreditPreference.Credit;
                                    else if (debCred == Uniconta.ClientTools.Localization.lookup("Debit").ToLower() || debCred == "debet" || debCred == "debit" || debCred == "1")
                                        rec._DebetCredit = DebitCreditPreference.Debet;
                                    break;
                                case "3":
                                case "fra-sum":
                                case "begin-total":
                                    rec._AccountType = (byte) GLAccountTypes.Header;
                                    rec._MandatoryTax = VatOptions.NoVat;
                                    break;
                                case "1":
                                case "overskrift":
                                case "heading":
                                    rec._AccountType = (byte) GLAccountTypes.Header;
                                    rec._PageBreak = true;
                                    rec._MandatoryTax = VatOptions.NoVat;
                                    break;

                                case "2":
                                case "4":
                                case "sum":
                                case "til-sum":
                                case "end-total":
                                    rec._AccountType = (byte) GLAccountTypes.Sum;
                                    rec._SumInfo = Nav.GetField(lines, totalingPos).Replace('|', ';');
                                    rec._MandatoryTax = VatOptions.NoVat;
                                    break;
                            }
                            break;
                    }

                    if (log.HasDim1)
                        rec.SetDimUsed(1, true);
                    if (log.HasDim2)
                        rec.SetDimUsed(2, true);
                    if (log.HasDim3)
                        rec.SetDimUsed(3, true);
                    if (log.HasDim4)
                        rec.SetDimUsed(4, true);
                    if (log.HasDim5)
                        rec.SetDimUsed(5, true);

                    if (rec.KeyStr == errorAccount)
                    {
                        rec._SystemAccount = (byte) SystemAccountTypes.ErrorAccount;
                    }

                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }

                if (!lst.ContainsKey(errorAccount))
                {
                    //Error account created
                    var errorAcc = new MyGLAccount();
                    errorAcc._Name = Localization.lookup("ErrorAccount");
                    errorAcc._AccountType = (byte) GLAccountTypes.BalanceSheet;
                    errorAcc._SystemAccount = (byte) SystemAccountTypes.ErrorAccount;
                    errorAcc._Account = errorAccount;
                    lst.Add(errorAccount, errorAcc);
                }
                log.errorAccount = errorAccount;

                await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.LedgerAccounts = new SQLCache(accs, true);

                return ErrorCodes.Succes;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }
        }

        public static async Task importDebPriceGroup(importLog log)
        {
            if (!log.OpenFile("7002 - Sales Price", "Debitor Prisgrupper"))
            {
                return;
            }

            List<string> lines;

            try
            {
                int itemNoPos = log.GetPos("Item No.");
                int salescodePos = log.GetPos("Sales Code");
                int currencyCodePos = log.GetPos("Currency Code");
                int startingDatePos = log.GetPos("Starting Date");
                int unitPricePos = log.GetPos("Unit Price");
                int priceIncludesVATPos = log.GetPos("Price Includes VAT");
                int salesTypePos = log.GetPos("Sales Type");
                int endingDatePos = log.GetPos("Ending Date");
                int measureCodePos = log.GetPos("Unit of Measure Code");

                var lst = new Dictionary<string, DebtorPriceList>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(3)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new DebtorPriceList();
                    rec._Name = Nav.GetField(lines, itemNoPos) + " " + Nav.GetField(lines, salescodePos);
                    if (string.IsNullOrWhiteSpace(rec._Name))
                        continue;

                    var inclVat = Nav.GetField(lines, priceIncludesVATPos).ToLower();

                    if (inclVat == "yes" || inclVat == "ja" || inclVat == Uniconta.ClientTools.Localization.lookup("Yes").ToLower())
                        rec._InclVat = true;
                    else
                        rec._InclVat = false;

                    rec._Currency = string.IsNullOrWhiteSpace(Nav.GetField(lines, currencyCodePos))
                        ? (byte) Currencies.XXX
                        : (byte) ((Currencies) Enum.Parse((typeof(Currencies)), Nav.GetField(lines, currencyCodePos)));
                    rec._ValidFrom = eco.ParseDT(Nav.GetField(lines, startingDatePos));
                    rec._ValidTo = eco.ParseDT(Nav.GetField(lines, endingDatePos));

                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }
                await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.PriceLists = new SQLCache(accs, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importCredPriceGroup(importLog log)
        {
            if (!log.OpenFile("7012 - Purchase Price", "Kreditor Prisgrupper"))
            {
                return;
            }

            List<string> lines;

            try
            {
                int itemNoPos = log.GetPos("Item No.");
                int vendorcodePos = log.GetPos("Vendor No.");
                int currencyCodePos = log.GetPos("Currency Code");
                int startingDatePos = log.GetPos("Starting Date");
                int unitPricePos = log.GetPos("Direct Unit Cost");
                // int priceIncludesVATPos = log.GetPos("Price Includes VAT");
                //int salesTypePos = log.GetPos("Sales Type");
                int endingDatePos = log.GetPos("Ending Date");
                int measureCodePos = log.GetPos("Unit of Measure Code");

                var lst = new Dictionary<string, CreditorPriceList>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(3)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new CreditorPriceList();
                    rec._Name = Nav.GetField(lines, itemNoPos) + " " + Nav.GetField(lines, vendorcodePos);
                    if (string.IsNullOrWhiteSpace(rec._Name))
                        continue;

                    //rec._InclVat = Nav.GetField(lines, priceIncludesVATPos] == "Yes";
                    rec._Currency = string.IsNullOrWhiteSpace(Nav.GetField(lines, currencyCodePos))
                        ? (byte) Currencies.XXX
                        : (byte) ((Currencies) Enum.Parse((typeof(Currencies)), Nav.GetField(lines, currencyCodePos)));
                    rec._ValidFrom = eco.ParseDT(Nav.GetField(lines, startingDatePos));
                    rec._ValidTo = eco.ParseDT(Nav.GetField(lines, endingDatePos));


                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }
                await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.PriceLists = new SQLCache(accs, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importInvGroup(importLog log)
        {

            if (!log.OpenFile("5813 - Inventory Posting Setup", "Varegrupper"))
            {
                return;
            }
            try
            {
                List<string> lines;
                var Ledger = log.LedgerAccounts;

                int localCodePos = log.GetPos("Location Code");
                int postGroupCodeCodePos = log.GetPos("Invt. Posting Group Code");
                int invAccountPos = log.GetPos("Inventory Account");
                int invAccount2Pos = log.GetPos("Inventory Account (Interim)");
                int wipAccountPos = log.GetPos("WIP Account");

                var lst = new Dictionary<string, InvGroup>(StringNoCaseCompare.GetCompare());
                var listOfGroups = new List<string>();
                while ((lines = log.GetLine(5)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new InvGroup();

                    rec._Group = Nav.GetField(lines, postGroupCodeCodePos);
                    if (string.IsNullOrWhiteSpace(rec._Group))
                        continue;
                    rec._Name = Nav.GetField(lines, postGroupCodeCodePos);

                    //rec._CostAccount = log.GLAccountFromC5_Validate(Nav.GetField(lines, 3]);
                    rec._InvAccount = log.GLAccountFromC5_Validate(Nav.GetField(lines, invAccountPos));
                    //rec._InvReceipt = log.GLAccountFromC5_Validate(Nav.GetField(lines, 10]);

                    //var Acc = (MyGLAccount)Ledger.Get(log.GLAccountFromC5(Nav.GetField(lines, 2]));
                    //if (Acc != null)
                    //{
                    //    rec._RevenueAccount = Acc._Account;
                    //    rec._SalesVat = log.GetVat_Validate(Acc.VatKode);
                    //}

                    var Acc = (MyGLAccount) Ledger.Get(log.GLAccountFromC5(Nav.GetField(lines, invAccountPos)));
                    if (Acc != null)
                    {
                        rec._PurchaseAccount = Acc._Account;
                        rec._PurchaseVat = log.GetVat_Validate(Acc.VatKode);
                    }

                    rec._UseFirstIfBlank = true;

                    if (!lst.Any())
                        rec._Default = true;

                    if (!lst.ContainsKey(rec.KeyStr) && !listOfGroups.Contains(rec._Group))
                    {
                        lst.Add(rec.KeyStr, rec);
                        listOfGroups.Add(rec._Group);
                    }

                }
                var err = await log.Insert(lst.Values);
                if (err == 0)
                {
                    var accs = lst.Values.ToArray();
                    log.ItemGroups = new SQLCache(accs, true);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task<ErrorCodes> importInv(importLog log)
        {
            if (!log.OpenFile("5940 - Service Item", "Service"))
            {
                return ErrorCodes.Succes;
            }
            var listOfServices = new List<string>();
            try
            {
                List<string> linesService;

                int itemNoPos = log.GetPos("Item No.");
                while ((linesService = log.GetLine(1)) != null)
                {
                    if (linesService[0] == "1" && linesService[1] == "2" && linesService[2] == "3")
                        continue;
                    
                    if (linesService.Count > 9)
                    {
                        var service = Nav.GetField(linesService, itemNoPos);
                        if (!string.IsNullOrWhiteSpace(service) && !listOfServices.Contains(service))
                            listOfServices.Add(service);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
            if (!log.OpenFile("27 - Item", "Varer"))
            {
                log.api.CompanyEntity.Inventory = false;
                return ErrorCodes.Succes;
            }

            try
            {
                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;

                var grpCache = log.ItemGroups;
                var Creditors = log.Creditors;

                int accountPos = log.GetPos("No.");
                int account2Pos = log.GetPos("No. 2");
                int descriptionPos = log.GetPos("Description");
                int searchDescriptionPos = log.GetPos("Search Description");
                int unitMeasurePos = log.GetPos("Base Unit of Measure");
                int dim1Pos = log.GetPos("Global Dimension 1 Code");
                int dim2Pos = log.GetPos("Global Dimension 2 Code");
                int invPostGroupPos = log.GetPos("Inventory Posting Group");
                int unitPricePos = log.GetPos("Unit Price");
                int unitCostPos = log.GetPos("Unit Cost");
                int stanCostPos = log.GetPos("Standard Cost");
                int vendorNoPos = log.GetPos("Vendor No.");
                int vendorItemNoPos = log.GetPos("Vendor Item No.");
                int netWeightPos = log.GetPos("Gross Weight");
                int unitsperParcelPos = log.GetPos("Units per Parcel");
                int tariffNoPos = log.GetPos("Tariff No.");
                int countryRegionPurchasedPos = log.GetPos("Country/Region Purchased Code");
                int countryRegionOriginCodePos = log.GetPos("Country/Region of Origin Code");
                int blockedPos = log.GetPos("Blocked");
                int unitvolumePos = log.GetPos("Unit Volume");
                int stockQtyPos = log.GetPos("Safety Stock Quantity");
                int legealIdentPos = log.GetPos("Our Account No.");
                int VATNoPos = log.GetPos("VAT Prod. Posting Group");
                int costingMethodPos = log.GetPos("Costing Method");
                int maxQtyPos = log.GetPos("Maximum Order Quantity");
                int minimumQtyPos = log.GetPos("Minimum Order Quantity");


                List<string> lines;
                var lst = new Dictionary<string, InvItemImport>(StringNoCaseCompare.GetCompare());
                var InvoiceAccs = new List<c5.InvoiceAccounts>();
                while ((lines = log.GetLine(23)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new InvItemImport();
                    rec._Item = Nav.GetField(lines, accountPos);
                    if (string.IsNullOrWhiteSpace(rec._Item))
                        continue;

                    rec._Group = grpCache.Get(Nav.GetField(lines, invPostGroupPos))?.KeyStr;
                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, descriptionPos));
                    rec._PurchaseAccount = Creditors?.Get(Nav.GetField(lines, vendorNoPos))?.KeyStr;

                    var blocked = Nav.GetField(lines, blockedPos).ToLower();
                    if (blocked == "yes" || blocked == "ja" || blocked == Uniconta.ClientTools.Localization.lookup("Yes").ToLower())
                        rec._Blocked = true;

                    rec._TariffNumber = Nav.GetField(lines, tariffNoPos);
                    rec._Volume = NumberConvert.ToDouble(Nav.GetField(lines, unitvolumePos));
                    rec._Unit = importLog.ConvertUnit(Nav.GetField(lines, unitMeasurePos));
                    rec._Weight = NumberConvert.ToDouble(Nav.GetField(lines, netWeightPos));

                    rec._StockPosition = importLog.GetNotEmpty(Nav.GetField(lines, stockQtyPos));
                    if (rec._StockPosition == "0")
                        rec._StockPosition = null;

                    rec._Dim1 = dim1?.Get(Nav.GetField(lines, dim1Pos))?.KeyStr;
                    rec._Dim2 = dim2?.Get(Nav.GetField(lines, dim2Pos))?.KeyStr;

                    rec._PurchasePrice = importLog.ToDouble(Nav.GetField(lines, stanCostPos));
                    rec._CostPrice = importLog.ToDouble(Nav.GetField(lines, unitCostPos));

                    var country = Nav.GetField(lines, countryRegionOriginCodePos);
                    VatZones vatZone;
                    rec._CountryOfOrigin = string.IsNullOrWhiteSpace(country)
                        ? CountryCode.Unknown
                        : c5.convertCountry(country, log.CompCountryCode, out vatZone);

                    switch (Nav.GetField(lines, costingMethodPos).ToLower())
                    {
                        case "0": //FIFO
                        case "fifo":
                        case "fifo-":
                        case "fifu":
                        case "1": //LIFO
                        case "lifo":
                            rec._CostModel = CostType.FIFO;
                            break;
                        case "2"://Average
                        case "average":
                        case "gennemsnit":
                            rec._CostModel = CostType.Average;
                            break; 
                        case "3": //Cost price 
                        case "4":
                        case "specific"://Cost price 
                        case "standard":
                        case "specifik":
                        case "bestemt":
                        case "konkret":
                            rec._CostModel = CostType.Fixed;
                            rec._CostModel = CostType.Fixed;
                            break; 
                    }

                    if (listOfServices.Contains(rec._Item))
                        rec._ItemType = (byte) ItemType.Service;
                    else
                        rec._ItemType = (byte) ItemType.Item;

                    rec._MaxStockLevel = NumberConvert.ToDouble(Nav.GetField(lines, maxQtyPos));
                    rec._MinStockLevel = NumberConvert.ToDouble(Nav.GetField(lines, minimumQtyPos));


                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }

                if (lst != null && lst.Count > 0)
                {
                    var err = await log.Insert(lst.Values);
                    if (err == 0)
                    {
                        log.HasItem = true;
                        var accs = lst.Values.ToArray();
                        log.Items = new SQLCache(accs, true);
                    }

                    return ErrorCodes.Succes;
                }
                return ErrorCodes.NoSucces;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }

        }

        public static async Task importInvBOM(importLog log)
        {
            if (!log.OpenFile("99000772 - Production BOM Line", "Styklister"))
            {
                return;
            }

            try
            {

                var Items = log.Items;
                List<string> lines;

                int productionBOMNoPos = log.GetPos("Production BOM No.");
                int lineNoPos = log.GetPos("Line No.");
                int partNoPos = log.GetPos("No.");
                int quantityoPos = log.GetPos("Quantity");

                var lst = new List<InvBOM>();
                while ((lines = log.GetLine(4)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4")
                        continue;

                    var item = Items?.Get(Nav.GetField(lines, productionBOMNoPos));
                    if (item == null)
                        continue;

                    var rec = new InvBOM();
                    rec._ItemMaster = item.KeyStr;

                    item = Items?.Get(Nav.GetField(lines, partNoPos));
                    if (item == null)
                        continue;
                    rec._ItemPart = item.KeyStr;

                    rec._LineNumber = NumberConvert.ToFloat(Nav.GetField(lines, lineNoPos));

                    var qty = Nav.GetField(lines, quantityoPos);
                    rec._Qty = NumberConvert.ToDouble(qty);
                    rec._QtyType = BOMQtyType.Propertional;
                    rec._MoveType = BOMMoveType.SalesAndBuy;

                    lst.Add(rec);
                }
                if (lst.Count > 0)
                {
                    var err = await log.Insert(lst, true);
                    log.HasBOM = (err == 0);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task<ErrorCodes> importVAT(importLog log)
        {
            if (!log.OpenFile("324 - VAT Product Posting Group", "Moms grupper"))
            {
                return ErrorCodes.Succes;
            }
            var dictionaryOfProductGroup = new Dictionary<string, string>();
            try
            {
                ErrorCodes err;
                List<string> lines;

                int codePostGroupPos = log.GetPos("Code");
                int descriptionProdPostGroupPos = log.GetPos("Description");

                while ((lines = log.GetLine(1)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2")
                        continue;

                    var code = Regex.Replace(Nav.GetField(lines, codePostGroupPos), " ", "");
                    var des = Nav.GetField(lines, descriptionProdPostGroupPos);

                    if (!string.IsNullOrWhiteSpace(code) && !string.IsNullOrWhiteSpace(des))
                        dictionaryOfProductGroup.Add(code, des);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }

            if (!log.OpenFile("325 - VAT Posting Setup", "Momskoder"))
            {
                return ErrorCodes.Succes;
            }
            try
            {
                ErrorCodes err;
                List<string> lines;

                var vattypecache = await log.api.CompanyEntity.LoadCache(typeof(GLVatType), log.api);

                var LedgerAccounts = log.LedgerAccounts;

                //Moms navn med i efter (indkøb). Moms med u (salg)
                int vatBussPostGroupPos = log.GetPos("VAT Bus. Posting Group");
                int vatProdPostGroupPos = log.GetPos("VAT Prod. Posting Group");
                int vatCalPos = log.GetPos("VAT Calculation Type");
                int vatProcentPos = log.GetPos("VAT %");
                int salesVATAccPos = log.GetPos("Sales VAT Account");
                int purchVATAccPos = log.GetPos("Purchase VAT Account");
                int revrChrgVatAccPos = log.GetPos("Reverse Chrg. VAT Acc.");
                int vatIdentifierPos = log.GetPos("VAT Identifier");

                string lastAcc = null;
                string lastAccOffset = null;
                var lst = new Dictionary<string, GLVat>(StringNoCaseCompare.GetCompare());
                var count = 0;
                while ((lines = log.GetLine(7)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new GLVat();

                    var vat = Nav.GetField(lines, vatBussPostGroupPos) + Nav.GetField(lines, vatProdPostGroupPos);
                    rec._Vat = Regex.Replace(vat, " ", "");
                    if (string.IsNullOrWhiteSpace(rec._Vat))
                        continue;

                    var purchAcc = log.GLAccountFromC5_Validate(Nav.GetField(lines, purchVATAccPos));
                    var salesAcc = log.GLAccountFromC5_Validate(Nav.GetField(lines, salesVATAccPos));

                    var vatProdPostGroup = Nav.GetField(lines, vatProdPostGroupPos);
                    var keyName = string.Empty;

                    if (purchAcc != null)
                    {
                        if (salesAcc != null)
                        {
                            if (dictionaryOfProductGroup.ContainsKey(vatProdPostGroup))
                            {
                                keyName = dictionaryOfProductGroup.First(x => x.Key == vatProdPostGroup).Value;
                                rec._Name = keyName + " indgående";
                            }
                            else
                                rec._Name = rec._Vat + " indgående";

                            rec._Vat = rec._Vat + "i";
                        }
                        else
                        {
                            if (dictionaryOfProductGroup.ContainsKey(vatProdPostGroup))
                            {
                                keyName = dictionaryOfProductGroup.First(x => x.Key == vatProdPostGroup).Value;
                                rec._Name = keyName;
                            }
                            else
                                rec._Name = rec._Vat;
                        }

                        rec._Account = purchAcc;

                        rec._OffsetAccount = log.GLAccountFromC5_Validate(Nav.GetField(lines, revrChrgVatAccPos));
                        rec._Rate = importLog.ToDouble(Nav.GetField(lines, vatProcentPos));

                        //if (Nav.GetField(lines, vatCalPos) == "1")
                        //    rec._Method = GLVatCalculationMethod.Netto;
                        //else
                        //    rec._Method = GLVatCalculationMethod.Brutto;

                        rec._Method = GLVatCalculationMethod.Automatic;

                        rec._VatType = GLVatSaleBuy.Buy;
                        rec._TypeSales = null;
                        rec._TypeBuy = "k1";

                        if (!lst.ContainsKey(rec.KeyStr))
                            lst.Add(rec.KeyStr, rec);

                        if (rec._Account != "" && rec._Account != lastAcc)
                        {
                            lastAcc = rec._Account;
                            var acc = (MyGLAccount) LedgerAccounts?.Get(lastAcc);
                            if (acc != null)
                            {
                                acc._SystemAccount = (byte) SystemAccountTypes.SalesTaxReceiveable;
                                acc.HasChanges = true;
                            }
                        }
                        if (rec._OffsetAccount != "" && rec._OffsetAccount != lastAccOffset)
                        {
                            lastAccOffset = rec._OffsetAccount;
                            var acc = (MyGLAccount) LedgerAccounts?.Get(lastAccOffset);
                            if (acc != null)
                            {
                                acc._SystemAccount = (byte) SystemAccountTypes.SalesTaxOffset;
                                acc.HasChanges = true;
                            }
                        }
                    }
                    if (salesAcc != null)
                    {
                       var rec2 = new GLVat();

                        if (purchAcc != null)
                        {
                            if (string.IsNullOrWhiteSpace(keyName))
                                rec2._Name = rec2._Vat + " udgående";
                            else
                                rec2._Name = keyName + " udgående";

                            rec2._Vat = rec2._Vat + "u";
                        }
                        else
                        {
                            if (string.IsNullOrWhiteSpace(keyName))
                                rec2._Name = rec2._Vat;
                            else
                                rec2._Name = keyName;
                        }

                        rec2._VatType = GLVatSaleBuy.Sales;
                        rec2._Vat = Nav.GetField(lines, vatBussPostGroupPos) + Nav.GetField(lines, vatProdPostGroupPos);
                        rec2._TypeBuy = null;
                        rec2._TypeSales = "s1";
                        rec2._Account = salesAcc;
                        rec2._OffsetAccount = log.GLAccountFromC5_Validate(Nav.GetField(lines, revrChrgVatAccPos));
                        rec2._Rate = importLog.ToDouble(Nav.GetField(lines, vatProcentPos));

                        if (!lst.ContainsKey(rec2.KeyStr))
                            lst.Add(rec2.KeyStr, rec2);

                        if (rec2._Account != "" && rec2._Account != lastAcc)
                        {
                            lastAcc = rec2._Account;
                            var acc = (MyGLAccount) LedgerAccounts?.Get(lastAcc);
                            if (acc != null)
                            {
                                acc._SystemAccount = (byte) SystemAccountTypes.SalesTaxPayable;
                                acc.HasChanges = true;
                            }
                        }
                        count++;
                    }
                }
                if (lst != null && lst.Count > 0)
                {
                    err = await log.Insert(lst.Values);
                    if (err != 0)
                        return err;

                    var vats = lst.Values.ToArray();
                    log.vats = new SQLCache(vats, true);
                    return 0;
                }
                return ErrorCodes.NoSucces;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }
        }

        public static async Task importPaymentGroup(importLog log)
        {
            if (!log.OpenFile("3 - Payment Terms", "Betalingsbetingelser"))
            {
                return;
            }

            try
            {
                int codePos = log.GetPos("Code");
                int dueDateCalPos = log.GetPos("Due Date Calculation");
                int disDateCalPos = log.GetPos("Discount Date Calculation");
                int disProcPos = log.GetPos("Discount %");
                int descriptionCalPos = log.GetPos("Description");


                List<string> lines;
                int n = 0;
                var lst = new Dictionary<string, PaymentTerm>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(5)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    var rec = new PaymentTerm();

                    rec._Payment = Nav.GetField(lines, codePos);
                    if (string.IsNullOrWhiteSpace(rec._Payment))
                        continue;

                    rec._Name = Nav.GetField(lines, descriptionCalPos);

                    string navDisCal = Nav.GetField(lines, disDateCalPos);
                    string cashDis = Regex.Replace(navDisCal, "[^0-9]", "");

                    if (!string.IsNullOrWhiteSpace(cashDis))
                        rec._CashDiscountDays = Convert.ToByte(cashDis);

                    string disProcent = Nav.GetField(lines, disProcPos);
                    if (!string.IsNullOrWhiteSpace(disProcent))
                        rec._CashDiscountPct = Convert.ToDouble(disProcent);

                    var duedatecal = Nav.GetField(lines, dueDateCalPos);
                    string days = "";
                    if (duedatecal.ToUpper().Contains("0D"))
                    {
                        rec._PaymentMethod = PaymentMethodTypes.NetCash;
                        rec._Days = 0;
                    }
                    else if (duedatecal.ToUpper().Contains("D"))
                    {
                        rec._PaymentMethod = PaymentMethodTypes.NetDays;
                        days = Regex.Replace(duedatecal, "[^0-9]", "");

                        if (!string.IsNullOrWhiteSpace(days))
                            rec._Days = Convert.ToInt32(days);
                    }
                    else if (duedatecal.ToUpper().Contains("M"))
                    {
                        rec._PaymentMethod = PaymentMethodTypes.EndMonth;
                        days = Regex.Replace(duedatecal, "[^0-9]", "");

                        if (!string.IsNullOrWhiteSpace(days))
                            rec._Days = Convert.ToInt32(days) * 30;

                        if (rec._Days == 0)
                            rec._Days = 30;
                    }
                    else if (duedatecal.ToUpper().Contains("W"))
                    {
                        rec._PaymentMethod = PaymentMethodTypes.EndWeek;
                        days = Regex.Replace(duedatecal, "[^0-9]", "");

                        if (!string.IsNullOrWhiteSpace(days))
                            rec._Days = Convert.ToInt32(days) * 7;
                    }
                    else
                    {
                        rec._PaymentMethod = PaymentMethodTypes.EndMonth;
                        rec._Days = 30;
                    }

                    if (!lst.Any())
                        rec._Default = true;
                    if (!lst.ContainsKey(rec.KeyStr))
                    {
                        lst.Add(rec.KeyStr, rec);
                        if (++n == 255)
                            break;
                    }
                }
                var err = await log.Insert(lst.Values);
                if (err == 0)
                {
                    var accs = lst.Values.ToArray();
                    log.Payments = new SQLCache(accs, true);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importDim1(importLog log, string dimension1)
        {
            if (!log.OpenFile("349 - Dimension Value", "Dimension 1"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int n = 0;
                var lst = new List<GLDimType1>();

                int dimCodePos = log.GetPos("Dimension Code");
                int codePos = log.GetPos("Code");
                int namePos = log.GetPos("Name");
                int codeCapPos = log.GetPos("Code Caption");
                int filterCaptionPos = log.GetPos("Filter Caption");
                int descriptionPos = log.GetPos("Description");
                int blockedPos = log.GetPos("Blocked");

                while ((lines = log.GetLine(3)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    if (Nav.GetField(lines, dimCodePos).ToUpper() != dimension1.ToUpper())
                        continue;

                    var rec = new GLDimType1();
                    rec._Dim = importLog.GetNotEmpty(Nav.GetField(lines, codePos));
                    if (rec._Dim == null)
                        continue;
                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, namePos));
                    lst.Add(rec);

                    log.HasDim1 = true;
                    if (++n == 10000)
                    {
                        log.AppendErrorLogLine("Man kan ikke have mere end 10.000 dimensioner");
                        break;
                    }
                }
                if (lst != null && lst.Count > 0)
                    await log.Insert(lst);

                if (log.HasDim1)
                {
                    log.dim1 = new SQLCache(lst.ToArray(), true);
                    log.api.CompanyEntity._Dim1 = dimension1;
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importDim2(importLog log, string dimension2)
        {
            if (!log.OpenFile("349 - Dimension Value", "Dimension 2"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int n = 0;
                var lst = new List<GLDimType2>();

                int dimCodePos = log.GetPos("Dimension Code");
                int codePos = log.GetPos("Code");
                int namePos = log.GetPos("Name");
                int codeCapPos = log.GetPos("Code Caption");
                int filterCaptionPos = log.GetPos("Filter Caption");
                int descriptionPos = log.GetPos("Description");
                int blockedPos = log.GetPos("Blocked");

                while ((lines = log.GetLine(3)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    if (Nav.GetField(lines, dimCodePos).ToUpper() != dimension2.ToUpper())
                        continue;

                    var rec = new GLDimType2();
                    rec._Dim = importLog.GetNotEmpty(Nav.GetField(lines, codePos));
                    if (rec._Dim == null)
                        continue;
                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, namePos));
                    lst.Add(rec);

                    log.HasDim2 = true;
                    if (++n == 10000)
                    {
                        log.AppendErrorLogLine("Man kan ikke have mere end 10.000 dimensioner");
                        break;
                    }
                }
                if (lst != null && lst.Count > 0)
                    await log.Insert(lst);

                if (log.HasDim2)
                {
                    log.dim2 = new SQLCache(lst.ToArray(), true);
                    log.api.CompanyEntity._Dim2 = dimension2;
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importDim3(importLog log, string dimension3)
        {
            if (!log.OpenFile("349 - Dimension Value", "Dimension 3"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int n = 0;
                var lst = new List<GLDimType3>();

                int dimCodePos = log.GetPos("Dimension Code");
                int codePos = log.GetPos("Code");
                int namePos = log.GetPos("Name");
                int codeCapPos = log.GetPos("Code Caption");
                int filterCaptionPos = log.GetPos("Filter Caption");
                int descriptionPos = log.GetPos("Description");
                int blockedPos = log.GetPos("Blocked");

                while ((lines = log.GetLine(3)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    if (Nav.GetField(lines, dimCodePos).ToUpper() != dimension3.ToUpper())
                        continue;

                    var rec = new GLDimType3();
                    rec._Dim = importLog.GetNotEmpty(Nav.GetField(lines, codePos));
                    if (rec._Dim == null)
                        continue;
                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, namePos));
                    lst.Add(rec);

                    log.HasDim3 = true;
                    if (++n == 10000)
                    {
                        log.AppendErrorLogLine("Man kan ikke have mere end 10.000 dimensioner");
                        break;
                    }
                }
                if (lst != null && lst.Count > 0)
                    await log.Insert(lst);

                if (log.HasDim3)
                {
                    log.dim3 = new SQLCache(lst.ToArray(), true);
                    log.api.CompanyEntity._Dim3 = dimension3;
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importDim4(importLog log, string dimension4)
        {
            if (!log.OpenFile("349 - Dimension Value", "Dimension 4"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int n = 0;
                var lst = new List<GLDimType4>();

                int dimCodePos = log.GetPos("Dimension Code");
                int codePos = log.GetPos("Code");
                int namePos = log.GetPos("Name");
                int codeCapPos = log.GetPos("Code Caption");
                int filterCaptionPos = log.GetPos("Filter Caption");
                int descriptionPos = log.GetPos("Description");
                int blockedPos = log.GetPos("Blocked");

                while ((lines = log.GetLine(3)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    if (Nav.GetField(lines, dimCodePos).ToUpper() != dimension4.ToUpper())
                        continue;

                    var rec = new GLDimType4();
                    rec._Dim = importLog.GetNotEmpty(Nav.GetField(lines, codePos));
                    if (rec._Dim == null)
                        continue;
                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, namePos));
                    lst.Add(rec);

                    log.HasDim4 = true;
                    if (++n == 10000)
                    {
                        log.AppendErrorLogLine("Man kan ikke have mere end 10.000 dimensioner");
                        break;
                    }
                }
                if (lst != null && lst.Count > 0)
                    await log.Insert(lst);

                if (log.HasDim4)
                {
                    log.dim4 = new SQLCache(lst.ToArray(), true);
                    log.api.CompanyEntity._Dim4 = dimension4;
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importDim5(importLog log, string dimension5)
        {
            if (!log.OpenFile("349 - Dimension Value", "Dimension 5"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int n = 0;
                var lst = new List<GLDimType5>();

                int dimCodePos = log.GetPos("Dimension Code");
                int codePos = log.GetPos("Code");
                int namePos = log.GetPos("Name");
                int codeCapPos = log.GetPos("Code Caption");
                int filterCaptionPos = log.GetPos("Filter Caption");
                int descriptionPos = log.GetPos("Description");
                int blockedPos = log.GetPos("Blocked");

                while ((lines = log.GetLine(3)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4" && lines[4] == "5")
                        continue;

                    if (Nav.GetField(lines, dimCodePos).ToUpper() != dimension5.ToUpper())
                        continue;

                    var rec = new GLDimType5();
                    rec._Dim = importLog.GetNotEmpty(Nav.GetField(lines, codePos));
                    if (rec._Dim == null)
                        continue;
                    rec._Name = importLog.GetNotEmpty(Nav.GetField(lines, namePos));
                    lst.Add(rec);

                    log.HasDim5 = true;
                    if (++n == 10000)
                    {
                        log.AppendErrorLogLine("Man kan ikke have mere end 10.000 dimensioner");
                        break;
                    }
                }
                if (lst != null && lst.Count > 0)
                    await log.Insert(lst);

                if (log.HasDim5)
                {
                    log.dim5 = new SQLCache(lst.ToArray(), true);
                    log.api.CompanyEntity._Dim5 = dimension5;
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static DCImportTrans[] importDCTrans(importLog log, bool deb, SQLCache Accounts)
        {
            if (deb)
            {
                if (!log.OpenFile("21 - Cust. Ledger Entry", "Debitorposteringer"))
                    return null;
            }
            else
            {
                if (!log.OpenFile("25 - Vendor Ledger Entry", "Kreditorposteringer"))
                    return null;
            }

            try
            {
                List<string> lines;

                int entryNoPos = log.GetPos("Entry No.");

                int debKredNoPos;
                if (deb)
                    debKredNoPos = log.GetPos("Customer No.");
                else
                    debKredNoPos = log.GetPos("Vendor No.");

                int postingDatePos = log.GetPos("Posting Date");
                int docTypePos = log.GetPos("Document Type");
                int docNoPos = log.GetPos("Document No.");
                int descriptionPos = log.GetPos("Description");
                int currCodePos = log.GetPos("Currency Code");

                int purchSalePos;
                if (deb)
                    purchSalePos = log.GetPos("Sales (LCY)");
                else
                    purchSalePos = log.GetPos("Purchase (LCY)");

                int invDiscPos = log.GetPos("Inv. Discount (LCY)");

                int sellToBuyFromPos;
                if (deb)
                    sellToBuyFromPos = log.GetPos("Sell-to Customer No.");
                else
                    sellToBuyFromPos = log.GetPos("Buy-from Vendor No.");

                int credDebGroupPos;
                if (deb)
                    credDebGroupPos = log.GetPos("Customer Posting Group");
                else
                    credDebGroupPos = log.GetPos("Vendor Posting Group");

                int dim1Pos = log.GetPos("Global Dimension 1 Code");
                int dim2Pos = log.GetPos("Global Dimension 2 Code");

                int empPos;
                if (deb)
                    empPos = log.GetPos("Salesperson Code");
                else
                    empPos = log.GetPos("Purchaser Code");

                int userIDPos = log.GetPos("User ID");
                int sourceCodePos = log.GetPos("Source Code");
                int dueDatenPos = log.GetPos("Due Date");
                int balAccNoPos = log.GetPos("Bal. Account No.");
                int transactionNoPos = log.GetPos("Transaction No.");
                int docDatePos = log.GetPos("Document Date");
                int externalDocNoPos = log.GetPos("External Document No.");

                DateTime firstValidDate = log.years[0]._FromDate;

                int offset = deb ? 0 : 1; // kreditor has one less
                List<DCImportTrans> lst = new List<DCImportTrans>();
                while ((lines = log.GetLine(20)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "3" && lines[2] == "4" && lines[3] == "5" && lines[4] == "6")
                        continue;

                    var Date = eco.ParseDT(Nav.GetField(lines, postingDatePos));
                    if (Date < firstValidDate)
                        continue;

                    var ac = Accounts?.Get(Nav.GetField(lines, debKredNoPos));
                    if (ac == null)
                        continue;

                    var rec = new DCImportTrans();
                    rec.Date = Date;
                    rec.Account = ac.KeyStr;
                    rec.Invoice = NumberConvert.ToInt(Nav.GetField(lines, docNoPos));
                    rec.Voucher = GetInt(Nav.GetField(lines, docNoPos));
                    if (rec.Voucher == 0)
                        rec.Voucher = 1;

                    rec.Text = Nav.GetField(lines, descriptionPos);
                    rec.Amount = importLog.ToDouble(Nav.GetField(lines, purchSalePos));


                    //TODO: Hvad er dif? (far)
                    //rec.dif = importLog.ToDouble(Nav.GetField(lines, 22 - offset]);

                    var cur = log.ConvertCur(Nav.GetField(lines, currCodePos));
                    //if (cur != 0)
                    //{
                    //    rec.Currency = cur;
                    //    rec.AmountCur = importLog.ToDouble(Nav.GetField(lines, 9 - offset]);
                    //    if (rec.Amount * rec.AmountCur < 0d) // different sign
                    //    {
                    //        rec.Currency = null;
                    //        rec.AmountCur = 0;
                    //    }
                    //}
                    lst.Add(rec);
                }
                if (lst.Count() > 0)
                {
                    var arr = lst.ToArray();
                    Array.Sort(arr, new DCImportTransSort());
                    return arr;
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
            return null;
        }

        public static async Task<ErrorCodes> importGLTrans(importLog log, DCImportTrans[] debpost,
            DCImportTrans[] crepost)
        {
            if (!log.OpenFile("17 - G_L Entry", "Finanspostering"))
            {
                return 0;
            }

            try
            {
                List<string> lines;

                //log.years = await log.api.Query<CompanyFinanceYear>();

                var Ledger = log.LedgerAccounts;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;

                DCImportTrans dksearch = new DCImportTrans();
                var dkcmp = new DCImportTransSort();

                var years = log.years;
                int lastYearIdx = 0;

                DateTime firstValidDate = years[0]._FromDate;
                int nYears = years.Length;
                List<GLPostingLineLocal>[] YearLst = new List<GLPostingLineLocal>[nYears];
                for (int i = nYears; (--i >= 0);)
                    YearLst[i] = new List<GLPostingLineLocal>();

                var primoPost = new List<GLPostingLineLocal>();
                DateTime primoYear = DateTime.MaxValue;

                int entryNoPos = log.GetPos("Entry No.");
                int accountNoPos = log.GetPos("G/L Account No.");
                int postingDatePos = log.GetPos("Posting Date");
                int docTypePos = log.GetPos("Document Type");
                int docNoPos = log.GetPos("Document No.");
                int descriptionPos = log.GetPos("Description");
                int currCodePos = log.GetPos("Currency Code");
                int balAccNoPos = log.GetPos("Bal. Account No.");
                int amountPos = log.GetPos("Amount");
                int dim1Pos = log.GetPos("Global Dimension 1 Code");
                int dim2Pos = log.GetPos("Global Dimension 2 Code");
                int userIDPos = log.GetPos("User ID");
                int sourceCodePos = log.GetPos("Source Code");
                int vatAmountPos = log.GetPos("VAT Amount");
                int qtyPos = log.GetPos("Quantity");
                int jounralNamePos = log.GetPos("Journal Batch Name");
                int transactionNoPos = log.GetPos("Transaction No.");
                int balAccTypePos = log.GetPos("Bal. Account Type");
                int credAmountPos = log.GetPos("Credit Amount");
                int debitAmountPos = log.GetPos("Debit Amount");
                int docDatenPos = log.GetPos("Document Date");
                int externalDocNoPos = log.GetPos("External Document No.");
                int busVATNoPos = log.GetPos("VAT Bus. Posting Group");
                int prodVATNoPos = log.GetPos("VAT Prod. Posting Group");
                int addCurrAmountPos = log.GetPos("Additional-Currency Amount");

                int cnt = 0;
                while ((lines = log.GetLine(17)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "3" && lines[2] == "4" && lines[3] == "5" && lines[4] == "6")
                        continue;

                    //TODO: gennemgå med far
                    //if (Nav.GetField(lines, 1] != "0") //Not budget and primo.
                    //    continue;

                    bool IsPrimo;

                    bool result = Nav.GetField(lines, postingDatePos).Any(x => !char.IsLetter(x));
                    if (result)
                        continue;

                    var removeLetterDate = Regex.Replace(Nav.GetField(lines, postingDatePos), "[a-zA-Z]", "");
                    var Date = eco.ParseDT(removeLetterDate);
                    if (Date < firstValidDate)
                        continue;

                    //var otherDate = GetDT(log, Date.ToString(), out IsPrimo);

                    GLPostingLineLocal kursDif = null;

                    var rec = new GLPostingLineLocal();
                    rec.Date = Date;
                    rec.Voucher = GetInt(Nav.GetField(lines, externalDocNoPos));
                    if (rec.Voucher == 0)
                        rec.Voucher = 1;

                    rec.Text = Nav.GetField(lines, descriptionPos);
                    rec.Amount = importLog.ToDouble(Nav.GetField(lines, amountPos));

                    //TODO: Ingen currency defineret
                    //var cur = log.ConvertCur(Nav.GetField(lines, 8]);
                    //if (cur != 0)
                    //{
                    //    rec.Currency = cur;
                    //    rec.IgnoreCurDif = true; // we do not want to do any regulation in a conversion
                    //    rec.AmountCur = importLog.ToDouble(Nav.GetField(lines, 7]);
                    //    if (rec.Amount * rec.AmountCur < 0d) // different sign
                    //    {
                    //        rec.Currency = null;
                    //        rec.AmountCur = 0;
                    //    }
                    //}

                    var orgAccount = log.GLAccountFromC5(Nav.GetField(lines, accountNoPos));
                    var acc = (MyGLAccount) Ledger.Get(orgAccount);
                    if (acc == null || !acc._PostingAccount)
                    {
                        acc = (MyGLAccount) Ledger.Get(log.errorAccount);
                        if (acc == null || !acc._PostingAccount)
                            continue;
                    }
                    rec.Account = acc._Account;

                    //if (Date < primoYear) // we have an earlier date than primo year. then we mark that as the first allowed primo date.
                    //{
                    //    primoYear = Date;
                    //    primoPost.Clear();
                    //}

                    //TODO: Hjælp mig med at forstå primo
                    //if (IsPrimo)
                    //{
                    //    if (Date == primoYear) // we only take primo from first year.
                    //    {
                    //        rec.primo = 1;
                    //        primoPost.Add(rec);
                    //    }
                    //    continue;
                    //}

                    if (acc.AccountTypeEnum == GLAccountTypes.Creditor || acc.AccountTypeEnum == GLAccountTypes.Debtor)
                    {
                        // here we convert account to debtor / creditor.
                        var arr = (acc.AccountTypeEnum == GLAccountTypes.Debtor) ? debpost : crepost;
                        if (arr != null)
                        {
                            dksearch.Amount = rec.Amount;
                            dksearch.Date = rec.Date;
                            dksearch.Voucher = rec.Voucher;

                            DCImportTrans post = null;
                            var idx = Array.BinarySearch(arr, dksearch, dkcmp);
                            if (idx >= 0)
                            {
                                for (int i = idx; (i >= 0); i--)
                                {
                                    post = arr[i];
                                    var c = dkcmp.Compare(post, dksearch);
                                    if (c == 0 && !post.Taken)
                                        break;
                                    post = null;
                                    if (c < 0)
                                        break;
                                }
                                if (post == null)
                                {
                                    while (++idx < arr.Length)
                                    {
                                        post = arr[idx];
                                        var c = dkcmp.Compare(post, dksearch);
                                        if (c == 0 && !post.Taken)
                                            break;
                                        post = null;
                                        if (c > 0)
                                            break;
                                    }
                                }
                            }

                            /*
                            if (post == null)
                            {
                                var l = arr.Length;
                                for (int i = 0; (i < l); i++)
                                {
                                    var p = arr[i];
                                    if (!p.Taken)
                                    {
                                        if (dkcmp.Compare(p, dksearch) == 0)
                                        {
                                            post = p;
                                            break;
                                        }
                                    }
                                }
                            }
                            */
                            if (post != null)
                            {
                                IdKey dc;
                                if (acc.AccountTypeEnum == GLAccountTypes.Debtor)
                                {
                                    dc = log.Debtors?.Get(post.Account);
                                    if (dc != null)
                                        rec.AccountType = GLJournalAccountType.Debtor;
                                }
                                else
                                {
                                    dc = log.Creditors?.Get(post.Account);
                                    if (dc != null)
                                        rec.AccountType = GLJournalAccountType.Creditor;

                                }
                                if (dc != null)
                                {
                                    post.Taken = true;
                                    rec.Account = post.Account;
                                    rec.Invoice = post.Invoice;

                                    ConvertDKText(rec, (DCAccount) dc);

                                    if (post.dif != 0d)
                                    {
                                        rec.Amount += post.dif;
                                        if (rec.Amount * rec.AmountCur < 0d) // different sign
                                        {
                                            rec.Currency = null;
                                            rec.AmountCur = 0;
                                        }

                                        kursDif = new GLPostingLineLocal();
                                        kursDif.Date = rec.Date;
                                        kursDif.Voucher = rec.Voucher;
                                        kursDif.Text = "Kursdif fra konvertering";
                                        kursDif.Account = acc._Account;
                                        kursDif.Amount = -post.dif;
                                        kursDif.DCPostType = DCPostType.ExchangeRateDif;

                                        post.dif = 0d;
                                    }
                                }
                            }
                        }
                    }
                    else if (acc._MandatoryTax != VatOptions.NoVat &&
                             acc.AccountTypeEnum != GLAccountTypes.Equity &&
                             acc.AccountTypeEnum != GLAccountTypes.Bank &&
                             acc.AccountTypeEnum != GLAccountTypes.LiquidAsset)
                    {
                        if (acc.AccountTypeEnum == GLAccountTypes.Debtor)
                        {
                            var valVat =
                                Regex.Replace(
                                    Nav.GetField(lines, busVATNoPos) + Nav.GetField(lines, prodVATNoPos) + "i", "", " ");

                            rec.Vat = log.GetVat_Validate(valVat);
                            if (rec.Vat != string.Empty)
                                acc.HasVat = true;
                            rec.VatHasBeenDeducted = true;
                        }
                        else if (acc.AccountTypeEnum == GLAccountTypes.Creditor)
                        {
                            var valVat =
                                Regex.Replace(
                                    Nav.GetField(lines, busVATNoPos) + Nav.GetField(lines, prodVATNoPos) + "u", "", " ");

                            rec.Vat = log.GetVat_Validate(valVat
                            );
                            if (rec.Vat != string.Empty)
                                acc.HasVat = true;
                            rec.VatHasBeenDeducted = true;
                        }

                    }

                    rec.SetDim(1, dim1?.Get(Nav.GetField(lines, dim1Pos))?.KeyStr);
                    rec.SetDim(2, dim2?.Get(Nav.GetField(lines, dim2Pos))?.KeyStr);

                    var y = years[lastYearIdx];
                    if (!(y._FromDate <= Date && y._ToDate >= Date))
                    {
                        lastYearIdx = -1;
                        for (int i = nYears; (--i >= 0);)
                        {
                            y = years[i];
                            if (y._FromDate <= Date && y._ToDate >= Date)
                            {
                                lastYearIdx = i;
                                break;
                            }
                        }
                    }
                    if (lastYearIdx >= 0)
                    {
                        YearLst[lastYearIdx].Add(rec);
                        if (kursDif != null)
                            YearLst[lastYearIdx].Add(kursDif);
                    }
                    else
                        lastYearIdx = 0;
                    cnt++;
                }

                //if (primoPost.Any())
                //{
                //    for (int i = nYears; (--i >= 0);)
                //    {
                //        var y = years[i];
                //        if (y._FromDate <= primoYear && y._ToDate >= primoYear)
                //        {
                //            YearLst[i].AddRange(primoPost);
                //            cnt += primoPost.Count;
                //            primoPost = null;
                //            break;
                //        }
                //    }
                //}

                log.AppendLogLine(string.Format("Number of transactions = {0}", cnt));

                if (debpost != null)
                    PostDK_NotFound(log, debpost, YearLst, GLJournalAccountType.Debtor);
                if (crepost != null)
                    PostDK_NotFound(log, crepost, YearLst, GLJournalAccountType.Creditor);

                log.progressBar.Maximum = nYears;

                var glSort = new GLTransSort();
                GLPostingHeader header = new GLPostingHeader();
                header.NumberSerie = "NR";
                header.ThisIsConversion = true;
                header.NoDateSum = true; // 
                for (int i = 0; (i < nYears); i++)
                {
                    if (log.errorAccount != null)
                    {
                        long sum = 0;
                        foreach (var rec in YearLst[i])
                            sum += NumberConvert.ToLong(rec.Amount * 100d);

                        if (sum != 0)
                        {
                            var rec = new GLPostingLineLocal();
                            rec.Date = years[i]._ToDate;
                            rec.Account = log.GLAccountFromC5(log.errorAccount);
                            rec.Voucher = 99999;
                            rec.Text = "Ubalance ved import fra NAV";
                            rec.Amount = sum / -100d;
                            YearLst[i].Add(rec);
                        }
                    }
                    var arr = YearLst[i].ToArray();
                    YearLst[i] = null;
                    if (arr.Length > 0)
                    {
                        Array.Sort(arr, glSort);
                        UpdateLedgerTrans(arr);
                        var ret = await c5.postYear(log, years[i], header, arr);
                        if (ret != 0)
                            return ret;
                    }
                    StartImport.UpdateProgressbar(i, null, log.progressBar);
                }

                foreach (var ac in (MyGLAccount[]) Ledger.GetNotNullArray)
                {
                    if (!ac.HasVat && ac._MandatoryTax != VatOptions.NoVat)
                    {
                        ac._MandatoryTax = VatOptions.NoVat;
                        ac.HasChanges = true;
                    }
                }

                Nav.UpdateVATonAccount(log);

                //log.AppendLogLine("Generate Primo Transactions");

                //var ap2 = new FinancialYearAPI(log.api);
                //for (int i = 1; (i < nYears); i++)
                //{
                //    var ok = await ap2.GeneratePrimoTransactions(years[i], null, null, 9999, "NR");
                //    if (ok != 0)
                //        break;
                //}

                var dcApi = new Uniconta.API.DebtorCreditor.MaintableAPI(log.api);
                await dcApi.SettleAllAfterConversion();

                StartImport.UpdateProgressbar(0, "Done", log.progressBar);

                return 0;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.SQLException;
            }
        }

        public static void UpdateVATonAccount(importLog log)
        {
            if (log.VatIsUpdated)
                return;
            if (!log.OpenFile("15 - G_L Account", "Finanskonti"))
            {
                return;
            }

            try
            {
               int accountPos = log.GetPos("No.");
                int namePos = log.GetPos("Name");
                int searchNamePos = log.GetPos("Search Name");
                int accountTypePos = log.GetPos("Account Type");
                int dim1Pos = log.GetPos("Global Dimension 1 Code");
                int dim2Pos = log.GetPos("Global Dimension 2 Code");
                int incomeBalancePos = log.GetPos("Income/Balance");
                int debCredPos = log.GetPos("Debit/Credit");
                int account2Pos = log.GetPos("No. 2");
                int blockPos = log.GetPos("Blocked");
                int dirPostPos = log.GetPos("Direct Posting");
                int recAccPos = log.GetPos("Reconciliation Account");
                int newPagePos = log.GetPos("New Page");
                int noBlankLinesPos = log.GetPos("No. of Blank Lines");
                int indentationPos = log.GetPos("Indentation");
                int lastDateModPos = log.GetPos("Last Date Modified");
                int totalingPos = log.GetPos("Totaling");
                int conTransMethPos = log.GetPos("Consol. Translation Method");
                int legealIdentPos = log.GetPos("Our Account No.");
                int VATBusNoPos = log.GetPos("VAT Bus. Posting Group");
                int VATPostNoPos = log.GetPos("VAT Prod. Posting Group");


                var lst = new Dictionary<string, MyGLAccount>(StringNoCaseCompare.GetCompare());
                var InvoiceAccs = new List<c5.InvoiceAccounts>();

                log.AppendLogLine("Update VAT on Chart of Account");

                var LedgerAccounts = log.LedgerAccounts;
                var VAT = log.vats;
                List<string> lines;
                while ((lines = log.GetLine(16)) != null)
                {
                    if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3" && lines[3] == "4")
                        continue;

                    var rec = (MyGLAccount) LedgerAccounts.Get(log.GLAccountFromC5(Nav.GetField(lines, accountPos)));
                    if (rec == null)
                        continue;

                    string s = "";

                    var valVat = string.Empty;
                        

                    if (rec._AccountType == (byte) GLAccountTypes.Revenue || rec._DebetCredit == DebitCreditPreference.Debet)
                        valVat = Regex.Replace(Nav.GetField(lines, VATBusNoPos) + Nav.GetField(lines, VATPostNoPos) + "i", "", " ");
                    else if(rec._AccountType == (byte) GLAccountTypes.CostOfGoodSold || rec._DebetCredit == DebitCreditPreference.Credit)
                       valVat = Regex.Replace(Nav.GetField(lines, VATBusNoPos) + Nav.GetField(lines, VATPostNoPos) + "u", "", " ");


                    s = log.GetVat_Validate(valVat); 
                    if (s != string.Empty)
                    {
                        rec._Vat = s;
                        rec.HasChanges = true;
                    }
                    
                    //s = log.GLAccountFromC5_Validate();
                    //if (s != string.Empty)
                    //{
                    //    rec._DefaultOffsetAccount = s;
                    //    rec.HasChanges = true;
                    //}

                    //rec._Currency = log.ConvertCur(lines[13]);
                    //if (rec._Currency != 0)
                    //    rec.HasChanges = true;

                    //if (lines.Count > (36 + 2))
                    //{
                    //    s = log.GLAccountFromC5_Validate(lines[36]);
                    //    if (s != string.Empty)
                    //    {
                    //        rec._PrimoAccount = s;
                    //        rec.HasChanges = true;
                    //    }
                    //}
                }

                var acclst = new List<GLAccount>();
                foreach (var r in LedgerAccounts.GetNotNullArray)
                {
                    var rec = (MyGLAccount) r;
                    if (rec.HasChanges)
                    {
                        rec.HasChanges = false;
                        acclst.Add(rec);
                    }
                }

                log.VatIsUpdated = true;
                log.Update(acclst);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }
    }
}
