﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.Common;
using Uniconta.DataModel;
using System.IO;
using Uniconta.Common.Utility;
using Uniconta.API.GeneralLedger;
using System.Threading;
using Uniconta.ClientTools;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Threading;
#if WPF
using Uniconta.ClientTools.Page;
#endif

namespace ImportingTool.Model
{
    public class lineNote
    {
        public string Note;
        public UnicontaBaseEntity master;
    }

    static public class c5
    {
        public class DebtorImport : Debtor
        {
            public int C5RecId;
        }
        public class CreditorImport : Creditor
        {
            public int C5RecId;
        }
        public class InvItemImport : InvItem
        {
            public int C5RecId;
            public string alternativeItem;
        }

        //Get zipcode
        static public string GetZipCode(string zipcity)
        {
            if (zipcity.Contains(" "))
            {
                string zip = zipcity.Substring(0, zipcity.IndexOf(" ", StringComparison.Ordinal));
                return zip.Trim();
            }
            else
            {
                string zip = zipcity;
                if (zip.Length > 4)
                    zip = zip.Substring(0, 4);
                return importLog.GetNotEmpty(zip);
            }
        }

        //Get country
        static public string GetCity(string zipcity)
        {
            if (zipcity.Contains(" "))
            {
                string city = zipcity.Substring(zipcity.IndexOf(" ", StringComparison.Ordinal) + 1);
                return city.Trim();
            }
            else
            {
                if (zipcity.Length > 4)
                    return zipcity.Substring(4, zipcity.Length - 4);
                else
                    return null;
            }
        }

        static public async Task<ErrorCodes> importAll(importLog log, int DimensionImport, bool isInvoiceEmail)
        {
            if (log.Set0InAccount)
                c5.getGLAccountLen(log);

            if (DimensionImport >= 1)
                await c5.importDepartment(log);
            if (DimensionImport >= 2)
                await c5.importCentre(log);
            if (DimensionImport >= 3)
                await c5.importPurpose(log);

            c5.importCompany(log);
            await c5.importYear(log);

            var err = await c5.importCOA(log);
            if (err != 0)
                return err;

            // we will need to update system account, since we need it in posting
            c5.importSystemKonti(log);

            err = await c5.importMoms(log);
            if (err != 0)
                return err;
            await c5.importDebtorGroup(log);
            await c5.importCreditorGroup(log);
            await c5.importEmployee(log);
            await c5.importShipment(log);
            await c5.importPaymentGroup(log);
            await c5.importPriceGroup(log);

            err = await c5.importDebitor(log, isInvoiceEmail);
            if (err != 0)
                return err;

            err = await c5.importCreditor(log);
            if (err != 0)
                return err;

            await c5.importContact(log, 1);
            await c5.importContact(log, 2);

            await c5.importInvGroup(log);
            var items = c5.importInv(log);
            if (items != null)
            {
                await c5.importInvPrices(log, items);
                await c5.importInvBOM(log);
            }

            var orderLineNotes = new Dictionary<long, lineNote>();
            await c5.importNotes(log, orderLineNotes);

            var orders = new Dictionary<long, DebtorOrder>();
            var offers = new Dictionary<long, DebtorOffer>();
            await c5.importOrder(log, orders, offers, orderLineNotes);
            if (orders.Count > 0 || offers.Count > 0)
                await c5.importOrderLinje(log, orders, offers, orderLineNotes);
            orders = null;
            offers = null;

            var purchages = new Dictionary<long, CreditorOrder>();
            await c5.importPurchage(log, purchages, orderLineNotes);
            if (purchages.Count > 0)
                await c5.importPurchageLinje(log, purchages, orderLineNotes);
            purchages = null;

            await c5.SaveNotes(log, orderLineNotes);
            orderLineNotes = null;

            // clear memory
            log.Items = null;
            log.ItemGroups = null;
            log.PriceLists = null;
            log.Payments = null;
            log.Employees = null;

            if (log.Terminate)
                return ErrorCodes.NoSucces;

            // we we do not import years, we do not import transaction
            if (log.years != null && log.years.Length > 0)
            {
                DCImportTrans[] debpost = null;
                if (log.Debtors != null)
                {
                    debpost = c5.importDCTrans(log, true, log.Debtors);
                    if (debpost != null && ! log.HasDebtorSummeryAccount)
                    {
                        log.AppendLogLine("FEJL..... Du har ikke nogen debitor-samlekonto sat op på debitorgrupperne");
                        return ErrorCodes.NoSucces;
                    }
                }

                DCImportTrans[] krepost = null;
                if (log.Creditors != null)
                {
                    krepost = c5.importDCTrans(log, false, log.Creditors);
                    if (krepost != null && !log.HasCreditorSummeryAccount)
                    {
                        log.AppendLogLine("FEJL..... Du har ikke nogen kreditor-samlekonto sat op på kreditorgrupperne");
                        return ErrorCodes.NoSucces;
                    }
                }
                err = await c5.importGLTrans(log, debpost, krepost);
                if (err != 0)
                    return err;
            }

            // We do that after posting is called, since we have fixe vat and other things that can prevent posting
            c5.UpdateVATonAccount(log);

            return ErrorCodes.Succes;
        }

        public static async Task importYear(importLog log)
        {
            if (!log.OpenFile("exp00031", "Finansperioder"))
            {
                log.AppendLogLine("Financial years not imported. No transactions will be imported");
                return;
            }
            try
            {
                // first we load primo and ultimo periodes.
                List<List<string>> ValidLines = new List<List<string>>();

                List<string> lin;
                while ((lin = log.GetLine(8)) != null)
                {
                    if (lin[3] != "0")
                        ValidLines.Add(lin);
                }
                // then we sort it in dato order
                var ValidLines2 = ValidLines.OrderBy(rec => rec[0]);

                var lst = new List<CompanyFinanceYear>();
                DateTime fromdate = DateTime.MinValue;

                foreach (var lines in ValidLines2)
                {
                    var datastr = lines[0];
                    if (datastr.Length < 10)
                        continue;
                    int year = int.Parse(datastr.Substring(0, 4));
                    int month = int.Parse(datastr.Substring(5, 2));

                    if (lines[3] == "1")
                    {
                        fromdate = new DateTime(year, month, 1);
                    }
                    else
                    {
                        if (fromdate == DateTime.MinValue)
                            fromdate = new DateTime(year, month, 1);

                        var dayinmonth = DateTime.DaysInMonth(year, month);
                        var ToDate = new DateTime(year, month, dayinmonth);
                        if (fromdate > ToDate)
                            fromdate = ToDate.AddDays(1).AddYears(-1);

                        var rec = new CompanyFinanceYear();
                        rec._FromDate = fromdate;
                        rec._ToDate = ToDate;
                        rec._State = FinancePeriodeState.Open; // set to open, otherwise we can't import transactions
                        rec.OpenAll();

                        log.AppendLogLine(rec._FromDate.ToShortDateString());

                        lst.Add(rec);
                    }
                }
                var Orderedlst = lst.OrderBy(rec => rec._ToDate);
                DateTime prevTo = DateTime.MinValue;
                var now = DateTime.Now.Date;
                foreach (var rec in Orderedlst)
                {
                    if (prevTo != DateTime.MinValue)
                        rec._FromDate = prevTo.AddDays(1);
                    prevTo = rec._ToDate;
                    if (rec._FromDate <= now && rec._ToDate >= now)
                        rec._Current = true;
                }
                var err = await log.Insert(Orderedlst);
                if (err != 0)
                    log.AppendLogLine("Financial years not imported. No transactions will be imported");
                else
                    log.years = Orderedlst.ToArray();
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static void getGLAccountLen(importLog log)
        {
            if (!log.OpenFile("exp00025", "Finanskonti"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int AccountPrev = 0;
                int Accountlen = 0, nMax = 0;

                while ((lines = log.GetLine(2)) != null)
                {
                    var _account = log.GLAccountFromC5(lines[1]);
                    if (_account.Length >= Accountlen)
                    {
                        if (_account.Length == Accountlen)
                            nMax++;
                        else
                        {
                            int i = _account.Length;
                            while (--i >= 0)
                            {
                                var ch = _account[0];
                                if (ch < '0' || ch > '9')
                                    break;
                            }
                            if (i < 0)
                            {
                                AccountPrev = Accountlen;
                                Accountlen = _account.Length;
                                nMax = 0;
                            }
                        }
                    }
                }
                if (nMax > 2)
                    log.Accountlen = Accountlen;
                else
                    log.Accountlen = AccountPrev;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task<ErrorCodes> importCOA(importLog log)
        {
            if (!log.OpenFile("exp00025", "Finanskonti"))
            {
                return ErrorCodes.FileDoesNotExist;
            }

            try
            { 
                List<string> lines;
                var lst = new Dictionary<string, MyGLAccount>(StringNoCaseCompare.GetCompare());
                var sum = new List<string>();
            
                while ((lines = log.GetLine(28)) != null)
                {
                    var rec = new MyGLAccount();
                    rec._Account = log.GLAccountFromC5(lines[1]);
                    if (string.IsNullOrWhiteSpace(rec._Account))
                        continue;

                    rec._Name = lines[2];
                    rec.VatKode = lines[11];
                    rec._Lookup = importLog.GetNotEmpty(lines[4]);
                    rec._ExternalNo = importLog.GetNotEmpty(lines[27]);
                    var counters = lines[15].ToLower();
                    var t = GetInt(lines[3]);
                    if (t != 6 && counters != string.Empty)
                    {
                        int n = 0;
                        foreach (var r in sum)
                        {
                            if (string.Compare(r, counters) == 0)
                            {
                                rec._SaveTotal = n + 1;
                                break;
                            }
                            n++;
                        }
                        if (rec._SaveTotal == 0)
                        {
                            rec._SaveTotal = n + 1;
                            sum.Add(counters);
                        }
                    }

                    switch (t)
                    {
                        case 0: rec._AccountType = (byte)GLAccountTypes.PL; break;
                        case 1:
                            rec._AccountType = (byte)GLAccountTypes.BalanceSheet;
                            if (rec._Name.IndexOf("bank", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                rec._AccountType = (byte)GLAccountTypes.Bank;
                            break;
                        case 2: rec._AccountType = (byte)GLAccountTypes.Header; rec._MandatoryTax = VatOptions.NoVat; break;
                        case 3: rec._AccountType = (byte)GLAccountTypes.Header; rec._PageBreak = true; rec._MandatoryTax = VatOptions.NoVat; break;
                        case 4: rec._AccountType = (byte)GLAccountTypes.Header; rec._MandatoryTax = VatOptions.NoVat; break;
                        case 5: rec._AccountType = (byte)GLAccountTypes.Sum; rec._SumInfo = log.GLAccountFromC5(lines[10]) + ".." + rec._Account; rec._MandatoryTax = VatOptions.NoVat; break;
                        case 6:
                            {
                                int n = 1;
                                foreach (var r in sum)
                                {
                                    counters = counters.Replace(r, string.Format("Sum({0})", n));
                                    n++;
                                }
                                rec._SumInfo = counters;
                                rec._AccountType = (byte)GLAccountTypes.CalculationExpression;
                                rec._MandatoryTax = VatOptions.NoVat;
                                break;
                            }
                    }
                    // We can't set Mandatory = true, since we can't garantie that transaction has dimensions
                    if (log.HasDepartment)
                        rec.SetDimUsed(1, true);
                    if (log.HasCentre)
                        rec.SetDimUsed(2, true);
                    if (log.HasPorpose)
                        rec.SetDimUsed(3, true);

                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }
                var err = await log.Insert(lst.Values);
                if (err != 0)
                    return err;

                var accs = lst.Values.ToArray();
                log.LedgerAccounts = new SQLCache(accs, true);
                return ErrorCodes.Succes;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }
        }

        static public void ClearDimension(GLAccount Acc)
        {
            Acc.SetDimUsed(1, false);
            Acc.SetDimUsed(2, false);
            Acc.SetDimUsed(3, false);
            Acc.SetDimUsed(4, false);
            Acc.SetDimUsed(5, false);
        }

        public static async Task<ErrorCodes> importMoms(importLog log)
        {
            if (!log.OpenFile("exp00014", "Momskoder"))
            {
                return ErrorCodes.Succes;
            }

            try
            {
                ErrorCodes err;
                List<string> lines;

                var vattypecache = await log.api.CompanyEntity.LoadCache(typeof(GLVatType), log.api);

                var LedgerAccounts = log.LedgerAccounts;

                string lastAcc = null;
                string lastAccOffset = null;
                var lst = new Dictionary<string, GLVat>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(11)) != null)
                {
                    var rec = new GLVat();
                    rec._Vat = lines[0];
                    if (string.IsNullOrWhiteSpace(rec._Vat))
                        continue;
                
                    rec._Name = lines[1];
                    rec._Account = log.GLAccountFromC5_Validate(lines[3]);                   
                    rec._OffsetAccount = log.GLAccountFromC5_Validate(lines[4]);
                    var Rate = importLog.ToDouble(lines[2]);
                    var Excempt = importLog.ToDouble(lines[5]);
                    rec._Rate = Rate * (100d - Excempt) / 100d;

                    if (rec._Account != string.Empty && rec._OffsetAccount != string.Empty)
                        rec._Method = GLVatCalculationMethod.Netto;
                    else
                        rec._Method = GLVatCalculationMethod.Brutto;

                    int vatId;
                    var vattype = lines[10].ToLower();
                    if (string.IsNullOrEmpty(vattype))
                    {
                        if (vattype.Contains("salg"))
                            vatId = 1;
                        else if (vattype.Contains("køb") || vattype.Contains("indg") || vattype.Contains("import"))
                            vatId = 2;
                        else
                            vatId = 1;
                    }
                    else
                        vatId = GetInt(vattype);

                    switch (vatId)
                    {
                        case 0:
                        case 1:
                            rec._VatType = GLVatSaleBuy.Sales;
                            rec._TypeSales = "s1";
                            break;
                        case 2:
                            rec._VatType = GLVatSaleBuy.Buy;
                            rec._TypeBuy = "k1";
                            break;
                    }

                    // Here we are assigning the VAT operations. You can see that on the VAT table on the VAT operations

                    switch (rec._Vat.ToLower())
                    {
                        case "b25": rec._TypeBuy = "k3"; rec._VatType = GLVatSaleBuy.Buy; break;
                        case "hrep":
                        case "repr":
                        case "rep": rec._TypeBuy = "k1"; rec._VatType = GLVatSaleBuy.Buy; rec._Rate = 6.25d; break;
                        case "i25": rec._TypeBuy = "k1"; rec._VatType = GLVatSaleBuy.Buy; break;
                        case "umoms":
                        case "iv25": rec._TypeBuy = "k4"; rec._VatType = GLVatSaleBuy.Buy; break;
                        case "iy25": rec._TypeBuy = "k5"; rec._VatType = GLVatSaleBuy.Buy; break;
                        case "u25": rec._TypeSales = "s1"; rec._VatType = GLVatSaleBuy.Sales; break;
                        case "ueuv": rec._TypeSales = "s3"; rec._VatType = GLVatSaleBuy.Sales; break;
                        case "uv0": rec._TypeSales = "s3"; rec._VatType = GLVatSaleBuy.Sales; break;
                        case "ueuy": rec._TypeSales = "s4"; rec._VatType = GLVatSaleBuy.Sales; break;
                        case "uy0": rec._TypeSales = "s4"; rec._VatType = GLVatSaleBuy.Sales; break;
                        case "abr": rec._TypeSales = "s6"; rec._VatType = GLVatSaleBuy.Sales; break;
                    }

                    var vt = rec._TypeSales ?? rec._TypeBuy;
                    if (vattypecache.Get(vt) == null)
                    {
                        rec._TypeSales = null;
                        rec._TypeBuy = null;
                    }

                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);

                    if (rec._Account != "" && rec._Account != lastAcc)
                    {
                        lastAcc = rec._Account;
                        var acc = (MyGLAccount)LedgerAccounts.Get(lastAcc);
                        if (acc != null)
                        {
                            acc._SystemAccount = rec._VatType == GLVatSaleBuy.Sales ? (byte)SystemAccountTypes.SalesTaxPayable : (byte)SystemAccountTypes.SalesTaxReceiveable;
                            acc.HasChanges = true;
                        }
                    }
                    if (rec._VatType == GLVatSaleBuy.Buy && rec._OffsetAccount != "" && rec._OffsetAccount != lastAccOffset)
                    {
                        lastAccOffset = rec._OffsetAccount;
                        var acc = (MyGLAccount)LedgerAccounts.Get(lastAccOffset);
                        if (acc != null)
                        {
                            acc._SystemAccount = (byte)SystemAccountTypes.SalesTaxOffset;
                            acc.HasChanges = true;
                        }
                    }
                }
                err = await log.Insert(lst.Values);
                if (err != 0)
                    return err;

                var vats = lst.Values.ToArray();
                log.vats = new SQLCache(vats, true);
                return 0;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }
        }

        public static void UpdateVATonAccount(importLog log)
        {
            if (log.VatIsUpdated)
                return;
            if (!log.OpenFile("exp00025", "Finanskonti"))
            {
                return;
            }

            try
            {
                log.AppendLogLine("Update VAT on Chart of Account");

                List<string> lines;
                var LedgerAccounts = log.LedgerAccounts;

                while ((lines = log.GetLine(16)) != null)
                {
                    var rec = (MyGLAccount)LedgerAccounts.Get(log.GLAccountFromC5(lines[1]));
                    if (rec == null)
                        continue;

                    var s = log.GetVat_Validate(lines[11]);
                    if (s != string.Empty)
                    {
                        rec._Vat = s;
                        rec.HasChanges = true;

                        // To be updated laver. We risk that we can't import transactions, since we require VAT on transactions.
                        if (lines.Count > (35 + 2) && lines[35] != "0")
                            rec._MandatoryTax = VatOptions.Fixed;
                        else
                            rec._MandatoryTax = VatOptions.Optional;
                    }
                    s = log.GLAccountFromC5_Validate(lines[8]);
                    if (s != string.Empty)
                    {
                        rec._DefaultOffsetAccount = s;
                        rec.HasChanges = true;
                    }

                    rec._Currency = log.ConvertCur(lines[13]);
                    if (rec._Currency != 0)
                        rec.HasChanges = true;

                    if (lines.Count > (36 + 2))
                    {
                        s = log.GLAccountFromC5_Validate(lines[36]);
                        if (s != string.Empty)
                        {
                            rec._PrimoAccount = s;
                            rec.HasChanges = true;
                        }
                    }
                }

                var acclst = new List<GLAccount>();
                foreach (var r in LedgerAccounts.GetNotNullArray)
                {
                    var rec = (MyGLAccount)r;
                    if (rec.HasChanges)
                    {
                        rec.HasChanges = false;
                        acclst.Add(rec);
                    }
                }

                log.VatIsUpdated = true;
                log.Update(acclst);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importPriceGroup(importLog log)
        {
            if (!log.OpenFile("exp00064", "Prisgrupper"))
            {
                return;
            }

            List<string> lines;

            try
            {
                var lst = new Dictionary<string, DebtorPriceList>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(3)) != null)
                {
                    var rec = new DebtorPriceList();
                    rec._Name = lines[0];
                    if (string.IsNullOrWhiteSpace(rec._Name))
                        continue;
                    rec._InclVat = lines[2] == "1";
                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }
                await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.PriceLists = new SQLCache(accs, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static void importSystemKonti(importLog log)
        {
            if (!log.OpenFile("exp00013", "SystemKonti"))
            {
                return;
            }

            try
            {
                List<string> lines;
                var lst = new List<GLAccount>();

                var LedgerAccounts = log.LedgerAccounts;

                while ((lines = log.GetLine(15)) != null)
                {
                    if (lines[1] == "0")
                    {
                        var rec = (MyGLAccount)LedgerAccounts.Get(log.GLAccountFromC5(lines[4]));
                        if (rec == null)
                            continue;

                        switch (lines[0])
                        {
                            case "ÅretsResultat" :
                            case "SYS60928":
                                rec._SystemAccount = (byte)SystemAccountTypes.EndYearResultTransfer;
                                if (log.errorAccount == null)
                                    log.errorAccount = rec.KeyStr;
                                ClearDimension(rec);
                                break;
                            case "Sek.Val.Afrunding" :
                            case "SYS60933":
                                rec._SystemAccount = (byte)SystemAccountTypes.ExchangeDif;
                                log.valutaDif = rec.KeyStr;
                                if (log.errorAccount == null)
                                    log.errorAccount = rec.KeyStr;
                                break;
                            case "Fejlkonto":
                            case "SYS11607":
                                rec._SystemAccount = (byte)SystemAccountTypes.ErrorAccount;
                                log.errorAccount = rec.KeyStr;
                                break;
                            case "Øredifference":
                            case "SYS34401":
                                rec._SystemAccount = (byte)SystemAccountTypes.PennyDif;
                                if (log.errorAccount == null)
                                    log.errorAccount = rec.KeyStr;
                                break;
                            case "AfgiftOlie":
                            case "SYS60929":
                            case "AfgiftEl":
                            case "SYS60930":
                            case "AfgiftVand":
                            case "SYS60931":
                            case "AfgiftKul":
                            case "SYS60932":
                                rec._SystemAccount = (byte)SystemAccountTypes.OtherTax;
                                rec.HasChanges = true;
                                continue;
                            default: continue;
                        }
                        lst.Add(rec);
                    }
                }
                log.Update(lst);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importDebtorGroup(importLog log)
        {
            if (!log.OpenFile("exp00034", "Debitorgrupper"))
            {
                return;
            }

            try
            {
                List<string> lines;
                string lastAcc = null;
                var LedgerAccounts = log.LedgerAccounts;

                var lst = new Dictionary<string, DebtorGroup>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(10)) != null)
                {
                    var rec = new DebtorGroup();
                    rec._Group = lines[0];
                    if (string.IsNullOrWhiteSpace(rec._Group))
                        continue;
                    rec._Name = lines[1];
                    rec._SummeryAccount = log.GLAccountFromC5_Validate(lines[7]);
                    rec._SettlementDiscountAccount = log.GLAccountFromC5_Validate(lines[8]);
                    rec._EndDiscountAccount = log.GLAccountFromC5_Validate(lines[4]);
                    var Acc = (MyGLAccount)LedgerAccounts.Get(log.GLAccountFromC5(lines[2]));
                    if (Acc != null)
                    {
                        rec._RevenueAccount = Acc._Account;
                        rec._Vat = log.GetVat_Validate(Acc.VatKode);
                    }

                    rec._UseFirstIfBlank = true;
                    if (!lst.Any())
                        rec._Default = true;

                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);

                    if (rec._SummeryAccount != string.Empty && rec._SummeryAccount != lastAcc)
                    {
                        lastAcc = rec._SummeryAccount;
                        var acc = (MyGLAccount)LedgerAccounts.Get(lastAcc);
                        if (acc != null)
                        {
                            acc._AccountType = (byte)GLAccountTypes.Debtor;
                            acc.HasChanges = true;
                        }
                    }
                }

                if (lastAcc != null)
                {
                    log.HasDebtorSummeryAccount = true;
                    foreach (var gr in lst.Values)
                            if (gr._SummeryAccount == string.Empty)
                                gr._SummeryAccount = lastAcc;
                }
                await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.DebGroups = new SQLCache(accs, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importCreditorGroup(importLog log)
        {
            if (!log.OpenFile("exp00042", "Kreditorgrupper"))
            {
                return;
            }

            try
            {
                List<string> lines;
                string lastAcc = null;
                var LedgerAccounts = log.LedgerAccounts;

                var lst = new Dictionary<string, CreditorGroup>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(10)) != null)
                {
                    var rec = new CreditorGroup();

                    rec._Group = lines[0];
                    if (string.IsNullOrWhiteSpace(rec._Group))
                        continue;

                    rec._Name = lines[1];
                    rec._SummeryAccount = log.GLAccountFromC5_Validate(lines[7]);
                    rec._SettlementDiscountAccount = log.GLAccountFromC5_Validate(lines[8]);
                    rec._EndDiscountAccount = log.GLAccountFromC5_Validate(lines[4]);
                    var Acc = (MyGLAccount)LedgerAccounts.Get(log.GLAccountFromC5(lines[2]));
                    if (Acc != null)
                    {
                        rec._RevenueAccount = Acc._Account;
                        rec._Vat = log.GetVat_Validate(Acc.VatKode);
                    }

                    rec._UseFirstIfBlank = true;
                    if (!lst.Any())
                        rec._Default = true;
                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);

                    if (rec._SummeryAccount != string.Empty && rec._SummeryAccount != lastAcc)
                    {
                        lastAcc = rec._SummeryAccount;
                        var acc = (MyGLAccount)LedgerAccounts.Get(lastAcc);
                        if (acc != null)
                        {
                            acc._AccountType = (byte)GLAccountTypes.Creditor;
                            acc.HasChanges = true;
                        }
                    }
                }
                if (lastAcc != null)
                {
                    log.HasCreditorSummeryAccount = true;
                    foreach (var gr in lst.Values)
                        if (gr._SummeryAccount == string.Empty)
                            gr._SummeryAccount = lastAcc;
                }
                await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.CreGroups = new SQLCache(accs, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importInvGroup(importLog log)
        {
            if (!log.OpenFile("exp00050", "Varegrupper"))
            {
                return;
            }

            try
            { 
                List<string> lines;
                var Ledger = log.LedgerAccounts;

                var lst = new Dictionary<string, InvGroup>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(15)) != null)
                {
                    var rec = new InvGroup();
                    rec._Group = lines[0];
                    if (string.IsNullOrWhiteSpace(rec._Group))
                        continue;
                    rec._Name = lines[1];

                    rec._CostAccount = log.GLAccountFromC5_Validate(lines[3]);
                    rec._InvAccount = log.GLAccountFromC5_Validate(lines[9]);
                    rec._InvReceipt = log.GLAccountFromC5_Validate(lines[10]);
                                      
                    var Acc = (MyGLAccount)Ledger.Get(log.GLAccountFromC5(lines[2]));
                    if (Acc != null)
                    {
                        rec._RevenueAccount = Acc._Account;
                        rec._SalesVat = log.GetVat_Validate(Acc.VatKode);
                    }

                    Acc = (MyGLAccount)Ledger.Get(log.GLAccountFromC5(lines[9]));
                    if (Acc != null)
                    {
                        rec._PurchaseAccount = Acc._Account;
                        rec._PurchaseVat = log.GetVat_Validate(Acc.VatKode);
                    }

                    rec._UseFirstIfBlank = true;

                    if (!lst.Any())
                        rec._Default = true;

                    if (! lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }
                var err = await log.Insert(lst.Values);
                if (err == 0)
                {
                    var accs = lst.Values.ToArray();
                    log.ItemGroups = new SQLCache(accs, true);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public class MyNote : Note
        {
            public int lineNumber;
        }
        class NoteSort : IComparer<MyNote>
        {
            public int Compare(MyNote x, MyNote y)
            {
                int c = x._TableId - y._TableId;
                if (c != 0)
                    return c;
                c = x._TableRowId - y._TableRowId;
                if (c != 0)
                    return c;
                c = DateTime.Compare(x._Created, y._Created);
                if (c != 0)
                    return c;
                return x.lineNumber - y.lineNumber;
            }
        }

        public static async Task importNotes(importLog log, Dictionary<long, lineNote> orderLineNotes)
        {
            if (!log.OpenFile("exp00004", "Notater"))
            {
                return;
            }

            try
            {
                List<string> lines;

                var lookup = new Dictionary<long, IdKey>();
                bool DebtorsAdded = false, CreditorAdded = false, ItemAdded = false;
                var lst = new List<MyNote>();
                while ((lines = log.GetLine(5)) != null)
                {
                    var text = lines[3].Trim();
                    if (text == string.Empty)
                        continue;

                    int TableId;
                    var FileId = NumberConvert.ToInt(lines[0]);
                    if (FileId == 33)
                    {
                        TableId = 50; // Debitor
                        if (!DebtorsAdded)
                        {
                            DebtorsAdded = true;
                            foreach (var id in log.Debtors.GetRecords)
                            {
                                var row = id as DebtorImport;
                                if (row != null)
                                    lookup.Add((50L << 32) + row.C5RecId, id);
                            }
                        }
                    }
                    else if (FileId == 41)
                    {
                        TableId = 51; // Creditor
                        if (!CreditorAdded)
                        {
                            CreditorAdded = true;
                            foreach (var id in log.Creditors.GetRecords)
                            {
                                var row = id as CreditorImport;
                                if (row != null)
                                    lookup.Add((51L << 32) + row.C5RecId, id);
                            }
                        }
                    }
                    else if (FileId == 49)
                    {
                        TableId = 23; // Inventory
                        if (!ItemAdded)
                        {
                            ItemAdded = true;
                            foreach (var id in log.Items.GetRecords)
                            {
                                var row = id as InvItemImport;
                                if (row != null)
                                    lookup.Add((23L << 32) + row.C5RecId, id);
                            }
                        }
                    }
                    else if (FileId >= 125 && FileId <= 128) // order header/lines, purchase header/lines.
                    {
                        lineNote noteRec;
                        var noteId = ((long)FileId << 32) + NumberConvert.ToInt(lines[1]);
                        if (orderLineNotes.TryGetValue(noteId, out noteRec))
                            noteRec.Note = string.Format("{0}\n{1}", noteRec.Note, text);
                        else
                            orderLineNotes.Add(noteId, new lineNote() { Note = text });
                        continue;
                    }
                    else
                        continue;

                    IdKey UniRec;
                    var LookupId = ((long)TableId << 32) + NumberConvert.ToInt(lines[1]);
                    if (!lookup.TryGetValue(LookupId, out UniRec))
                        continue;

                    var rec = new MyNote();
                    rec._Text = text;
                    rec._Created = GetDT(lines[4]);
                    rec._TableId = TableId;
                    rec._TableRowId = UniRec.RowId;
                    rec.lineNumber = (int)NumberConvert.ToInt(lines[2]);

                    lst.Add(rec);
                }
                var arr = lst.ToArray();
                lst = null;

                DateTime lastDate = DateTime.MinValue;
                int SecondsToAdd = 0;
                int lastRow = -1;
                Array.Sort(arr, new NoteSort());
                foreach (var r in arr)
                {
                    if (r._TableRowId == lastRow && r._Created == lastDate)
                    {
                        SecondsToAdd++;
                        r._Created = lastDate.AddSeconds(SecondsToAdd);
                    }
                    else
                    {
                        lastDate = r._Created;
                        lastRow = r._TableRowId;
                        SecondsToAdd = 0;
                    }
                }
                await log.Insert(arr, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task SaveNotes(importLog log, Dictionary<long, lineNote> orderLineNotes)
        {
            try
            {
                var now = DateTime.UtcNow;
                var lst = new List<Note>();
                foreach(var note in orderLineNotes.Values)
                {
                    if (note.master == null)
                        continue;
                    var rec = new Note();
                    rec._Text = note.Note;
                    rec._Created = now;
                    rec.SetMaster(note.master);
                    lst.Add(rec);
                }
                await log.Insert(lst, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importShipment(importLog log)
        {
            if (!log.OpenFile("exp00019", "Forsendelser"))
            {
                return;
            }

            try
            {
                List<string> lines;

                var lst = new Dictionary<string, ShipmentType>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(2)) != null)
                {
                    var rec = new ShipmentType();
                    rec._Number = lines[0];
                    if (rec._Number == string.Empty)
                        continue;
                    rec._Name = lines[1];

                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }
                await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.Shipments = new SQLCache(accs, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importEmployee(importLog log)
        {
            if (!log.OpenFile("exp00011", "Medarbejder"))
            {
                return;
            }

            try
            {
                List<string> lines;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;

                var lst = new Dictionary<string, Employee>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(12)) != null)
                {
                    var rec = new Employee();
                    rec._Number = importLog.GetNotEmpty(lines[0]);
                    if (rec._Number == null)
                        continue;
                    rec._Name = importLog.GetNotEmpty(lines[1]);
                    rec._Address1 = importLog.GetNotEmpty(lines[2]);
                    rec._Address2 = importLog.GetNotEmpty(lines[3]);
                    rec._ZipCode = GetZipCode(lines[4]);
                    rec._City = GetCity(lines[4]);
                    rec._Mobil = importLog.GetNotEmpty(lines[6]);
                    rec._Email = importLog.GetNotEmpty(lines[11]);

                    var x = NumberConvert.ToInt(lines[10]);
                    if (x == 0 || x == 3)
                        rec._Title = ContactTitle.Employee;
                    else if (x == 2)
                        rec._Title = ContactTitle.Sales;
                    else if (x == 4)
                        rec._Title = ContactTitle.Purchase;

                    rec._Dim1 = dim1?.Get(importLog.GetNotEmpty(lines[8]))?.KeyStr;
                    if (lines.Count > (12+2))
                        rec._Dim2 = dim2?.Get(importLog.GetNotEmpty(lines[12]))?.KeyStr;
                    if (lines.Count > (13 + 2))
                        rec._Dim3 = dim3?.Get(importLog.GetNotEmpty(lines[13]))?.KeyStr;

                    if (lines.Count > (18 + 2) && lines[18] != string.Empty)
                        rec._Mobil = lines[18];

                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);
                }
                var err = await log.Insert(lst.Values);
                var accs = lst.Values.ToArray();
                log.Employees = new SQLCache(accs, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static Dictionary<string, InvItemImport> importInv(importLog log)
        {
            if (!log.OpenFile("exp00049", "Varer"))
            {
                log.api.CompanyEntity.Inventory = false;
                return null;
            }

            try
            {
                List<string> lines;

                var grpCache = log.ItemGroups;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;
                var Creditors = log.Creditors;

                var lst = new Dictionary<string, InvItemImport>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(47)) != null)
                {
                    var rec = new InvItemImport();
                    rec._Item = lines[1];
                    if (rec._Item == string.Empty)
                        continue;

                    rec.C5RecId = (int)NumberConvert.ToInt(lines[lines.Count - 2]);
                    rec._Name = importLog.GetNotEmpty(lines[2]);
                    rec._Group = grpCache.Get(importLog.GetNotEmpty(lines[9]))?.KeyStr;
                    rec._PurchaseAccount = Creditors?.Get(importLog.GetNotEmpty(lines[13]))?.KeyStr;

                    if (lines[15] == "1")
                        rec._Blocked = true;

                    rec._PurchaseCurrency = (byte)log.ConvertCur(lines[7]);
                    if (rec._PurchaseCurrency != 0)
                        rec._PurchasePrice = importLog.ToDouble(lines[8]);
                    else
                        rec._CostPrice = importLog.ToDouble(lines[8]);

                    var desc = NumberConvert.ToInt(lines[18]);
                    if (desc > 0 && desc <= 6)
                        rec._Decimals = (byte)desc;
                    rec._Weight = importLog.ToDouble(lines[22]);
                    rec._Volume = importLog.ToDouble(lines[23]);
                    rec._TariffNumber = importLog.GetNotEmpty(lines[24]);
                    rec._StockPosition = importLog.GetNotEmpty(lines[31]);
                    if (rec._StockPosition == "0")
                        rec._StockPosition = null;
                    rec._Unit = importLog.ConvertUnit(lines[25]);

                    //cost price unit = importLog.ToDouble(lines[43]);

                    rec._Dim1 = dim1?.Get(importLog.GetNotEmpty(lines[42]))?.KeyStr;
                    if (lines.Count > (54 + 2))
                        rec._Dim2 = dim2?.Get(importLog.GetNotEmpty(lines[54]))?.KeyStr;
                    if (lines.Count > (55 + 2))
                        rec._Dim3 = dim3?.Get(importLog.GetNotEmpty(lines[55]))?.KeyStr;

                    switch (lines[5])
                    {
                        case "0": rec._ItemType = (byte)ItemType.Item; break;           //Item
                        case "1": rec._ItemType = (byte)ItemType.Service; break;        //Service
                        case "2": rec._ItemType = (byte)ItemType.ProductionBOM; break;  //BOM
                        case "3": rec._ItemType = (byte)ItemType.BOM; break;            //Kit
                    }

                    switch (lines[11])
                    {
                        case "0": rec._CostModel = CostType.FIFO; break;    //FIFO
                        case "1": rec._CostModel = CostType.FIFO; break;    //LIFO
                        case "2": rec._CostModel = CostType.Fixed; break;   //Cost price
                        case "3": rec._CostModel = CostType.Average; break; //Average
                        case "4": rec._CostModel = CostType.Fixed; break;   //Serial number
                    }

                    if (!lst.ContainsKey(rec.KeyStr))
                        lst.Add(rec.KeyStr, rec);

                    rec.alternativeItem = importLog.GetNotEmpty(lines[17]);
                    desc = NumberConvert.ToInt(lines[16]);
                    if (desc > 0 && desc <= 2)
                        rec._UseAlternative = (UseAlternativeItem)desc;
                }
                return lst;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
            return null;
        }

        public static async Task importInvBOM(importLog log)
        {
            if (!log.OpenFile("exp00059", "Styklister"))
            {
                return;
            }

            try
            {

                var Items = log.Items;

                List<string> lines;

                var lst = new List<InvBOM>();
                while ((lines = log.GetLine(5)) != null)
                {
                    var item = Items.Get(importLog.GetNotEmpty(lines[0]));
                    if (item == null)
                        continue;

                    var rec = new InvBOM();
                    rec._ItemMaster = item.KeyStr;

                    item = Items.Get(importLog.GetNotEmpty(lines[2]));
                    if (item == null)
                        continue;

                    rec._ItemPart = item.KeyStr;
                    rec._LineNumber = NumberConvert.ToFloat(lines[1]);
                    rec._Qty = NumberConvert.ToDouble(lines[3]);
                    rec._QtyType = BOMQtyType.Propertional;
                    rec._MoveType = BOMMoveType.SalesAndBuy;
  
                    lst.Add(rec);
                }
                if (lst.Count > 0)
                {
                    var err = await log.Insert(lst, true);
                    log.HasBOM = (err == 0);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importInvPrices(importLog log, Dictionary<string, InvItemImport> invItems)
        {
            List<InvPriceListLine> lst = null;
            int nPrice = 0;
            var PriceLists = log.PriceLists;
            var ItemCurFile = log.CurFile;

            if (PriceLists != null && log.OpenFile("exp00063", "Priser"))
            {
                try
                {
                    nPrice = PriceLists.Count;

                    List<string> lines;

                    InvItemImport item;
                    lst = new List<InvPriceListLine>();
                    while ((lines = log.GetLine(5)) != null)
                    {
                        if (!invItems.TryGetValue(lines[0], out item))
                            continue;

                        var priceList = PriceLists.Get(importLog.GetNotEmpty(lines[4]));
                        if (priceList == null)
                            continue;

                        var Price = importLog.ToDouble(lines[1]);

                        var prisId = priceList.RowId;
                        if (prisId <= 3)
                        {
                            byte priceCur = (byte)log.ConvertCur(lines[3]);
                            if (prisId == 1)
                            {
                                item._SalesPrice1 = Price;
                                item._Currency1 = priceCur;
                            }
                            else if (prisId == 2)
                            {
                                item._SalesPrice2 = Price;
                                item._Currency2 = priceCur;
                            }
                            else
                            {
                                item._SalesPrice3 = Price;
                                item._Currency3 = priceCur;
                            }
                        }

                        if (nPrice > 2)
                        {
                            var rec = new InvPriceListLine();
                            rec.SetMaster((UnicontaBaseEntity)priceList);
                            rec._DCType = 1;
                            rec._Item = item._Item;
                            rec._Price = Price;
                            lst.Add(rec);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Ex(ex);
                }
            }

            try
            {
                var PriceCurFile = log.CurFile;
                log.CurFile = ItemCurFile;

                var err = await log.Insert(invItems.Values);
                if (err == 0)
                {
                    log.HasItem = true;
                    var accs = invItems.Values.ToArray();
                    log.Items = new SQLCache(accs, true);
                }

                if (nPrice > 2)
                {
                    log.CurFile = PriceCurFile;

                    log.HasPriceList = true;
                    await log.Insert(lst, true);
                }

                var altItemLst = new List<InvItem>();
                foreach(var itm in invItems.Values)
                {
                    if (itm.alternativeItem != null)
                    {
                        itm._AlternativeItem = log.Items.Get(itm.alternativeItem)?.KeyStr;
                        altItemLst.Add(itm);
                    }
                }
                if (altItemLst.Count > 0)
                    log.Update(altItemLst);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importPaymentGroup(importLog log)
        {
            if (!log.OpenFile("exp00021", "Betalingsbetingelser"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int n = 0;
                var lst = new Dictionary<string, PaymentTerm>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(5)) != null)
                {
                    var rec = new PaymentTerm();
                    rec._Payment = lines[0];
                    if (string.IsNullOrWhiteSpace(rec._Payment))
                        continue;
                    rec._Name = lines[1];
                    rec._Days = GetInt(lines[3]);
                    var t = GetInt(lines[2]);
                    switch (t)
                    {
                        case 0: rec._PaymentMethod = PaymentMethodTypes.NetDays;  break;                    //Net
                        case 1: rec._PaymentMethod = PaymentMethodTypes.EndMonth; break;                    //End month
                        case 2: rec._PaymentMethod = PaymentMethodTypes.EndMonth; rec._Days += 90;  break;  //End quarter
                        case 3: rec._PaymentMethod = PaymentMethodTypes.EndMonth; rec._Days += 365; break;  //End year
                        case 4: rec._PaymentMethod = PaymentMethodTypes.EndWeek;  break;                    //End week
                     }
                    if (!lst.Any())
                        rec._Default = true;
                    if (!lst.ContainsKey(rec.KeyStr))
                    {
                        lst.Add(rec.KeyStr, rec);
                        if (++n == 255)
                            break;
                    }
                }
                var err = await log.Insert(lst.Values);
                if (err == 0)
                {
                    var accs = lst.Values.ToArray();
                    log.Payments = new SQLCache(accs, true);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static public CountryCode convertCountry(string str, CountryCode companyCode, out VatZones zone)
        {
            zone = VatZones.Domestic;
            if (! string.IsNullOrEmpty(str))
            {
                CountryCode code;
                if (Enum.TryParse(str, true, out code))
                {
                    if (code == companyCode)
                        zone = VatZones.Domestic;
                    else
                        zone = VatZones.EU;
                    return code;
                }

                switch (str.ToLower())
                {
                    case "": return companyCode;
                    case "dk":
                    case "danmark":
                        if (companyCode != CountryCode.Denmark)
                            zone = VatZones.Foreign;
                        return CountryCode.Denmark;
                    case "s":
                    case "se":
                    case "sverige": zone = VatZones.EU; return CountryCode.Sweden;
                    case "n":
                    case "no":
                    case "norge":
                        if (companyCode != CountryCode.Norway)
                            zone = VatZones.Foreign;
                        return CountryCode.Norway;
                    case "færøerne": zone = VatZones.EU; return CountryCode.FaroeIslands;
                    case "grønland": zone = VatZones.Domestic; return CountryCode.Greenland;
                    case "is":
                    case "island": zone = VatZones.EU; return CountryCode.Iceland;
                    case "d":
                    case "de":
                    case "tyskland": zone = VatZones.EU; return CountryCode.Germany;
                    case "nl":
                    case "holland": zone = VatZones.EU; return CountryCode.Netherlands;
                    case "b":
                    case "be":
                    case "belgien": zone = VatZones.EU; return CountryCode.Belgium;
                    case "pl":
                    case "polen": zone = VatZones.EU; return CountryCode.Poland;
                    case "Østrig":
                    case "at":
                    case "østrig": zone = VatZones.EU; return CountryCode.Austria;
                    case "ch":
                    case "schweiz": zone = VatZones.EU; return CountryCode.Switzerland;
                    case "f":
                    case "fr":
                    case "frankrig": zone = VatZones.EU; return CountryCode.France;
                    case "es":
                    case "spanien": zone = VatZones.EU; return CountryCode.Spain;
                    case "i":
                    case "italien": zone = VatZones.EU; return CountryCode.Italy;
                    case "grækenland": zone = VatZones.EU; return CountryCode.Greece;
                    case "uk":
                    case "gb":
                    case "greatbritain":
                    case "storbritannien":
                    case "england": zone = VatZones.EU; return CountryCode.UnitedKingdom;
                    case "rusland": zone = VatZones.Foreign; return CountryCode.Russia;
                    case "usa": zone = VatZones.Foreign; return CountryCode.UnitedStates;
                    case "korea": zone = VatZones.Foreign; return CountryCode.KoreaSouth;
                    case "kina": zone = VatZones.Foreign; return CountryCode.China;
                    case "brasilien": zone = VatZones.Foreign; return CountryCode.Brazil;
                    case "cypern": zone = VatZones.EU; return CountryCode.Cyprus;
                    case "hungary": zone = VatZones.EU; return CountryCode.Hungary;
                    case "bulgarien": zone = VatZones.EU; return CountryCode.Bulgaria;
                    case "romanien": zone = VatZones.EU; return CountryCode.Romania;
                    case "sl":
                    case "slovenien": zone = VatZones.Foreign; return CountryCode.Slovenia;
                    case "irland": zone = VatZones.EU; return CountryCode.Ireland;
                    case "litauen": zone = VatZones.EU; return CountryCode.Lithuania;
                    case "letland": zone = VatZones.EU; return CountryCode.Latvia;
                    case "estland": zone = VatZones.EU; return CountryCode.Estonia;
                    case "indien": zone = VatZones.Foreign; return CountryCode.India;
                    case "egypten": zone = VatZones.Foreign; return CountryCode.Egypt;
                    case "serbien": zone = VatZones.Foreign; return CountryCode.Serbia;
                    case "cz": zone = VatZones.EU; return CountryCode.CzechRepublic;
                    case "za": zone = VatZones.Foreign; return CountryCode.SouthAfrica;
                    case "mo": zone = VatZones.Foreign; return CountryCode.Macao;
                    case "my": zone = VatZones.Foreign; return CountryCode.Malaysia;
                    case "ca": zone = VatZones.Foreign; return CountryCode.Canada;
                    case "us": zone = VatZones.Foreign; return CountryCode.UnitedStates;

                }
            }
            return companyCode;
        }

        public static async Task<ErrorCodes> importCreditor(importLog log)
        {
            if (!log.OpenFile("exp00041", "kreditorer"))
            {
                return ErrorCodes.Succes;
            }

            try
            {
                List<string> lines;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;
                var Payments = log.Payments;
                var grpCache = log.CreGroups;

                var InvoiceAccs = new List<InvoiceAccounts>();

                var lst = new Dictionary<string, CreditorImport>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(45)) != null)
                {
                    var rec = new CreditorImport();
                    rec._Account = lines[1];
                    if (string.IsNullOrWhiteSpace(rec._Account))
                        continue;

                    rec.C5RecId = (int)NumberConvert.ToInt(lines[lines.Count - 2]);
                    rec._Group = grpCache.Get(importLog.GetNotEmpty(lines[11]))?.KeyStr;
                    rec._Name = importLog.GetNotEmpty(lines[2]);
                    rec._Address1 = importLog.GetNotEmpty(lines[3]);
                    rec._Address2 = importLog.GetNotEmpty(lines[4]);
                    rec._ZipCode = GetZipCode(lines[5]);
                    rec._City = GetCity(lines[5]);
                    if (string.IsNullOrEmpty(lines[30]))
                    {
                        rec._PaymentMethod = PaymentTypes.VendorBankAccount;
                        rec._PaymentId = lines[30];
                    }
                    rec._LegalIdent = importLog.GetNotEmpty(lines[31]);
                    if (lines.Count > (57 + 2))
                        rec._ContactEmail = importLog.GetNotEmpty(lines[57]);
                    rec._ContactPerson = importLog.GetNotEmpty(lines[7]);
                    rec._Phone = importLog.GetNotEmpty(lines[8]);
                    rec._Vat = log.GetVat_Validate(lines[25]);
                    rec._Dim1 = dim1?.Get(importLog.GetNotEmpty(lines[32]))?.KeyStr;
                    if (lines.Count > (61 + 2))
                        rec._Dim2 = dim2?.Get(importLog.GetNotEmpty(lines[61]))?.KeyStr;
                    if (lines.Count > (62 + 2))
                        rec._Dim3 = dim3?.Get(importLog.GetNotEmpty(lines[62]))?.KeyStr;

                    rec._PricesInclVat = lines[17] != "0";

                    rec._Country = convertCountry(lines[6], log.CompCountryCode, out rec._VatZone);
                    rec._Currency = log.ConvertCur(lines[18]);
                    rec._EndDiscountPct = importLog.ToDouble(lines[12]);

                    rec._Payment = Payments?.Get(importLog.GetNotEmpty(lines[20]))?.KeyStr;

                    if (!lst.ContainsKey(rec.KeyStr))
                    {
                        lst.Add(rec.KeyStr, rec);
                        if (lines[10] != string.Empty)
                            InvoiceAccs.Add(new InvoiceAccounts { Acc = rec._Account, InvAcc = lines[10] });
                    }
                }
                await log.Insert(lst.Values);
                log.HasCreditor = true;
                var accs = lst.Values.ToArray();
                log.Creditors = new SQLCache(accs, true);

                UpdateInvoiceAccount(log, InvoiceAccs, false);

                return ErrorCodes.Succes;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }
        }

        public class InvoiceAccounts
        {
            public string Acc, InvAcc;
        }

        static public void UpdateInvoiceAccount(importLog log, List<InvoiceAccounts> InvoiceAccs, bool isDeb)
        {
            if (InvoiceAccs.Count > 0)
            {
                var recs = new List<UnicontaBaseEntity>();
                foreach (var debUpd in InvoiceAccs)
                {
                    DCAccount rec;
                    if (isDeb)
                        rec = (DCAccount)log.Debtors.Get(debUpd.Acc);
                    else
                        rec = (DCAccount)log.Creditors.Get(debUpd.Acc);
                    if (rec != null)
                    {
                        rec._InvoiceAccount = debUpd.InvAcc;
                        recs.Add((UnicontaBaseEntity)rec);
                    }
                }
                log.Update(recs);
            }
        }

        public static async Task<ErrorCodes> importDebitor(importLog log, bool isInvoiceEmail)
        {
            if (!log.OpenFile("exp00033", "Debitorer"))
            {
                return ErrorCodes.Succes;
            }

            try
            { 
                List<string> lines;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;

                var PriceLists = log.PriceLists;
                var Employees = log.Employees;
                var Payments = log.Payments;
                var grpCache = log.DebGroups;

                var InvoiceAccs = new List<InvoiceAccounts>();

                var lst = new Dictionary<string, DebtorImport>(StringNoCaseCompare.GetCompare());
                while ((lines = log.GetLine(45)) != null)
                {
                    var rec = new DebtorImport();
                    rec._Account = lines[1];
                    if (string.IsNullOrWhiteSpace(rec._Account))
                        continue;

                    rec.C5RecId = (int)NumberConvert.ToInt(lines[lines.Count - 2]);
                    rec._Group = grpCache.Get(importLog.GetNotEmpty(lines[11]))?.KeyStr;
                    rec._Name = importLog.GetNotEmpty(lines[2]);
                    rec._Address1 = importLog.GetNotEmpty(lines[3]);
                    rec._Address2 = importLog.GetNotEmpty(lines[4]);
                    rec._ZipCode = GetZipCode(lines[5]);
                    rec._City = GetCity(lines[5]);
                    rec._LegalIdent = importLog.GetNotEmpty(lines[27]);
                    if (lines.Count > (52 + 2))
                    {
                        if (isInvoiceEmail)
                            rec._InvoiceEmail = importLog.GetNotEmpty(lines[52]);
                        else
                            rec._ContactEmail = importLog.GetNotEmpty(lines[52]);
                    }
                    rec._ContactPerson = importLog.GetNotEmpty(lines[7]);
                    rec._Phone = importLog.GetNotEmpty(lines[8]);
                    if (lines.Count > (58 + 2))
                        rec._EAN = importLog.GetNotEmpty(lines[58]);
                    rec._Vat = log.GetVat_Validate(lines[24]);
                    rec._Dim1 = dim1?.Get(importLog.GetNotEmpty(lines[29]))?.KeyStr;
                    if (lines.Count > (56 + 2))
                        rec._Dim2 = dim2?.Get(importLog.GetNotEmpty(lines[56]))?.KeyStr;
                    if (lines.Count > (57 + 2))
                        rec._Dim3 = dim3?.Get(importLog.GetNotEmpty(lines[57]))?.KeyStr;
                    if (lines[42] != string.Empty)
                        rec._CreditMax = importLog.ToDouble(lines[42]);

                    var priceRec = (InvPriceList)PriceLists?.Get(importLog.GetNotEmpty(lines[14]));
                    if (priceRec != null)
                    {
                        rec._PricesInclVat = priceRec._InclVat;
                        if (PriceLists.Count > 2)
                            rec._PriceList = priceRec.KeyStr;
                        if (priceRec.RowId <= 3)
                            rec._PriceGroup = (byte)priceRec.RowId;
                    }

                    //var t = (int)NumberConvert.ToInt(lines[11]);
                    //if (t > 0)
                    //    rec._VatZone = (VatZones)(t - 1);

                    rec._Country = convertCountry(lines[6], log.CompCountryCode, out rec._VatZone);
                    rec._Currency = log.ConvertCur(lines[18]);
                    rec._EndDiscountPct = importLog.ToDouble(lines[12]);

                    rec._Payment = Payments?.Get(importLog.GetNotEmpty(lines[20]))?.KeyStr;
                    rec._Employee = Employees?.Get(importLog.GetNotEmpty(lines[23]))?.KeyStr;

                    if (!lst.ContainsKey(rec.KeyStr))
                    {
                        lst.Add(rec.KeyStr, rec);
                        // lines[10]  invoice account 
                        if (lines[10] != string.Empty)
                            InvoiceAccs.Add(new InvoiceAccounts { Acc = rec._Account, InvAcc = lines[10] });
                    }
                }
                await log.Insert(lst.Values);
                log.HasDebitor = true;
                var accs = lst.Values.ToArray();
                log.Debtors = new SQLCache(accs, true);

                UpdateInvoiceAccount(log, InvoiceAccs, true);

                return ErrorCodes.Succes;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.Exception;
            }
        }

        public static async Task importContact(importLog log, byte DCType)
        {
            string filename = (DCType == 1) ? "exp00177" : "exp00178";

            if (!log.OpenFile(filename))
            {
                log.AppendLogLine(String.Format(Localization.lookup("FileNotFound"), filename + " : KontaktPerson"));
                return;
            }

            try
            {
                SQLCache dk;
                if (DCType == 1)
                    dk = log.Debtors;
                else
                    dk = log.Creditors;

                List<string> lines;

                var lst = new List<Contact>();
                while ((lines = log.GetLine(10)) != null)
                {
                    var rec = new Contact();
                    var dc = dk?.Get(importLog.GetNotEmpty(lines[0]));
                    if (dc != null)
                        rec.SetMaster(dc as UnicontaBaseEntity);

                    rec._Name = lines[2];
                    if (string.IsNullOrWhiteSpace(rec._Name))
                    {
                        rec._Name = lines[0];
                        if (string.IsNullOrWhiteSpace(rec._Name))
                            continue;
                    }

                    if (lines.Count >= 13+2)
                        rec._Mobil = lines[12] != "" ? lines[12] : lines[9];
                    rec._Email = lines[8];
                   
                    if (lines[1] != "0")
                        rec._AccountStatement = rec._InterestNote = rec._CollectionLetter = rec._Invoice = true;
                    lst.Add(rec);
                }
                await log.Insert(lst, true);
                if (lst.Count > 0)
                    log.HasContact = true;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static async Task importOrder(importLog log, Dictionary<long, DebtorOrder> orders, Dictionary<long, DebtorOffer> offers, Dictionary<long, lineNote> orderLineNotes)
        {
            if (!log.OpenFile("exp00127", "Salgsordre"))
            {
                return;
            }

            try
            {
                var Debtors = log.Debtors;
                var Employees = log.Employees;

                List<string> lines;
                int MaxOrderNumber = 0, MaxOfferNumber = 0;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;
                var Payments = log.Payments;
                var PriceLists = log.PriceLists;

                while ((lines = log.GetLine(60)) != null)
                {
                    var deb = (Debtor)Debtors.Get(importLog.GetNotEmpty(lines[5]));
                    if (deb == null)
                        continue;

                    var OrderNumber = (int)NumberConvert.ToInt(lines[1].Replace("-", ""));
                    if (OrderNumber == 0)
                        continue;

                    DCOrder rec;

                    int phase = 0;
                    if (lines.Count > (74 + 2))
                        phase = (int)NumberConvert.ToInt(lines[74]);
                    if (phase == 1)
                    {
                        var recOff = new DebtorOffer();
                        rec = recOff;
                        offers.Add(OrderNumber, recOff);

                        if (OrderNumber > MaxOfferNumber)
                            MaxOfferNumber = OrderNumber;
                    }
                    else
                    {
                        var recOrd = new DebtorOrder();
                        rec = recOrd;
                        orders.Add(OrderNumber, recOrd);

                        if (OrderNumber > MaxOrderNumber)
                            MaxOrderNumber = OrderNumber;
                    }

                    rec._OrderNumber = OrderNumber;
                    rec.SetMaster(deb);
                    if (deb._PriceList != null)
                    {
                        var plist = (InvPriceList)PriceLists.Get(deb._PriceList);
                        rec._PricesInclVat = plist._InclVat;
                    }
                    rec._DeliveryAddress1 = importLog.GetNotEmpty(lines[32]);
                    if (rec._DeliveryAddress1 != null)
                        log.HasDelivery = true;
                    rec._DeliveryAddress2 = importLog.GetNotEmpty(lines[33]);
                    rec._DeliveryAddress3 = importLog.GetNotEmpty(lines[34]);

                    if (lines[36] != string.Empty)
                    {
                        VatZones vatz;
                        rec._DeliveryCountry = c5.convertCountry(lines[36], log.CompCountryCode, out vatz);
                    }

                    if (lines[4] != string.Empty)
                        rec._DeliveryDate = GetDT(lines[4]);
                    if (lines[3] != string.Empty)
                        rec._Created = GetDT(lines[3]);

                    rec._Currency = log.ConvertCur(lines[20]);

                    rec._EndDiscountPct = importLog.ToDouble(lines[16]);
                    var val = importLog.ToDouble(lines[27]);
                    rec._DeleteLines = 
                    rec._DeleteOrder = (val <= 2);
                    
                    rec._Remark = importLog.GetNotEmpty(lines[23]);
                    rec._YourRef = importLog.GetNotEmpty(lines[37]);
                    rec._OurRef = importLog.GetNotEmpty(lines[38]);
                    rec._Requisition = importLog.GetNotEmpty(lines[39]);

                    rec._Payment = Payments?.Get(importLog.GetNotEmpty(lines[22]))?.KeyStr;
                    rec._Employee = Employees?.Get(importLog.GetNotEmpty(lines[25]))?.KeyStr;

                    rec._Dim1 = dim1?.Get(importLog.GetNotEmpty(lines[28]))?.KeyStr;
                    if (lines.Count > (69 + 2))
                        rec._Dim2 = dim2?.Get(importLog.GetNotEmpty(lines[69]))?.KeyStr;
                    if (lines.Count > (70 + 2))
                        rec._Dim3 = dim3?.Get(importLog.GetNotEmpty(lines[70]))?.KeyStr;

                    lineNote note;
                    var C5RowId = NumberConvert.ToInt(lines[lines.Count - 2]);
                    if (orderLineNotes.TryGetValue(C5RowId + (127 << 32), out note))
                        note.master = (UnicontaBaseEntity)rec;
                }

                if (orders.Count > 0)
                    await log.Insert(orders.Values);
                if (offers.Count > 0)
                    await log.Insert(offers.Values);

                var arr = await log.api.Query<CompanySettings>();
                if (arr != null && arr.Length > 0)
                {
                    if (MaxOrderNumber > 0)
                        arr[0]._SalesOrder = MaxOrderNumber;
                    if (MaxOfferNumber > 0)
                        arr[0]._SalesOffer = MaxOfferNumber;
                    log.api.UpdateNoResponse(arr[0]);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static async Task importOrderLinje(importLog log, Dictionary<long, DebtorOrder> orders, Dictionary<long, DebtorOffer> offers, Dictionary<long, lineNote> orderLineNotes)
        {
            if (!log.OpenFile("exp00128", "Ordrelinjer"))
            {
                return;
            }

            try
            {
                var Items = log.Items;
                if (Items == null)
                    return;

                List<string> lines;

                var lst = new List<DebtorOrderLine>();
                var lst2 = new List<DebtorOfferLine>();

                while ((lines = log.GetLine(22)) != null)
                {
                    var OrderNumber = NumberConvert.ToInt(lines[0].Replace("-", ""));
                    if (OrderNumber == 0)
                        continue;

                    DCOrderLine rec;
                    DebtorOrder order;
                    if (orders.TryGetValue(OrderNumber, out order))
                    {
                        var orderLin = new DebtorOrderLine();
                        orderLin.SetMaster(order);
                        lst.Add(orderLin);
                        rec = orderLin;
                    }
                    else
                    {
                        DebtorOffer offer;
                        if (!offers.TryGetValue(OrderNumber, out offer))
                            continue;

                        var offerLin = new DebtorOfferLine();
                        offerLin.SetMaster(offer);
                        lst2.Add(offerLin);
                        rec = offerLin;
                    }

                    rec._LineNumber = NumberConvert.ToDoubleNoThousandSeperator(lines[1]);
                    if (rec._LineNumber == 0d)
                        rec._LineNumber = -0.001d;

                    InvItem item;
                    if (!string.IsNullOrEmpty(lines[2]))
                    {
                        item = (InvItem)Items.Get(importLog.GetNotEmpty(lines[2]));
                        if (item != null)
                        {
                            rec._Item = item._Item;
                            rec._CostPrice = item._CostPrice;
                        }
                    }
                    else
                        item = null;
                    rec._Text = importLog.GetNotEmpty(lines[8]);
                    rec._Qty = importLog.ToDouble(lines[4]);
                    rec._Price = importLog.ToDouble(lines[5]);
                    rec._DiscountPct = importLog.ToDouble(lines[6]);
                    var amount = importLog.ToDouble(lines[7]);
                    if (rec._Price * rec._Qty == 0d && amount != 0)
                        rec._AmountEntered = amount;

                    rec._QtyNow = importLog.ToDouble(lines[11]);
                    if (rec._QtyNow == rec._Qty)
                        rec._QtyNow = 0d;

                    rec._Date = GetDT(lines[13]);

                    rec._Unit = importLog.ConvertUnit(lines[9]);
                    if (item != null && item._Unit == rec._Unit)
                        rec._Unit = 0;

                    if (rec._CostPrice == 0d)
                        rec._CostPrice = importLog.ToDouble(lines[21]);

                    lineNote noteRec;
                    var noteId = ((long)128 << 32) + NumberConvert.ToInt(lines[lines.Count-2]);
                    if (orderLineNotes.TryGetValue(noteId, out noteRec))
                        rec._Note = noteRec.Note;
                }

                if (lst.Count > 0)
                    await log.Insert(lst, true);
                if (lst2.Count > 0)
                    await log.Insert(lst2, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static async Task importPurchage(importLog log, Dictionary<long, CreditorOrder> purchages, Dictionary<long, lineNote> orderLineNotes)
        {
            if (!log.OpenFile("exp00125", "Indkøbssordre"))
            {
                return;
            }

            try
            {
                var Creditors = log.Creditors;
                var Employees = log.Employees;

                List<string> lines;
                int MaxOrderNumber = 0;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;
                var Payments = log.Payments;
                var PriceLists = log.PriceLists;

                while ((lines = log.GetLine(60)) != null)
                {
                    var rec = new CreditorOrder();
                    rec._OrderNumber = (int)NumberConvert.ToInt(lines[1].Replace("-", ""));
                    if (rec._OrderNumber == 0)
                        continue;
                    if (rec._OrderNumber > MaxOrderNumber)
                        MaxOrderNumber = rec._OrderNumber;

                    var cre = (Creditor)Creditors.Get(importLog.GetNotEmpty(lines[5]));
                    if (cre == null)
                        continue;
                    rec.SetMaster(cre);
                    rec._PricesInclVat = lines[16] == "1";
                    rec._DeliveryAddress1 = importLog.GetNotEmpty(lines[32]);
                    if (rec._DeliveryAddress1 != null)
                        log.HasDelivery = true;
                    rec._DeliveryAddress2 = importLog.GetNotEmpty(lines[33]);
                    rec._DeliveryAddress3 = importLog.GetNotEmpty(lines[34]);
                    if (lines[4] != string.Empty)
                        rec._DeliveryDate = GetDT(lines[4]);
                    if (lines[3] != string.Empty)
                        rec._Created = GetDT(lines[3]);

                    rec._Currency = log.ConvertCur(lines[20]);

                    rec._EndDiscountPct = importLog.ToDouble(lines[17]);
                    var val = importLog.ToDouble(lines[27]);
                    rec._DeleteLines =
                    rec._DeleteOrder = (val <= 2);

                    rec._Remark = importLog.GetNotEmpty(lines[23]);
                    rec._YourRef = importLog.GetNotEmpty(lines[37]);
                    rec._OurRef = importLog.GetNotEmpty(lines[38]);
                    rec._Requisition = importLog.GetNotEmpty(lines[39]);

                    rec._Payment = Payments?.Get(importLog.GetNotEmpty(lines[22]))?.KeyStr;
                    rec._Employee = Employees?.Get(importLog.GetNotEmpty(lines[25]))?.KeyStr;

                    rec._Dim1 = dim1?.Get(importLog.GetNotEmpty(lines[28]))?.KeyStr;
                    if (lines.Count > (69 + 2))
                        rec._Dim2 = dim2?.Get(importLog.GetNotEmpty(lines[69]))?.KeyStr;
                    if (lines.Count > (70 + 2))
                        rec._Dim3 = dim3?.Get(importLog.GetNotEmpty(lines[70]))?.KeyStr;

                    purchages.Add(rec._OrderNumber, rec);

                    lineNote note;
                    var C5RowId = NumberConvert.ToInt(lines[lines.Count - 2]);
                    if (orderLineNotes.TryGetValue(C5RowId + (125 << 32), out note))
                        note.master = rec;
                }

                await log.Insert(purchages.Values);

                var arr = await log.api.Query<CompanySettings>();
                if (arr != null && arr.Length > 0)
                {
                    arr[0]._PurchaceOrder = MaxOrderNumber;
                    log.api.UpdateNoResponse(arr[0]);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        static async Task importPurchageLinje(importLog log, Dictionary<long, CreditorOrder> purchages, Dictionary<long, lineNote> orderLineNotes)
        {
            if (!log.OpenFile("exp00126", "Indkøbslinjer"))
            {
                return;
            }

            try
            {
                var Items = log.Items;
                if (Items == null)
                    return;

                List<string> lines;

                var lst = new List<CreditorOrderLine>();

                while ((lines = log.GetLine(22)) != null)
                {
                    var OrderNumber = NumberConvert.ToInt(lines[0].Replace("-", ""));
                    if (OrderNumber == 0)
                        continue;

                    CreditorOrder order;
                    if (!purchages.TryGetValue(OrderNumber, out order))
                        continue;

                    var rec = new CreditorOrderLine();
                    rec._LineNumber = NumberConvert.ToDoubleNoThousandSeperator(lines[1]);
                    if (rec._LineNumber == 0d)
                        rec._LineNumber = -0.001d;

                    InvItem item;
                    if (!string.IsNullOrEmpty(lines[2]))
                    {
                        item = (InvItem)Items.Get(importLog.GetNotEmpty(lines[2]));
                        if (item == null)
                            continue;
                        rec._Item = item._Item;
                    }
                    else
                        item = null;

                    rec._Text = importLog.GetNotEmpty(lines[8]);
                    rec._Qty = importLog.ToDouble(lines[4]);
                    rec._Price = importLog.ToDouble(lines[5]);
                    rec._DiscountPct = importLog.ToDouble(lines[6]);
                    var amount = importLog.ToDouble(lines[7]);
                    if (rec._Price * rec._Qty == 0d && amount != 0)
                        rec._AmountEntered = amount;

                    rec._QtyNow = importLog.ToDouble(lines[11]);
                    if (rec._QtyNow == rec._Qty)
                        rec._QtyNow = 0d;

                    rec._Date = GetDT(lines[13]);

                    rec._Unit = importLog.ConvertUnit(lines[9]);
                    if (item != null && item._Unit == rec._Unit)
                        rec._Unit = 0;

                    rec.SetMaster(order);
                    lst.Add(rec);

                    lineNote noteRec;
                    var noteId = ((long)126 << 32) + NumberConvert.ToInt(lines[lines.Count - 2]);
                    if (orderLineNotes.TryGetValue(noteId, out noteRec))
                        rec._Note = noteRec.Note;
                }

                await log.Insert(lst, true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static void importCompany(importLog log)
        {
            if (!log.OpenFile("exp00010", "Firmaoplysninger"))
            {
                return;
            }

            try
            {
                List<string> lines;

                if ((lines = log.GetLine(28)) != null)
                {
                    var rec = log.api.CompanyEntity;
                    rec._Address1 = lines[1];
                    rec._Address2 = lines[2];
                    rec._Address3 = lines[3];
                    rec._Phone = lines[4];
                    rec._Id = lines[21];
                    rec._NationalBank = lines[6];
                    rec._FIK = lines[7];
                    if (string.IsNullOrWhiteSpace(rec._FIK))
                        rec._FIK = lines[28];
                    rec._IBAN = lines[27];
                    rec._SWIFT = lines[25];
                    rec._FIKDebtorIdPart = 7;

                    if (log.HasDepartment)
                    {
                        rec.NumberOfDimensions = 1;
                        rec._Dim1 = "Afdeling";
                    }
                    if (log.HasCentre)
                    {
                        rec.NumberOfDimensions = 2;
                        rec._Dim2 = "Bærer";
                    }
                    if (log.HasPorpose)
                    {
                        rec.NumberOfDimensions = 3;
                        rec._Dim3 = "Formål";
                    }

                    log.NumberOfDimensions = rec.NumberOfDimensions;
                    log.api.UpdateNoResponse(rec);
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static Currencies GetCurrency(importLog log)
        {
            if (log.OpenFile("exp00006", "Parameters"))
            {
                try
                {
                    List<string> lines;
                    while ((lines = log.GetLine(3)) != null)
                    {
                        var s = lines[1];
                        if (s == "ParametersLedger" || s == "ParametreFinans")
                            return log.ConvertCur(lines[2]);
                    }
                }
                catch (Exception ex)
                {
                    log.Ex(ex);
                }
            }
            return 0;
        }

        public static async Task importDepartment(importLog log)
        {
            if (!log.OpenFile("exp00017", "Afdelinger"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int n = 0;
                var lst = new List<GLDimType1>();
                while ((lines = log.GetLine(3)) != null)
                {
                    var rec = new GLDimType1();
                    rec._Dim = importLog.GetNotEmpty(lines[0]);
                    if (rec._Dim == null)
                        continue;
                    rec._Name = importLog.GetNotEmpty(lines[1]);
                    lst.Add(rec);

                    log.HasDepartment = true;
                    if (++n == 10000)
                    {
                        log.AppendErrorLogLine("Man kan ikke have mere end 10.000 afdelinger");
                        break;
                    }
                }
                await log.Insert(lst);

                if (log.HasDepartment)
                {
                    log.dim1 = new SQLCache(lst.ToArray(), true);
                    log.api.CompanyEntity._Dim1 = "Afdeling";
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importCentre(importLog log)
        {
            if (!log.OpenFile("exp00182", "Bærer"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int n = 0;
                var lst = new List<GLDimType2>();
                while ((lines = log.GetLine(2)) != null)
                {
                    var rec = new GLDimType2();
                    rec._Dim = importLog.GetNotEmpty(lines[0]);
                    if (rec._Dim == null)
                        continue;
                    rec._Name = importLog.GetNotEmpty(lines[1]);
                    lst.Add(rec);

                    log.HasCentre = true;
                    if (++n == 10000)
                    {
                        log.AppendErrorLogLine("Man kan ikke have mere end 10.000 bærer");
                        break;
                    }
                }
                await log.Insert(lst);

                if (log.HasCentre)
                    log.dim2 = new SQLCache(lst.ToArray(), true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static async Task importPurpose(importLog log)
        {
            if (!log.OpenFile("exp00183", "Formål"))
            {
                return;
            }

            try
            {
                List<string> lines;
                int n = 0;
                var lst = new List<GLDimType3>();
                while ((lines = log.GetLine(2)) != null)
                {
                    var rec = new GLDimType3();
                    rec._Dim = importLog.GetNotEmpty(lines[0]);
                    if (rec._Dim == null)
                        continue;
                    rec._Name = importLog.GetNotEmpty(lines[1]);
                    lst.Add(rec);

                    log.HasPorpose = true;
                    if (++n == 10000)
                    {
                        log.AppendErrorLogLine("Man kan ikke have mere end 10.000 formål");
                        break;
                    }
                }
                await log.Insert(lst);

                if (log.HasPorpose)
                    log.dim3 = new SQLCache(lst.ToArray(), true);
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
        }

        public static void PostDK_NotFound(importLog log, DCImportTrans[] arr, List<GLPostingLineLocal>[] YearLst, GLJournalAccountType dktype)
        {
            SQLCache Accounts = dktype == GLJournalAccountType.Debtor ? log.Debtors : log.Creditors;
            SQLCache Groups = dktype == GLJournalAccountType.Debtor ? log.DebGroups : log.CreGroups;
            var years = log.years;
            int nYears = years.Length;

            for (int n = arr.Length; (--n >= 0);)
            {
                var p = arr[n];
                if (p.Taken || p.Amount == 0)
                    continue;

                // we did not find this transaaction in the ledger. we need to generate 2 new transaction, that will net each other out
                var rec = new GLPostingLineLocal();
                rec.AccountType = dktype;
                rec.Account = p.Account;
                rec.Voucher = p.Voucher;
                rec.Text = p.Text;
                rec.Date = p.Date;
                rec.Invoice = p.Invoice;
                rec.Amount = p.Amount;
                rec.Currency = p.Currency;
                rec.AmountCur = p.AmountCur;

                var ac = (DCAccount)Accounts.Get(p.Account);
                if (ac == null)
                    continue;
                var grp = (DCGroup)Groups.Get(ac._Group);
                if (grp == null)
                    continue;

                ConvertDKText(rec, ac);

                var Offset = new GLPostingLineLocal();
                Offset.Account = grp._SummeryAccount;
                Offset.Voucher = rec.Voucher;
                Offset.Text = rec.Text;
                Offset.Date = rec.Date;
                Offset.Invoice = rec.Invoice;
                Offset.DCPostType = rec.DCPostType;
                Offset.Amount = -rec.Amount;
                Offset.Currency = rec.Currency;
                Offset.AmountCur = -rec.AmountCur;

                var Date = rec.Date;
                for (int i = nYears; (--i >= 0);)
                {
                    var y = years[i];
                    if (y._FromDate <= Date && y._ToDate >= Date)
                    {
                        var lst = YearLst[i];
                        lst.Add(rec);
                        lst.Add(Offset);

                        if (p.dif != 0d)
                        {
                            var kursDif = new GLPostingLineLocal();
                            kursDif.Date = rec.Date;
                            kursDif.Voucher = rec.Voucher;
                            kursDif.Text = rec.Text;
                            kursDif.AccountType = dktype;
                            kursDif.Account = rec.Account;
                            kursDif.Amount = p.dif;
                            kursDif.DCPostType = DCPostType.ExchangeRateDif;
                            lst.Add(kursDif);

                            kursDif = new GLPostingLineLocal();
                            kursDif.Date = rec.Date;
                            kursDif.Voucher = rec.Voucher;
                            kursDif.Text = "Ophævet kursdif fra konvertering";
                            kursDif.Account = Offset.Account;
                            kursDif.Amount = -p.dif;
                            kursDif.DCPostType = DCPostType.ExchangeRateDif;
                            lst.Add(kursDif);
                        }
                        break;
                    }
                }
            }
        }

        static public void UpdateLedgerTrans(GLPostingLineLocal[] arr)
        {
            int voucher = 0;
            long Invoice = 0;
            DCPostType posttype = 0;
            bool clearText = false;
            string orgText = null;
            foreach (var rec in arr)
            {
                if (rec.AccountType > 0)
                {
                    Invoice = rec.Invoice;
                    voucher = rec.Voucher;
                    posttype = rec.DCPostType;
                    clearText = (rec.Text == null);
                    orgText = rec.OrgText;
                }
                else if (voucher == rec.Voucher)
                {
                    rec.Invoice = Invoice;
                    rec.DCPostType = posttype;
                    if (clearText && rec.Text == orgText)
                        rec.Text = null;
                }
            }
        }

        static async Task<ErrorCodes> postPeriod(importLog log, GLPostingHeader header, GLPostingLineLocal[] arr)
        {
            var ap = new Uniconta.API.GeneralLedger.PostingAPI(log.api);
            var res = await ap.PostJournal(header, arr, false);
            if (res.Err != 0)
            {
                log.AppendLogLine("Fejl i poster i " + header.Comment);
                await log.Error(res.Err);
                return res.Err;
            }
            log.AppendLogLine("Posting " + header.Comment);
            return 0;
        }

        public static async Task<ErrorCodes> postYear(importLog log, CompanyFinanceYear year, GLPostingHeader header, GLPostingLineLocal[] arr)
        {
            const int maxPost = 25000;

            header.Comment = string.Format("Import {0} - {1}", year._FromDate.ToShortDateString(), year._ToDate.ToShortDateString());

            if (arr.Length <= maxPost)
                return await postPeriod(log, header, arr);
            else
            {
                int n = arr.Length;
                int start = 0;
                long sum = 0;
                int lastVoucher = 0;
                for (int pos = 0; (pos < n); pos++)
                {
                    var rec = arr[pos];
                    if ((pos + 1 == n) ||
                        ((sum == 0) &&
                         ((((pos - start) > maxPost && rec.Voucher != lastVoucher) || (pos - start) > maxPost*2))
                        )
                       )
                    {
                        var len = pos - start;
                        if (pos + 1 == n)
                            len++;
                        var arrTmp = new GLPostingLineLocal[len];
                        Array.Copy(arr, start, arrTmp, 0, len);
                        var res = await postPeriod(log, header, arrTmp);
                        if (res != 0)
                            return res;

                        start = pos;
                    }
                    lastVoucher = rec.Voucher;
                    sum += NumberConvert.ToLong(rec.Amount * 100d);
                }
                return 0;
            }
        }

        static public void ConvertDKText(GLPostingLineLocal rec, DCAccount dc)
        {
            rec.Account = dc._Account;
            var t = rec.Text;
            if (t != null && t.Length > 6)
            {
                int index = 0;

                // "Fa:13325 D:33936765" eller Kn:13328 D:97911111"
                if (t.StartsWith("Fa:"))
                    rec.DCPostType = DCPostType.Invoice;
                else if (t.IndexOf("fakt", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    rec.DCPostType = DCPostType.Invoice;
                    index = 1;
                }
                else if (t.StartsWith("Kn:"))
                    rec.DCPostType = DCPostType.Creditnote;
                else if (t.IndexOf("kreditno", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    rec.DCPostType = DCPostType.Creditnote;
                    index = 1;
                }
                else if (t.StartsWith("Udlign"))
                {
                    rec.DCPostType = DCPostType.Payment;
                    index = 2;
                }
                else if (t.IndexOf("betal", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    rec.DCPostType = DCPostType.Payment;
                    index = 1;
                }
                else
                {
                    index = -1;
                    var name = ((DCAccount)dc)._Name;
                    if (name != null)
                    {
                        name = name.Replace(".", "").Replace(" ", "");
                        t = t.Replace(".", "").Replace(" ", "");
                        if (t.IndexOf(name, StringComparison.CurrentCultureIgnoreCase) >= 0 || name.IndexOf(t, StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            rec.OrgText = rec.Text;
                            rec.Text = null;
                        }
                    }
                }
                if (index >= 0 && rec.DCPostType != 0)
                {
                    if (rec.Invoice == 0)
                    {
                        var tt = t.Split(' ');
                        if (tt.Length > index)
                        {
                            t = tt[index];
                            var l = t.Length;
                            long inv = 0;
                            for (int i = 0; i < l; i++)
                            {
                                var ch = t[i];
                                if (ch >= '0' && ch <= '9')
                                    inv = inv * 10 + (ch - '0');
                                else if (inv != 0)
                                    break;
                            }
                            if (inv != 0)
                                rec.Invoice = inv;
                        }
                    }
                    if (rec.Invoice != 0)
                    {
                        rec.OrgText = rec.Text;
                        rec.Text = null;
                    }
                }
            }
        }

        public static async Task<ErrorCodes> importGLTrans(importLog log, DCImportTrans[] debpost, DCImportTrans[] crepost)
        {
            if (!log.OpenFile("exp00030", "Finanspostering"))
            {
                return 0;
            }

            try
            {
                List<string> lines;

                //log.years = await log.api.Query<CompanyFinanceYear>();

                var Ledger = log.LedgerAccounts;

                var dim1 = log.dim1;
                var dim2 = log.dim2;
                var dim3 = log.dim3;

                DCImportTrans dksearch = new DCImportTrans();
                var dkcmp = new DCImportTransSort();

                var years = log.years;
                int lastYearIdx = 0;

                DateTime firstValidDate = years[0]._FromDate;
                int nYears = years.Length;
                List<GLPostingLineLocal>[] YearLst = new List<GLPostingLineLocal>[nYears];
                for (int i = nYears; (--i >= 0);)
                    YearLst[i] = new List<GLPostingLineLocal>();

                var primoPost = new List<GLPostingLineLocal>();
                DateTime primoYear = DateTime.MaxValue;

                int cnt = 0;
                while ((lines = log.GetLine(17)) != null)
                {
                    if (lines[1] != "0") //Not budget and primo.
                        continue;

                    bool IsPrimo;
                    var Date = GetDT(log, lines[3], out IsPrimo);
                    if (Date < firstValidDate)
                        continue;

                    GLPostingLineLocal kursDif = null;

                    var rec = new GLPostingLineLocal();
                    rec.Date = Date;
                    rec.Voucher = GetInt(lines[4]);
                    if (rec.Voucher == 0)
                        rec.Voucher = 1;

                    rec.Text = lines[5];
                    rec.Amount = importLog.ToDouble(lines[6]);

                    var cur = log.ConvertCur(lines[8]);
                    if (cur != 0)
                    {
                        rec.Currency = cur;
                        rec.IgnoreCurDif = true; // we do not want to do any regulation in a conversion
                        rec.AmountCur = importLog.ToDouble(lines[7]);
                        if (rec.Amount * rec.AmountCur < 0d) // different sign
                        {
                            rec.Currency = null;
                            rec.AmountCur = 0;
                        }
                    }

                    var orgAccount = log.GLAccountFromC5(lines[0]);
                    var acc = (MyGLAccount)Ledger.Get(orgAccount);
                    if (acc == null || !acc._PostingAccount)
                    {
                        acc = (MyGLAccount)Ledger.Get(log.errorAccount);
                        if (acc == null || !acc._PostingAccount)
                            continue;
                    }
                    rec.Account = acc._Account;

                    if (Date < primoYear) // we have an earlier date than primo year. then we mark that as the first allowed primo date.
                    {
                        primoYear = Date;
                        primoPost.Clear();
                    }

                    if (IsPrimo)
                    {
                        if (Date == primoYear) // we only take primo from first year.
                        {
                            rec.primo = 1;
                            primoPost.Add(rec);
                        }
                        continue;
                    }

                    if (acc.AccountTypeEnum == GLAccountTypes.Creditor || acc.AccountTypeEnum == GLAccountTypes.Debtor)
                    {
                        // here we convert account to debtor / creditor.
                        var arr = (acc.AccountTypeEnum == GLAccountTypes.Debtor) ? debpost : crepost;
                        if (arr != null)
                        {
                            dksearch.Amount = rec.Amount;
                            dksearch.Date = rec.Date;
                            dksearch.Voucher = rec.Voucher;

                            DCImportTrans post = null;
                            var idx = Array.BinarySearch(arr, dksearch, dkcmp);
                            if (idx >= 0)
                            {
                                for (int i = idx; (i >= 0); i--)
                                {
                                    post = arr[i];
                                    var c = dkcmp.Compare(post, dksearch);
                                    if (c == 0 && !post.Taken)
                                        break;
                                    post = null;
                                    if (c < 0)
                                        break;
                                }
                                if (post == null)
                                {
                                    while (++idx < arr.Length)
                                    {
                                        post = arr[idx];
                                        var c = dkcmp.Compare(post, dksearch);
                                        if (c == 0 && !post.Taken)
                                            break;
                                        post = null;
                                        if (c > 0)
                                            break;
                                    }
                                }
                            }

                            /*
                            if (post == null)
                            {
                                var l = arr.Length;
                                for (int i = 0; (i < l); i++)
                                {
                                    var p = arr[i];
                                    if (!p.Taken)
                                    {
                                        if (dkcmp.Compare(p, dksearch) == 0)
                                        {
                                            post = p;
                                            break;
                                        }
                                    }
                                }
                            }
                            */
                            if (post != null)
                            {
                                IdKey dc;
                                if (acc.AccountTypeEnum == GLAccountTypes.Debtor)
                                {
                                    dc = log.Debtors.Get(post.Account);
                                    if (dc != null)
                                        rec.AccountType = GLJournalAccountType.Debtor;
                                }
                                else
                                {
                                    dc = log.Creditors.Get(post.Account);
                                    if (dc != null)
                                        rec.AccountType = GLJournalAccountType.Creditor;

                                }
                                if (dc != null)
                                {
                                    post.Taken = true;
                                    rec.Account = post.Account;
                                    rec.Invoice = post.Invoice;

                                    ConvertDKText(rec, (DCAccount)dc);

                                    if (post.dif != 0d)
                                    {
                                        rec.Amount += post.dif;
                                        if (rec.Amount * rec.AmountCur < 0d) // different sign
                                        {
                                            rec.Currency = null;
                                            rec.AmountCur = 0;
                                        }

                                        kursDif = new GLPostingLineLocal();
                                        kursDif.Date = rec.Date;
                                        kursDif.Voucher = rec.Voucher;
                                        kursDif.Text = "Kursdif fra konvertering";
                                        kursDif.Account = acc._Account;
                                        kursDif.Amount = -post.dif;
                                        kursDif.DCPostType = DCPostType.ExchangeRateDif;

                                        post.dif = 0d;
                                    }
                                }
                            }
                        }
                    }
                    else if (acc._MandatoryTax != VatOptions.NoVat &&
                        acc.AccountTypeEnum != GLAccountTypes.Equity &&
                        acc.AccountTypeEnum != GLAccountTypes.Bank &&
                        acc.AccountTypeEnum != GLAccountTypes.LiquidAsset)
                    {
                        rec.Vat = log.GetVat_Validate(lines[9]);
                        if (rec.Vat != string.Empty)
                            acc.HasVat = true;
                        rec.VatHasBeenDeducted = true;
                    }

                    rec.SetDim(1, dim1?.Get(importLog.GetNotEmpty(lines[2]))?.KeyStr);
                    if (lines.Count > (19 + 2))
                        rec.SetDim(2, dim2?.Get(importLog.GetNotEmpty(lines[19]))?.KeyStr);
                    if (lines.Count > (20 + 2))
                        rec.SetDim(3, dim3?.Get(importLog.GetNotEmpty(lines[20]))?.KeyStr);

                    var y = years[lastYearIdx];
                    if (!(y._FromDate <= Date && y._ToDate >= Date))
                    {
                        lastYearIdx = -1;
                        for (int i = nYears; (--i >= 0);)
                        {
                            y = years[i];
                            if (y._FromDate <= Date && y._ToDate >= Date)
                            {
                                lastYearIdx = i;
                                break;
                            }
                        }
                    }
                    if (lastYearIdx >= 0)
                    {
                        YearLst[lastYearIdx].Add(rec);
                        if (kursDif != null)
                            YearLst[lastYearIdx].Add(kursDif);
                    }
                    else
                        lastYearIdx = 0;
                    cnt++;
                }

                if (primoPost.Any())
                {
                    for (int i = nYears; (--i >= 0);)
                    {
                        var y = years[i];
                        if (y._FromDate <= primoYear && y._ToDate >= primoYear)
                        {
                            YearLst[i].AddRange(primoPost);
                            cnt += primoPost.Count;
                            primoPost = null;
                            break;
                        }
                    }
                }

                log.AppendLogLine(string.Format("Number of transactions = {0}", cnt));

                if (debpost != null)
                    PostDK_NotFound(log, debpost, YearLst, GLJournalAccountType.Debtor);
                if (crepost != null)
                    PostDK_NotFound(log, crepost, YearLst, GLJournalAccountType.Creditor);

                log.progressBar.Maximum = nYears;

                var glSort = new GLTransSort();
                GLPostingHeader header = new GLPostingHeader();
                header.NumberSerie = "NR";
                header.ThisIsConversion = true;
                header.NoDateSum = true; // 
                for (int i = 0; (i < nYears); i++)
                {
                    if (log.errorAccount != null)
                    {
                        long sum = 0;
                        foreach (var rec in YearLst[i])
                            sum += NumberConvert.ToLong(rec.Amount * 100d);

                        if (sum != 0)
                        {
                            var rec = new GLPostingLineLocal();
                            rec.Date = years[i]._ToDate;
                            rec.Account = log.GLAccountFromC5(log.errorAccount);
                            rec.Voucher = 99999;
                            rec.Text = "Ubalance ved import fra C5";
                            rec.Amount = sum / -100d;
                            YearLst[i].Add(rec);
                        }
                    }
                    var arr = YearLst[i].ToArray();
                    YearLst[i] = null;
                    if (arr.Length > 0)
                    {
                        Array.Sort(arr, glSort);
                        UpdateLedgerTrans(arr);
                        var ret = await postYear(log, years[i], header, arr);
                        if (ret != 0)
                            return ret;
                    }
                    StartImport.UpdateProgressbar(i, null, log.progressBar);
                }

                foreach (var ac in (MyGLAccount[])Ledger.GetNotNullArray)
                {
                    if (!ac.HasVat && ac._MandatoryTax != VatOptions.NoVat)
                    {
                        ac._MandatoryTax = VatOptions.NoVat;
                        ac.HasChanges = true;
                    }
                }

                c5.UpdateVATonAccount(log);

                log.AppendLogLine("Generate Primo Transactions");

                var ap2 = new FinancialYearAPI(log.api);
                for (int i = 1; (i < nYears); i++)
                {
                    var ok = await ap2.GeneratePrimoTransactions(years[i], null, null, 9999, "NR");
                    if (ok != 0)
                        break;
                }

                var dcApi = new Uniconta.API.DebtorCreditor.MaintableAPI(log.api);
                await dcApi.SettleAllAfterConversion();

                StartImport.UpdateProgressbar(0, "Done", log.progressBar);

                return 0;
            }
            catch (Exception ex)
            {
                log.Ex(ex);
                return ErrorCodes.SQLException;
            }
        }

        public class GLTransSort : IComparer<GLPostingLineLocal>
        {
            public int Compare(GLPostingLineLocal x, GLPostingLineLocal y)
            {
                int c = DateTime.Compare(x.Date, y.Date);
                if (c != 0)
                    return c;
                c = y.primo - x.primo;
                if (c != 0)
                    return c;
                c = x.Voucher - y.Voucher;
                if (c != 0)
                    return c;
                c = (int)y.AccountType - (int)x.AccountType;
                if (c != 0)
                    return c;
                var v = x.Amount - y.Amount;
                if (v > 0.001d)
                    return 1;
                if (v < -0.001d)
                    return 1;
                return 0;
            }
        }

        public class DCImportTransSort : IComparer<DCImportTrans>
        {
            public int Compare(DCImportTrans x, DCImportTrans y)
            {
                var c = DateTime.Compare(x.Date, y.Date);
                if (c != 0)
                    return c;
                c = x.Voucher - y.Voucher;
                if (c != 0)
                    return c;
                var v = x.Amount - y.Amount;
                if (v > 0.001d)
                    return 1;
                if (v < -0.001d)
                    return -1;
                return 0;
            }
        }

        public class DCImportTrans
        {
            public DateTime Date;
            public DateTime DueDate;
            public DateTime DocumentDate;
            public double Amount;
            public double AmountCur;
            public double dif;
            public Currencies? Currency;
            public string Account;
            public string Text;
            public int Voucher;
            public long Invoice;
            public bool Taken;
        }

        public static int GetInt(string str)
        {
            if (string.IsNullOrEmpty(str))
                return 0;
            var l = str.Length;
            if (l > 9)
                str = str.Substring(l - 9);
            return (int)NumberConvert.ToInt(str);
        }

        public static DateTime GetDT(string str)
        {
            if (string.IsNullOrEmpty(str))
                return DateTime.MinValue;
            bool IsPrimo;
            return GetDT(null, str, out IsPrimo);
        }

        public static DateTime GetDT(importLog log, string str, out bool IsPrimo)
        {
            IsPrimo = false;
            if (str.Length < 10)
            {
                if (log != null)
                    log.AppendLogLine(string.Format("Error in date:'{0}'", str));
                return DateTime.MinValue;
            }

            int year = GetInt(str.Substring(0, 4));
            int month = GetInt(str.Substring(5, 2));
            if (month < 1 || month > 12)
            {
                if (log != null)
                    log.AppendErrorLogLine("Error in month: " + str);
                return DateTime.MinValue;
            }

            try
            {
                var daystr = str.Substring(8, 2);
                int dayinmonth = 0;
                if (daystr == "PR" || daystr == "OP")
                {
                    IsPrimo = true;
                    dayinmonth = 1;
                }
                else if (daystr == "UL")
                    dayinmonth = DateTime.DaysInMonth(year, month);
                else
                {
                    dayinmonth = GetInt(daystr);
                    if (dayinmonth < 1 || dayinmonth > 31)
                    {
                        if (log != null)
                            log.AppendErrorLogLine("Error in day: " + str);
                        return DateTime.MinValue;
                    }
                }
                return new DateTime(year, month, dayinmonth);
            }
            catch (Exception ex)
            {
                log.AppendErrorLogLine(string.Format("Error in date:'{0}', {1}", str, ex.Message));
                return DateTime.MinValue;
            }
        }

        public static DCImportTrans[] importDCTrans(importLog log, bool deb, SQLCache Accounts)
        {
            if (deb)
            {
                if (!log.OpenFile("exp00037", "Debitorposteringer"))
                    return null;
            }
            else
            {
                if (!log.OpenFile("exp00045", "Kreditorposteringer"))
                    return null;
            }

            try
            {
                List<string> lines;

                DateTime firstValidDate = log.years[0]._FromDate;

                int offset = deb ? 0 : 1;  // kreditor has one less
                List<DCImportTrans> lst = new List<DCImportTrans>();
                while ((lines = log.GetLine(27)) != null)
                {
                    var Date = GetDT(lines[3]);
                    if (Date < firstValidDate)
                        continue;

                    var ac = Accounts.Get(importLog.GetNotEmpty(lines[1]));
                    if (ac == null)
                        continue;

                    var rec = new DCImportTrans();           
                    rec.Date = Date;
                    rec.Account = ac.KeyStr;
                    rec.Invoice = NumberConvert.ToInt(lines[deb ? 4 : 22]);
                    rec.Voucher = GetInt(lines[5 - offset]);
                    if (rec.Voucher == 0)
                        rec.Voucher = 1;
                    rec.Text = lines[6 - offset];
                    rec.Amount = importLog.ToDouble(lines[8 - offset]);
                    rec.dif = importLog.ToDouble(lines[22 - offset]);

                    var cur = log.ConvertCur(lines[10 - offset]);
                    if (cur != 0)
                    {
                        rec.Currency = cur;
                        rec.AmountCur = importLog.ToDouble(lines[9 - offset]);
                        if (rec.Amount * rec.AmountCur < 0d) // different sign
                        {
                            rec.Currency = null;
                            rec.AmountCur = 0;
                        }
                    }
                    lst.Add(rec);
                }
                if (lst.Count() > 0)
                {
                    var arr = lst.ToArray();
                    Array.Sort(arr, new DCImportTransSort());
                    return arr;
                }
            }
            catch (Exception ex)
            {
                log.Ex(ex);
            }
            return null;
        }
    }
}
