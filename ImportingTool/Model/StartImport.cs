﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.Common;
using Uniconta.DataModel;
using ImportingTool;
using System.Windows.Controls;

namespace ImportingTool.Model
{
	public enum ImportFrom
	{
		c5_Danmark,
		c5_Iceland,
		economic_Danmark,
		economic_Norge,
		economic_Sweden,
		economic_English,
		NAV,
		xal,
	}

	public class GLPostingLineLocal : Uniconta.API.GeneralLedger.GLPostingLine
	{
		public string OrgText;
		public int primo;
	}

	public class MyGLAccount : GLAccount
	{
		public bool HasVat, HasChanges;
		public string VatKode;
	}

	static public class StartImport
	{
		public static void UpdateProgressbar(int value, string text, ProgressBar progressBar)
		{
			progressBar.Value = value;
			if (!string.IsNullOrEmpty(text))
				progressBar.Tag = text;
			else
				progressBar.Tag = string.Format("{0} of {1}", value, progressBar.Maximum);
		}
#if WPF
        static public async Task Import(Session ses, Company cc, ImportFrom from, importLog log, int DimensionLevel, System.Windows.Controls.ProgressBar progressBar, string dim1, string dim2, string dim3, string dim4, string dim5, string errorAccount, bool isInvoiceEmail, string currencyCodeEco)

#else
		static public async void Import(Session ses, Company cc, ImportFrom from, importLog log, int DimensionLevel, System.Windows.Controls.ProgressBar progressBar, string dim1, string dim2, string dim3, string dim4, string dim5, string errorAccount, bool isInvoiceEmail, string currencyCodeEco)
#endif
		{
			var country = cc._CountryId;
			log.CompCountryCode = country;

			UpdateProgressbar(0, "Starting..", progressBar);
			if (from == ImportFrom.c5_Danmark || from == ImportFrom.c5_Iceland)
			{
				// c5
				cc._ConvertedFrom = (int)ConvertFromType.C5;
				log.extension = ".def";
				log.CharBuf = new byte[1];
				log.C5Encoding = Encoding.GetEncoding(850); // western Europe. 865 is Nordic
				if (!log.OpenFile("exp00000", "Definitions"))
				{
					log.AppendLogLine("These is no c5-files in this directory");
					return;
				}
				log.extension = ".kom";
			}
			else if (from == ImportFrom.xal)
			{
				// xal
				cc._ConvertedFrom = (int)ConvertFromType.Other;
				log.extension = ".def";
				log.CharBuf = new byte[1];
				log.C5Encoding = Encoding.GetEncoding(850); // western Europe. 865 is Nordic
				if (!log.OpenFile("exp00000", "Definitions"))
				{
					log.AppendLogLine("These are no xal-files in this directory");
					return;
				}
				log.extension = ".kom";
			}
			else if (from == ImportFrom.NAV)
			{
				var realErrorAccount = Regex.Replace(errorAccount, "[a-zA-Z]", "");
				if (string.IsNullOrWhiteSpace(realErrorAccount))
				{
					log.AppendLogLine(Uniconta.ClientTools.Localization.lookup("ErrorAccount"));
					return;
				}
				cc._ConvertedFrom = (int)ConvertFromType.Nav;
				log.FirstLineIsEmpty = true;
				log.extension = ".csv";
				log.C5Encoding = Encoding.GetEncoding(850);

				if (!log.OpenFile("50 - Accounting Period", "Regnskabsår"))
				{
					log.AppendLogLine("These is no NAV-files in this directory");
					return;
				}

				//var lines = log.GetLine(1);
				//if (lines[0] == "1" && lines[1] == "2" && lines[2] == "3")
				//    log.C5Encoding = Encoding.UTF8;
				//else
				//    log.C5Encoding = Encoding.GetEncoding(850);
			}
			else // e-conomic
			{
				if (from == ImportFrom.economic_Danmark)
					country = CountryCode.Denmark;
				else if (from == ImportFrom.economic_Norge)
					country = CountryCode.Norway;
				else if (from == ImportFrom.economic_Sweden)
					country = CountryCode.Sweden;
				else if (from == ImportFrom.economic_English)
					country = CountryCode.UnitedKingdom;

				log.CompCountryCode = country;
				cc._ConvertedFrom = (int)ConvertFromType.Eco;
				log.extension = ".csv";
				log.FirstLineIsEmpty = true;
				if (!log.OpenFile(log.GetFilename("RegnskabsAar")))
				{
					log.AppendLogLine("Der er ingen filer i dette bibliotek fra e-conomic");
					return;
				}
			}

			var lin = log.ReadLine();
			if (lin == null)
			{
				log.AppendLogLine("Filen er tom");
				log.closeFile();
				return;
			}

			char splitCh, apostrof;
			var cols = lin.ToString().Split(';');
			if (cols.Length > 1)
				splitCh = ';';
			else
			{
				cols = lin.ToString().Split(',');
				if (cols.Length > 1)
					splitCh = ',';
				else
					splitCh = '#';
			}

			if (lin[0] == '\'')
				apostrof = '\'';
			else
				apostrof = '"';
			log.sp = new StringSplit(splitCh, apostrof);

			UpdateProgressbar(0, "Opret firma..", progressBar);

			if (cc._ConvertedFrom == (int)ConvertFromType.C5)
			{
				// read company currency
				log.CompCurCode = cc._CurrencyId;
				var cur = c5.GetCurrency(log);
				if (cur != 0)
					log.CompCurCode = cc._CurrencyId = cur;
			}
			ErrorCodes err;

			Company fromCompany = null;
			var ccList = await ses.GetStandardCompanies(country);
			if (ccList == null || ccList.Length == 0)
				ccList = await ses.GetStandardCompanies(CountryCode.Unknown);
			foreach (var c in ccList)
				if ((c.CompanyId == 19 && country == CountryCode.Denmark) || // 19 = Dev/erp
						(c.CompanyId == 1855 && country == CountryCode.Iceland) || // 1855 = erp
						(c.CompanyId == 443 && country == CountryCode.Sweden) || // 1855 = erp
						(c.CompanyId == 13 && country == CountryCode.UnitedKingdom) || // 1855 = erp
						(c.CompanyId == 108 && country == CountryCode.Norway)) // 108 = Dev/erp
				{
					fromCompany = c;
					break;
				}
			if (fromCompany == null)
			{
				ccList = await ses.GetStandardCompanies(CountryCode.Unknown);
				foreach (var c in ccList)
					if (c.CompanyId == 13)
					{
						fromCompany = c;
						break;
					}
			}

			if (fromCompany == null)
			{
				log.AppendLogLine("Cannot find standard Company !");
				return;
			}

			if (cc.RowId == 0)
			{
				err = await ses.CreateCompany(cc);
				if (err != 0)
				{
					log.AppendLogLine("Cannot create Company !");
					return;
				}
			}

			CompanyAPI comApi = new CompanyAPI(ses, cc);
			err = await comApi.CopyBaseData(fromCompany, cc, false, true, true, false, true, false, false, false);
			if (err != 0)
			{
				log.AppendLogLine(string.Format("Copy of default company failed. Error = {0}", err.ToString()));
				log.AppendLogLine("Conversion has stopped");
				return;
			}

			log.AppendLogLine("Import started....");

			log.api = new CrudAPI(ses, cc);
			log.api.RunInTransaction = false;

			log.CompCurCode = log.api.CompanyEntity._CurrencyId;
			log.CompCur = log.CompCurCode.ToString();
			log.CompCountryCode = log.api.CompanyEntity._CountryId;
			log.CompCountry = log.CompCountryCode.ToString();

			if (from == ImportFrom.c5_Danmark || from == ImportFrom.c5_Iceland)
				err = await c5.importAll(log, DimensionLevel, isInvoiceEmail);
			else if (from == ImportFrom.xal)
			{
				// MIMO XAL IMPORT
				
					//err = await xal.importCOA(log);
				err = await xal.importCustom(log, DimensionLevel, isInvoiceEmail);
			}
			else if (from == ImportFrom.NAV)
				err = await Nav.importAll(log, country, DimensionLevel, dim1, dim2, dim3, dim4, dim5, errorAccount);
			else // import from e-conomic
				err = await eco.importAll(log, country, isInvoiceEmail, currencyCodeEco);

			cc.InvPrice = (log.HasPriceList);
			cc.Inventory = (log.HasItem);
			cc.InvBOM = (log.HasBOM);
			cc.Creditor = (log.HasCreditor);
			cc.Debtor = (log.HasDebitor);
			cc.Contacts = (log.HasContact);
			cc.DeliveryAddress = (log.HasDelivery);
			cc.InvClientName = false;
			await log.api.Update(cc);

			log.closeFile();

			if (err == 0)
			{
				log.AppendLogLine("Din konvertering er nu færdig");
				log.AppendLogLine("");
#if !WPF
				log.AppendLogLine("Du kan nu logge ind i Uniconta og åbne dit firma.");
#endif
				log.AppendLogLine("Der kan godt gå lidt tid inden alle dine poster er bogført.");
				log.AppendLogLine("Du kan slette firmaet i menuen Firma/Firmaoplysninger,");
				log.AppendLogLine("hvis du ønsker at foretage en ny import.");
				log.AppendLogLine("Du kan finde vejledning på www.uniconta.dk/unipedia");
				log.AppendLogLine("Held og lykke med din Uniconta");
			}
			else
				log.AppendLogLine(string.Format("Der er fejl i dine data. Fejlkode = {0}", err.ToString()));
		}
	}
}
