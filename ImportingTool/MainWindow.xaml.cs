﻿using ImportingTool.Model;
using ImportingTool.Pages;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Uniconta.API.Service;
using Uniconta.API.System;
using Uniconta.Common;
using Uniconta.DataModel;
using Localization = Uniconta.ClientTools.Localization;

namespace ImportingTool
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		bool _terminate;
		public bool Terminate { get { return _terminate; } set { _terminate = value; BreakConversion(); } }
		public MainWindow()
		{
			this.DataContext = this;
			InitializeComponent();
			this.Loaded += MainWindow_Loaded;
			this.MouseDown += MainWindow_MouseDown;
			btnCopyLog.Content = String.Format(Uniconta.ClientTools.Localization.lookup("CopyOBJ"), Uniconta.ClientTools.Localization.lookup("Logs"));
			progressBar.Visibility = Visibility.Collapsed;

			txtDim1.Text = Localization.lookup("Optional");
			txtDim2.Text = Localization.lookup("Optional");
			txtDim3.Text = Localization.lookup("Optional");
			txtDim4.Text = Localization.lookup("Optional");
			txtDim5.Text = Localization.lookup("Optional");
			txtErrorAccount.Text = Localization.lookup("Required");
			txtecoCurrCode.Text = Localization.lookup("Required");

			this.Closing += HomePage_Closing;

			/* Remove it*/
			progressBar.Visibility = Visibility.Visible;
			progressBar.Maximum = 100;
			StartImport.UpdateProgressbar(0, null, progressBar);
		}

		private void TxtDim5OnGotFocus(object sender, RoutedEventArgs routedEventArgs)
		{
			throw new NotImplementedException();
		}

		static public void HomePage_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			SessionInitializer.GetSession().LogOut();
			System.Threading.Thread.Sleep(50);
		}

		private void MainWindow_MouseDown(object sender, MouseButtonEventArgs e)
		{
			btnImport.IsEnabled = true;
		}

		void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			DisplayLoginScreen();
			cmbImportFrom.ItemsSource = Enum.GetNames(typeof(ImportFrom));
			cmbImportDimension.ItemsSource = new List<string>() { "Ingen", "Kun Afdeling", "Afdeling, Bærer", "Afdeling, Bærer, Formål" };
			cmbImportDimension.SelectedIndex = 3;
		}
		async private void DisplayLoginScreen()
		{
			var logon = new LoginWindow();
			logon.Owner = this;
			logon.ShowDialog();

			if (logon.DialogResult.HasValue && logon.DialogResult.Value)
			{
				await SessionInitializer.SetupCompanies();
			}
			else
				this.Close();
		}

		public void Import(importLog log)
		{
			Company cc = new Company();
			cc._Name = txtCompany.Text;

			if (cmbImportFrom.SelectedIndex == (int)ImportFrom.c5_Iceland) // c5 iceland
			{
				cc._CountryId = CountryCode.Iceland;
				cc._CurrencyId = Currencies.ISK;
			}
			else if (cmbImportFrom.SelectedIndex == (int)ImportFrom.xal) // xal_dk
			{
				cc._CountryId = CountryCode.Denmark;
				cc._CurrencyId = Currencies.DKK;
			}
			else if (cmbImportFrom.SelectedIndex == (int)ImportFrom.economic_Norge) // Norsk Eco
			{
				cc._CountryId = CountryCode.Norway;
				cc._CurrencyId = Currencies.NOK;
			}
			else if (cmbImportFrom.SelectedIndex == (int)ImportFrom.economic_Sweden) // Norsk Eco
			{
				cc._CountryId = CountryCode.Sweden;
				cc._CurrencyId = Currencies.SEK;
			}
			else if (cmbImportFrom.SelectedIndex == (int)ImportFrom.economic_English) //eco english
			{
				cc._CountryId = CountryCode.UnitedKingdom;

				Currencies curr;
				bool isCurrency = Enum.TryParse(txtecoCurrCode.Text, out curr);
				if (isCurrency)
					cc._CurrencyId = curr;
				else
				{
					log.AppendLogLine("Currency code does not exist. Has to be a three leter code.");
					return;
				}
			}
			else
			{
				cc._CurrencyId = Currencies.DKK;
				cc._CountryId = CountryCode.Denmark;
			}
			StartImport.Import(SessionInitializer.CurrentSession, cc, (ImportFrom)cmbImportFrom.SelectedIndex, log, cmbImportDimension.SelectedIndex, progressBar, txtDim1.Text, txtDim2.Text, txtDim3.Text, txtDim4.Text, txtDim5.Text, txtErrorAccount.Text, txtInvConEmail.SelectedIndex == 0, txtecoCurrCode.Text);
		}

		private void cmbCompanies_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
		}

		FolderBrowserDialog openFolderDialog;
		private void btnImportFromDir_Click(object sender, RoutedEventArgs e)
		{
			openFolderDialog = new FolderBrowserDialog();
			if (openFolderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				txtImportFromDirectory.Text = openFolderDialog.SelectedPath;
			}
		}

		private void btnImport_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrEmpty(txtCompany.Text))
			{
				System.Windows.MessageBox.Show(String.Format(Uniconta.ClientTools.Localization.lookup("CannotBeBlank"), Uniconta.ClientTools.Localization.lookup("CompanyName")));
				return;
			}
			if (cmbImportFrom.SelectedIndex == -1)
			{
				System.Windows.MessageBox.Show("Please select C5, NAV or e-conomic.");
				return;
			}
			if (cmbImportDimension.SelectedIndex > -1)
			{

			}
			var path = txtImportFromDirectory.Text;
			if (string.IsNullOrEmpty(path))
			{
				System.Windows.MessageBox.Show("Select directory");
				return;
			}

			txtCompany.Focus();
			btnImport.IsEnabled = false;

			if (path[path.Length - 1] != '\\')
				path += '\\';

			importLog log = new importLog(path);
			log.progressBar = progressBar;
			log.Set0InAccount = chkSetAccount.IsChecked.Value;
			log.target = txtLogs;
			txtLogs.DataContext = log;
			log.AppendLogLine(string.Format("Reading from path {0}", path));
			Import(log);
		}

		private void cmbImportFrom_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (cmbImportFrom.SelectedIndex <= 1) // c5
			{
				cmbImportDimension.IsEnabled = true;

				lblDim1.Text = "Import Dimensioner:";
				lblDim1.Visibility = Visibility.Visible;
				cmbImportDimension.Visibility = Visibility.Visible;
				txtDim1.Visibility = Visibility.Collapsed;
				txtecoCurrCode.Visibility = Visibility.Collapsed;

				lblInvConEmail.Visibility = Visibility.Visible;
				txtInvConEmail.Visibility = Visibility.Visible;

				lblDim2.Visibility = Visibility.Collapsed;
				txtDim2.Visibility = Visibility.Collapsed;

				lblDim3.Visibility = Visibility.Collapsed;
				txtDim3.Visibility = Visibility.Collapsed;

				lblDim4.Visibility = Visibility.Collapsed;
				txtDim4.Visibility = Visibility.Collapsed;

				lblDim5.Visibility = Visibility.Collapsed;
				txtDim5.Visibility = Visibility.Collapsed;

				lblErrorAccount.Visibility = Visibility.Collapsed;
				txtErrorAccount.Visibility = Visibility.Collapsed;
			}
			else if (cmbImportFrom.SelectedIndex == (int)ImportFrom.NAV)
			{
				cmbImportDimension.IsEnabled = false;
				cmbImportDimension.Visibility = Visibility.Collapsed;
				lblDim1.Text = "Import Dimension 1:";
				lblDim1.Visibility = Visibility.Visible;
				txtDim1.Visibility = Visibility.Visible;
				txtecoCurrCode.Visibility = Visibility.Collapsed;

				lblInvConEmail.Visibility = Visibility.Collapsed;
				txtInvConEmail.Visibility = Visibility.Collapsed;

				lblDim2.Visibility = Visibility.Visible;
				txtDim2.Visibility = Visibility.Visible;

				lblDim3.Visibility = Visibility.Visible;
				txtDim3.Visibility = Visibility.Visible;

				lblDim4.Visibility = Visibility.Visible;
				txtDim4.Visibility = Visibility.Visible;

				lblDim5.Visibility = Visibility.Visible;
				txtDim5.Visibility = Visibility.Visible;

				lblErrorAccount.Visibility = Visibility.Visible;
				txtErrorAccount.Visibility = Visibility.Visible;
			}
			else
			{
				cmbImportDimension.IsEnabled = false;

				if (cmbImportFrom.SelectedIndex == 4)
				{
					lblDim1.Text = "Company currency code:";
					lblDim1.Visibility = Visibility.Visible;
					txtecoCurrCode.Visibility = Visibility.Visible;
				}
				else
				{
					txtecoCurrCode.Visibility = Visibility.Collapsed;
					lblDim1.Visibility = Visibility.Collapsed;
				}

				cmbImportDimension.Visibility = Visibility.Collapsed;
				txtDim1.Visibility = Visibility.Collapsed;

				lblInvConEmail.Visibility = Visibility.Visible;
				txtInvConEmail.Visibility = Visibility.Visible;

				lblDim2.Visibility = Visibility.Collapsed;
				txtDim2.Visibility = Visibility.Collapsed;

				lblDim3.Visibility = Visibility.Collapsed;
				txtDim3.Visibility = Visibility.Collapsed;

				lblDim4.Visibility = Visibility.Collapsed;
				txtDim4.Visibility = Visibility.Collapsed;

				lblDim5.Visibility = Visibility.Collapsed;
				txtDim5.Visibility = Visibility.Collapsed;

				lblErrorAccount.Visibility = Visibility.Collapsed;
				txtErrorAccount.Visibility = Visibility.Collapsed;
			}
		}

		private void btnCopyLog_Click(object sender, RoutedEventArgs e)
		{
			System.Windows.Forms.Clipboard.SetText(txtLogs.Text);
		}

		private void btnTerminate_Click(object sender, RoutedEventArgs e)
		{
			var res = System.Windows.MessageBox.Show(Uniconta.ClientTools.Localization.lookup("ExitPage"), Uniconta.ClientTools.Localization.lookup("Confirmation"), MessageBoxButton.YesNo);
			if (res == MessageBoxResult.Yes)
			{
				this.Terminate = true;
			}
		}

		void BreakConversion()
		{
		}

		private bool isEnteredDim1 = false;
		private bool isEnteredDim2 = false;
		private bool isEnteredDim3 = false;
		private bool isEnteredDim4 = false;
		private bool isEnteredDim5 = false;
		private bool isEnteredErrorAcc = false;
		private bool isEcoCurrCode = false;

		private void TxtDim1_OnGotFocus(object sender, RoutedEventArgs e)
		{
			if (!isEnteredDim1)
			{
				txtDim1.Text = "";
				isEnteredDim1 = true;
				txtDim1.FontWeight = FontWeights.Normal;
			}
		}

		private void TxtDim2_OnGotFocus(object sender, RoutedEventArgs e)
		{
			if (!isEnteredDim2)
			{
				txtDim2.Text = "";
				txtDim2.FontWeight = FontWeights.Normal;
				isEnteredDim2 = true;
			}
		}

		private void TxtDim3_OnGotFocus(object sender, RoutedEventArgs e)
		{
			if (!isEnteredDim3)
			{
				txtDim3.Text = "";
				txtDim3.FontWeight = FontWeights.Normal;
				isEnteredDim3 = true;
			}
		}

		private void TxtDim4_OnGotFocus(object sender, RoutedEventArgs e)
		{
			if (!isEnteredDim4)
			{
				txtDim4.Text = "";
				txtDim4.FontWeight = FontWeights.Normal;
				isEnteredDim4 = true;
			}
		}

		private void TxtDim5_OnGotFocus(object sender, RoutedEventArgs e)
		{
			if (!isEnteredDim5)
			{
				txtDim5.Text = "";
				txtDim5.FontWeight = FontWeights.Normal;
				isEnteredDim5 = true;
			}
		}

		private void TxtErrorAccount_OnGotFocus(object sender, RoutedEventArgs e)
		{
			if (!isEnteredErrorAcc)
			{
				txtErrorAccount.Text = "";
				txtErrorAccount.FontWeight = FontWeights.Normal;
				isEnteredErrorAcc = true;
			}
		}

		private void TxtecoCurrCode_OnGotFocus(object sender, RoutedEventArgs e)
		{
			if (!isEcoCurrCode)
			{
				txtecoCurrCode.Text = "";
				txtecoCurrCode.FontWeight = FontWeights.Normal;
				isEcoCurrCode = true;
			}
		}
	}

	public class HeightConverter : IValueConverter
	{

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return (double)value - 30;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return (double)value + 30;
		}
	}
	/* For ProgressBar*/
	public class RectConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			double width = (double)values[0];
			double height = (double)values[1];
			return new Rect(0, 0, width, height);
		}
		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
